var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");
var less = require("gulp-less");
var watch = require("gulp-watch");
var qunit = require("gulp-qunit");

gulp.task("buildts", function() {
    return browserify({
            basedir: '.',
            debug: true,
            entries: ['resources/assets/scripts/main.ts'],
            cache: {},
            packageCache: {}
        })
        .plugin(tsify)
        .bundle()
        .pipe(source('game.js'))
        .pipe(gulp.dest("public/js"));
});

gulp.task("less", function() {
    return gulp.src("resources/assets/styles/**/*.less")
               .pipe(less())
               .pipe(gulp.dest("public/css"));
});

gulp.task("copyfiles", function() {
    gulp.src("resources/assets/external/**/*")
               .pipe(gulp.dest("public/external"));
});

gulp.task("buildtests", function() {
    return browserify({
            basedir: '.',
            debug: true,
            entries: ['resources/assets/scripts/tests/main.ts'],
            cache: {},
            packageCache: {}
        })
        .plugin(tsify)
        .bundle()
        .pipe(source('tests.js'))
        .pipe(gulp.dest("public/js"));
});

gulp.task("test", ["notest"], function() {
    return gulp.src("public/tests.html")
               .pipe(qunit());
});

gulp.task("watch", function() {
    gulp.start("default");

    watch("resources/assets/scripts/**/*.ts", function() {
        gulp.start("buildts");
        gulp.start("buildtests");
    });

    watch("resources/assets/styles/**/*.less", function() {
        gulp.start("less");
    })

    watch("resources/assets/external/**/*", function() {
        gulp.start("copyfiles");
    });
});

gulp.task("notest", ["buildts", "buildtests", "less", "copyfiles"]);
gulp.task("default", ["notest", "test"]);
