<?php

use App\Game;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('game.{token}', function ($user, $gameToken) {
    $game = Game::where("token", $gameToken)->with("players")->first();
    if ($game) {
        $player = $game->players->where("user_id", $user->id)->first();

        if ($player) {
            return [
                "num" => $player->num
            ];
        }
    }
});
