<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get("/", "MenuController@index");

Route::post("/game/host", "GameController@hostGame")->name("hostGame");
Route::post("/game/join", "GameController@joinGame")->name("joinGame");
Route::get("/game/{token?}", "GameController@playGame")->name("playGame");

Route::post("/game/placePieces", "PieceController@placePieces");
Route::post("/game/movePiece", "PieceController@movePiece");
