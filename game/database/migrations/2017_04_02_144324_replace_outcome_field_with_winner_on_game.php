<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceOutcomeFieldWithWinnerOnGame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("players", function (Blueprint $table) {
            $table->dropColumn("outcome");
        });

        Schema::table("games", function (Blueprint $table) {
            $table->integer("winner_id")->unsigned()->nullable()->after("turn_id");
            $table->foreign("winner_id")->references("id")->on("players");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("players", function (Blueprint $table) {
            $table->enum("outcome", array("Win", "Draw", "Loss"))->nullable();
        });

        Schema::table("games", function (Blueprint $table) {
            $table->dropForeign(["winner_id"]);
            $table->dropColumn("winner_id");
        });
    }
}
