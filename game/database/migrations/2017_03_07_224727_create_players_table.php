<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("players", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("game_id")->unsigned();
            $table->integer("user_id")->unsigned()->nullable();
            $table->integer("num")->unsigned();
            $table->enum("outcome", array("Win", "Draw", "Loss"))->nullable();
            $table->timestamps();

            $table->foreign("game_id")->references("id")->on("games");
            $table->foreign("user_id")->references("id")->on("users");
            $table->unique(["game_id", "num"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("players");
    }
}
