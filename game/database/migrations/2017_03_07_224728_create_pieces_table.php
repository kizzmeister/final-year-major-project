<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("pieces", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("player_id")->unsigned();
            $table->string("token");
            $table->enum("type", array(
                "Marshall",
                "General",
                "Colonel",
                "Major",
                "Captain",
                "Lieutenant",
                "Sergeant",
                "Miner",
                "Scout",
                "Spy",
                "Bomb",
                "Flag"
            ));
            $table->integer("row")->unsigned()->nullable();
            $table->integer("column")->unsigned()->nullable();
            $table->timestamps();

            $table->foreign("player_id")->references("id")->on("players");
            $table->unique(["player_id", "token"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("pieces");
    }
}
