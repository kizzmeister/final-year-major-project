import AI from "../ai";
import Board from "../board";
import Player from "../player";
import Space from "../space";
import Piece from "../piece";
import Utils from "../utils";

export default class ImprovedAI extends AI {
    public placePieces(player: Player): void {
        let possibleSpaces = this.board.getPossiblePlacementSpaces(player);

        // Place the flag
        let flagSpace = this.getFlagSpace(possibleSpaces);
        this.board.spaces[flagSpace.coords[0]][flagSpace.coords[1]].addPiece(player.pieces[39]);

        // Place each remaining piece randomly
        this.board.automatePlacement(true, player);
    }

    public getFlagSpace(possibleSpaces: Space[]): Space {
        // Ensure the Flag isn't directly accessible on the front row
        let disallowedFlagSpaces = [
            [3,0], [3,1], [3,4], [3,5], [3,8], [3,9],
            [6,0], [6,1], [6,4], [6,5], [6,8], [6,9]
        ];

        let possibleFlagSpaces = possibleSpaces.filter((space) => {
            for (let coords of disallowedFlagSpaces) {
                if (space.coords[0] === coords[0] && space.coords[1] === coords[1]) {
                    return false;
                }
            }

            return true;
        });

        return possibleFlagSpaces[Utils.getRandomNum(0, possibleFlagSpaces.length)];
    }

    /**
    * Returns the movement the AI should take, based on the potential win factor of each move
    *
    * @param player
    * @param movements
    *
    * @return movement
    */
    public getMovement(player: Player, movements: [Piece, Space][]): [Piece, Space] {
        // For each possible move, calculate its win factor
        let moves: [[Piece, Space], number][] = [];
        for (let movement of movements) {
            moves.push([movement, this.getWinFactor(movement)]);
        }

        // Randomize the array, then sort by its win factor
        Utils.shuffleArray(moves);
        moves.sort((a, b) => {
            return a[1] - b[1];
        });

        // Then return the top movement
        return moves[0][0];
    }

    /**
    * Calculate a win factor for a particular move, with a lower (better) factor for more aggressive moves
    * This factor is based on the number of spaces between an enemy piece and the movement target.
    * A win factor of 0, for example, would be a direct attack on an enemy piece
    *
    * @param movement
    *
    * @return number
    */
    public getWinFactor(movement: [Piece, Space]): number {
        let player = movement[0].player;
        let distance = 0;

        if (!movement[0].space) return Number.MAX_VALUE;

        // If the target space contains a piece
        if (movement[1].piece && movement[1].piece.player !== player) {
            // If piece revealed, and we're unable to defeat it, set a high win factor
            if (
                this.knownPieces.indexOf(movement[1].piece) !== -1 &&
                movement[0].canDefeat(movement[1].piece) === false
            ) {
                return Number.MAX_VALUE;
            }

            return 0;
        }

        // Else search for the nearest piece
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label
        searchLoop:
        while (true) {
            distance++;

            let radialCoords = this.getRadialCoords(movement[1], distance);
            if (radialCoords.length === 0) break;

            for (let coords of radialCoords) {
                let space = this.board.spaces[coords[0]][coords[1]];
                if (space.piece && space.piece.player !== player) {
                    // If piece revealed, and we're unable to defeat it, don't break the loop
                    if (
                        !(this.knownPieces.indexOf(space.piece) !== -1 &&
                          movement[0].canDefeat(space.piece) === false)
                    ) {
                        break searchLoop;
                    }
                }
            }
        }

        return distance;
    }

    public getRadialCoords(space: Space, radius: number): [number, number][] {
        let radialCoords: [number, number][] = [];
        let coords = space.coords;

        // Calculate the coordinates of the radial corners
        let topLeft: [number, number] = [coords[0]-radius, coords[1]-radius];
        let topRight: [number, number] = [coords[0]-radius, coords[1]+radius];
        let bottomLeft: [number, number] = [coords[0]+radius, coords[1]-radius];
        let bottomRight: [number, number] = [coords[0]+radius, coords[1]+radius];

        // Get the coordinates in between each radial corner
        let top = Utils.calculateCoordsBetween(topLeft, topRight);
        let right = Utils.calculateCoordsBetween(topRight, bottomRight);
        let bottom = Utils.calculateCoordsBetween(bottomRight, bottomLeft);
        let left = Utils.calculateCoordsBetween(bottomLeft, topLeft);

        // Merge the coordinates together, in a clockwise formation
        radialCoords = radialCoords.concat(
            [topLeft],
            top,
            [topRight],
            right,
            [bottomRight],
            bottom,
            [bottomLeft],
            left
        );

        // Finally, filter the array to remove any coordinates out of scope
        let maxCoord = this.board.boardSize - 1;
        return radialCoords.filter((coord) => {
            return coord[0] >= 0 && coord[1] >= 0 && coord[0] <= maxCoord && coord[1] <= maxCoord;
        });
    }
}
