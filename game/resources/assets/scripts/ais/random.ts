import AI from "../ai";
import Player from "../player";
import Space from "../space";
import Piece from "../piece";
import Utils from "../utils";

export default class RandomAI extends AI {
    public placePieces(player: Player): void {
        // Place each piece randomly
        this.board.automatePlacement(true, player);
    }

    public getMovement(player: Player, movements: [Piece, Space][]): [Piece, Space] {
        return movements[Utils.getRandomNum(0, movements.length)];
    }
}
