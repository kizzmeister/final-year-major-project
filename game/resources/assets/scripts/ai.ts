import Board from "./board";
import { GameState } from "./board";
import Player from "./player";
import Space from "./space";
import Piece from "./piece";
import Utils from "./utils";

abstract class AI {
    protected board: Board;
    protected _knownPieces: Piece[] = [];

    constructor(board: Board) {
        this.board = board;
    }

    public get knownPieces(): Piece[] {
        return this._knownPieces;
    }

    public handleReveal(piece: Piece): void {
        this.knownPieces.push(piece);
    }

    /**
     * Function to retrieve all possible movements for a player
     *
     * @param Player the player instance
     *
     * @return array the possible movements in the form [Piece, Space]
     */
    public getPossibleMovements(player: Player): [Piece, Space][] {
        let movements: [Piece, Space][] = [];
        let pieces = player.getPieces(true);

        for (let piece of pieces) {
            for (let row of this.board.spaces) {
                for (let space of row) {
                    if (space.piece && space.piece.player === player) continue;

                    if (piece.canMoveTo(space, this.board.spaces, this.board.boardSize)) {
                        movements.push([piece, space]);
                    }
                }
            }
        }

        return movements;
    }

    public takeTurn(player: Player, guiCallback: any, delay: number): void {
        if (this.board.gameState !== GameState.Play) return;

        this.board.turn = player;
        guiCallback();

        let movements = this.getPossibleMovements(player);
        let movement = this.getMovement(player, movements);
        this.handleMovement(movement, guiCallback, delay);

        guiCallback();
    }

    public handleMovement(movement: [Piece, Space], guiCallback: any, delay: number): void {
        // In the unlikely event that we don't get a valid movement, return
        // This shouldn't happen in-game as we monitor the number of non-movable pieces
        if (!movement || !movement[0] || !movement[1]) {
            this.board.turn = this.board.currentPlayer;
            this.board.disabled = false;
            return;
        }

        let piece = movement[0];
        let originSpace = piece.space;
        let targetSpace = movement[1];

        if (targetSpace.piece) {
            let combatSpace = this.board.getCombatSpace(originSpace, targetSpace);

            piece.revealed = true;
            targetSpace.piece.revealed = true;
            this.handleReveal(targetSpace.piece);

            setTimeout(() => {
                this.board.handleCombat(combatSpace, targetSpace);
                this.board.turn = this.board.currentPlayer;
                this.board.disabled = false;
                guiCallback();
            }, delay);
        } else {
            originSpace.removePiece();
            targetSpace.addPiece(piece);

            this.board.turn = this.board.currentPlayer;
            this.board.disabled = false;
        }
    }

    public abstract placePieces(player: Player): void;
    public abstract getMovement(player: Player, movements: [Piece, Space][]): [Piece, Space];
}

export default AI;
