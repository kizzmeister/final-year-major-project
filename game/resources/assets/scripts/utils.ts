export default class Utils {
    // Generates a random number in between a min and a max (exclusively)
    // Taken from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    public static getRandomNum(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);

        return Math.floor(Math.random() * (max - min)) + min;
    }

    // Shuffles an array randomly
    // Taken from http://stackoverflow.com/a/12646864
    public static shuffleArray(array: any): any {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        return array;
    }

    // Helper function to calculate the possible coordinates between two spaces (in one direction only)
    public static calculateCoordsBetween(origin: [number, number], target: [number, number]): [number, number][] {
        let coords: [number, number][] = [];

        // Throw an error if both the rows and columns are different
        if (origin[0] !== target[0] && origin[1] !== target[1]) {
            throw new Error("Unable to calculate coordinates");
        }

        if (origin[0] !== target[0]) { // If the row is changing
            let increment = (origin[0] < target[0]) ? 1 : -1;
            for (let row = (origin[0] + increment); row !== target[0]; row = row + increment) {
                coords.push([row, origin[1]]);
            }
        } else if (origin[1] !== target[1]) { // If the column is changing
            let increment = (origin[1] < target[1]) ? 1 : -1;
            for (let col = (origin[1] + increment); col !== target[1]; col = col + increment) {
                coords.push([origin[0], col]);
            }
        }

        return coords;
    }
}
