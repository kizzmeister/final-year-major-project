import Player from "./player";
import Space from "./space";
import Server from "./server";
import Piece from "./piece";
import AI from "./ai";
import AiFactory from "./aiFactory";
import Utils from "./utils";

export enum GameState {
    Placement,
    Ready,
    Play,
    End
}

export default class Board {
    private _players: Player[];
    private _spaces: Space[][];
    private _gameState: GameState;
    private _gameToken: string;
    private _turn: Player;
    private _winner: Player;
    private _numMoves: number;
    private _disabled: boolean;
    private _server: Server;
    private _ai: AI;

    public readonly currentPlayer: Player;
    public readonly opposingPlayer: Player;
    public readonly boardSize: number = 10;
    public readonly placementSpaceRange: [number, number][][] = [
        [[6,0], [9,9]], //Player 1
        [[0,0], [3,9]]  //Player 2
    ];
    public readonly disabledSpaces: [number, number][] = [
        [4,2],[4,3],[4,6],[4,7],
        [5,2],[5,3],[5,6],[5,7]
    ];
    private readonly pieceOrder = [
        "Marshall",
        "General",
        "Colonel",
        "Major",
        "Captain",
        "Lieutenant",
        "Sergeant",
        "Miner",
        "Scout",
        "Spy",
        "Bomb",
        "Flag",
        "Unknown"
    ];

    constructor(
        players: Player[],
        aiType: string = "random",
        gameToken: string = "",
        gameState: string = "",
        playerNum: number = 0,
        winner: number = null,
        playerPieces: any = []
    ) {
        this._players = players;
        this._spaces = [];
        this._gameState = (gameState) ? GameState[gameState] : GameState.Placement;
        this._gameToken = gameToken;
        this._numMoves = 0;
        this._disabled = false;

        // For simplicity, the current player is always the first
        this.currentPlayer = this._players[0];
        this.opposingPlayer = this._players[1];

        for (let row = 0; row < this.boardSize; row++) {
            for (let col = 0; col < this.boardSize; col++) {
                if (!this._spaces[row]) this._spaces[row] = [];
                this._spaces[row][col] = new Space([row, col], this.isDisabledSpace([row,col]));
            }
        }

        if (this.isMultiplayer()) {
            this.handleMultiplayerData(playerNum, winner, playerPieces);
        } else {
            this._ai = AiFactory.getInstance(aiType, this);

            // Set the turn to a random player
            this._turn = this._players[Utils.getRandomNum(0, this._players.length)];
        }
    }

    public init(guiCallback: any): void {
        // Initialise the game server if necessary
        if (this.isMultiplayer()) {
            this._server = new Server(this, guiCallback);
        }
    }

    public get players(): Player[] {
        return this._players;
    }

    public get spaces(): Space[][] {
        return this._spaces;
    }

    public get gameState(): GameState {
        return this._gameState;
    }

    public set gameState(gameState: GameState) {
        this._gameState = gameState;
    }

    public get gameToken(): string {
        return this._gameToken;
    }

    public isMultiplayer() {
        return this._gameToken.length !== 0;
    }

    public get turn(): Player {
        return this._turn;
    }

    public set turn(player: Player) {
        this._turn = player;
    }

    public get winner(): Player {
        return this._winner;
    }

    public set winner(winner: Player) {
        this._winner = winner;
    }

    public get numMoves(): number {
        return this._numMoves;
    }

    public get disabled(): boolean {
        return this._disabled;
    }

    public set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    public incrementNumMoves(): void {
        this._numMoves++;
    }

    public decrementNumMoves(): void {
        this._numMoves--;
    }

    public getPlacementSpaceRange(player: Player = this.currentPlayer): number[][] {
        let index = (player === this.currentPlayer) ? 0 : 1;
        return this.placementSpaceRange[index];
    }

    public isPlacementSpace(space: Space, player: Player = this.currentPlayer): boolean {
        let index = (player === this.currentPlayer) ? 0 : 1;
        let coords = space.coords;

        return(
            coords[0] >= this.placementSpaceRange[index][0][0] &&
            coords[1] >= this.placementSpaceRange[index][0][1] &&
            coords[0] <= this.placementSpaceRange[index][1][0] &&
            coords[1] <= this.placementSpaceRange[index][1][1]
        );
    }

    // Calculate the coordinates for each available space in the placement range
    public getPossiblePlacementSpaces(player: Player = this.currentPlayer): Space[] {
        let placementRange = this.getPlacementSpaceRange(player);
        let possibleSpaces: Space[] = [];

        for (let row = placementRange[0][0]; row <= placementRange[1][0]; row++) {
            for (let col = placementRange[0][1]; col <= placementRange[1][1]; col++) {
                let space = this.spaces[row][col];
                if (space.piece === null) possibleSpaces.push(space);
            }
        }

        return possibleSpaces;
    }

    public handleMultiplayerData(playerNum: number, winner: number, playerPieces: any): void {
        for (let item of playerPieces) {
            let p = (item.player === playerNum) ? this.currentPlayer : this.opposingPlayer;

            p.num = item.player;
            p.updatePieces(item.pieces, this.spaces);
            if (item.turn) this.turn = p;
        }

        if (winner) {
            if (winner === this.currentPlayer.num) {
                this.winner = this.currentPlayer;
            } else {
                this.winner = this.opposingPlayer;
            }
        }

        if (this.turn && this.turn !== this.currentPlayer) {
            this.disabled = true;
        }
    }

    public movePiece(originId: string, originCoords: [number, number], targetCoords: [number, number], guiCallback: any, delay: number = 3000): void {
        let targetSpace = this.spaces[targetCoords[0]][targetCoords[1]];

        // Check that the board and space aren't disabled, it's our turn and we're placing in the correct spaces
        if (this.disabled) return;
        if (targetSpace.disabled) return;
        if (this.gameState === GameState.Play && this.turn !== this.currentPlayer) return;
        if (this.gameState === GameState.Placement && !this.isPlacementSpace(targetSpace)) return;
        if (this.gameState === GameState.End) return;

        // If we're moving a piece already on the board
        if (originId.indexOf("#board") !== -1) {
            let originSpace = this.spaces[originCoords[0]][originCoords[1]];
            this.movePieceOnBoard(originSpace, targetSpace, guiCallback, delay);
        } else if (originId.indexOf("#leftSide") !== -1 && this.gameState === GameState.Placement) {
            this.movePieceOnSide(originCoords, targetSpace);
        }
    }

    public movePieceOnBoard(originSpace: Space, targetSpace: Space, guiCallback: any, delay: number) {
        let origin = originSpace.piece;
        let target = targetSpace.piece;

        // Check we're moving our piece and that this is a valid move (or we're in the placement stage)
        if (origin.player !== this.currentPlayer) return;
        if (!origin.canMoveTo(targetSpace, this.spaces, this.boardSize) && this.gameState !== GameState.Placement) return;

        // If the target space is occupied and we're in play
        if (target !== null && this.gameState === GameState.Play) {
            // Ensure we can't take our own pieces
            if (target.player === this.currentPlayer) return;
            let combatSpace = this.getCombatSpace(originSpace, targetSpace);

            // If we're in single player mode, handle the combat stages.
            // This will be handled by the server.movePiece method for multiplayer
            if (!this.isMultiplayer()) {
                this.disabled = true;

                // Reveal the pieces
                origin.revealed = true;
                target.revealed = true;
                this._ai.handleReveal(target);

                // Delay the combat stage, allowing the user to see the revealed piece
                setTimeout(() => {
                    this.handleCombat(combatSpace, targetSpace);
                    guiCallback();

                    this._ai.takeTurn(this.opposingPlayer, guiCallback, delay);
                }, delay);
            }
        } else {
            originSpace.removePiece();
            targetSpace.addPiece(origin);

            // If we're in single player mode, make the AI take its turn
            if (!this.isMultiplayer() && this.gameState === GameState.Play) {
                this.disabled = true;
                this._ai.takeTurn(this.opposingPlayer, guiCallback, delay);
            }
        }

        if (this.gameState === GameState.Play) {
            if (this.isMultiplayer()) {
                this.disabled = true;
                this._server.movePiece(originSpace, targetSpace, origin, target);
            }

            this.incrementNumMoves();
        }
    }

    public getCombatSpace(originSpace: Space, targetSpace: Space): Space {
        let combatSpace = originSpace;
        let piece = originSpace.piece;

        // When attacking with the scout, move the piece next to the opponent
        if (piece && piece.rankName === "Scout") {
            let possibleCoords = Utils.calculateCoordsBetween(originSpace.coords, targetSpace.coords);

            if (possibleCoords.length > 0) {
                // Move the scout to the 'battle' coordinates, updating the origin space at the same time
                let battleCoords = possibleCoords[possibleCoords.length-1];

                originSpace.removePiece();
                combatSpace = this.spaces[battleCoords[0]][battleCoords[1]];
                combatSpace.addPiece(piece);
            }
        }

        return combatSpace;
    }

    public handleCombat(originSpace: Space, targetSpace: Space) {
        let attackingPiece = originSpace.piece;
        let defendingPiece = targetSpace.piece;
        attackingPiece.revealed = false;
        defendingPiece.revealed = false;

        if (attackingPiece.canDefeat(defendingPiece)) {
            targetSpace.removePiece();
            originSpace.removePiece();

            // If the pieces aren't the same, move the attacking piece
            if (attackingPiece.rankNum !== defendingPiece.rankNum) {
                targetSpace.addPiece(attackingPiece);
            }

            // If the captured piece is the Flag, or we've now captured all movable pieces
            if (!this.hasMovablePieces(this.currentPlayer) && !this.hasMovablePieces(this.opposingPlayer)) {
                this.endGame(null); // Draw
            } else if (!this.hasMovablePieces(this.opposingPlayer)) {
                this.endGame(this.currentPlayer);
            } else if (!this.hasMovablePieces(this.currentPlayer)) {
                this.endGame(this.opposingPlayer);
            } else if (defendingPiece.rankName === "Flag") {
                this.endGame(this.turn);
            }
        } else {
            originSpace.removePiece();
        }
    }

    public movePieceOnSide(originCoords: [number, number], targetSpace: Space) {
        let numPieces = this.currentPlayer.pieces.length;
        let pieceNum = (originCoords[0] * numPieces/this.boardSize) + originCoords[1];

        let pieces = this.sortPieces(this.currentPlayer.getPieces());
        targetSpace.addPiece(pieces[pieceNum]);
    }

    public hasMovablePieces(player: Player = this.currentPlayer): boolean {
        let unMovablePieces = ["Flag", "Bomb"];
        for (let row = 0; row < this.boardSize; row++) {
            for (let col = 0; col < this.boardSize; col++) {
                let piece = this.spaces[row][col].piece;
                if (piece === null || piece.player !== player) continue;

                if (unMovablePieces.indexOf(piece.rankName) === -1) {
                    return true;
                }
            }
        }

        return false;
    }

    public automatePlacement(randomize: boolean = false, player: Player = this.currentPlayer): void {
        let possibleSpaces = this.getPossiblePlacementSpaces(player);
        if (possibleSpaces.length === 0) return;

        for (let piece of player.getPieces()) {
            let spaceIndex = (randomize) ? Utils.getRandomNum(0, possibleSpaces.length) : 0;
            let space = possibleSpaces[spaceIndex];

            space.addPiece(piece);
            possibleSpaces.splice(spaceIndex, 1);
        }
    }

    public startGame(guiCallback: any): void {
        if (this.currentPlayer.getPieces().length === 0) {
            if (this.isMultiplayer()) {
                // Send our placed pieces over to the server and set the gameState to ready
                this._server.placePieces();
            } else {
                // Place all opposing pieces onto the board, randomly
                this._ai.placePieces(this.opposingPlayer);
                this.gameState = GameState.Play;

                // If it's the opposing players turn, make a move
                if (this.turn === this.opposingPlayer) {
                    this.disabled = true;
                    this._ai.takeTurn(this.opposingPlayer, guiCallback, 3000);
                }

                guiCallback();
            }
        }
    }

    public endGame(winner: Player): void {
        // End the game and set the winner
        this.gameState = GameState.End;
        this.winner = winner;
    }

    public restartGame(): void {
        this.gameState = GameState.Placement;
        this.turn = this.currentPlayer;
        this._numMoves = 0;
        this.winner = null;

        this.reset(this.currentPlayer);
        this.reset(this.opposingPlayer);
    }

    public reset(player: Player): void {
        for (let row = 0; row < this.boardSize; row++) {
            for (let col = 0; col < this.boardSize; col++) {
                let piece = this.spaces[row][col].piece;
                if (piece && piece.player === player) {
                    this.spaces[row][col].removePiece();
                }
            }
        }
    }

    public sortPieces(pieces: Piece[]): Piece[] {
        // Copy the array, so we don't sort the original argument - https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
        pieces = pieces.slice();

        pieces.sort((a: Piece, b: Piece) => {
            // Compare using the configured rank order
            let aRank = this.pieceOrder.indexOf(a.rankName);
            let bRank = this.pieceOrder.indexOf(b.rankName);

            if (aRank < bRank) return -1;
            if (aRank > bRank) return 1;

            // Also compare using the piece tokens, before returning 0 (which is unpredictable)
            if (a.token < b.token) return -1;
            if (a.token > b.token) return 1;

            return 0;
        });

        return pieces;
    }

    public isDisabledSpace(space: [number, number]): boolean {
        for (let s of this.disabledSpaces) {
            if (space[0] === s[0] && space[1] === s[1]) return true;
        }

        return false;
    }
}
