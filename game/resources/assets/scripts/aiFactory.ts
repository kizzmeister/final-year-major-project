import Board from "./board";
import AI from "./ai";
import RandomAI from "./ais/random";
import ImprovedAI from "./ais/improved";

export default class AiFactory {
    //With help from: http://stackoverflow.com/questions/12964895/how-to-implement-factory-design-pattern-with-requirejs
    private static aiMap = {
        random: RandomAI,
        improved: ImprovedAI
    };

    public static getInstance(aiName: string, board: Board): AI {
        aiName = aiName.toLowerCase();

        if (this.aiMap[aiName]) {
            return new this.aiMap[aiName](board);
        } else {
            throw new Error("Invalid aiName - " + aiName);
        }
    }
}
