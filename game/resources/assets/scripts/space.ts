import Piece from "./piece";

export default class Space {
    private _row: number;
    private _col: number;
    private _piece: Piece;
    private _disabled: boolean;

    constructor(coords: [number, number], disabled: boolean = false) {
        this._row = coords[0];
        this._col = coords[1];
        this._disabled = disabled;
        this._piece = null;
    }

    public addPiece(piece: Piece) {
        if (this._disabled === false) {
            this.removePiece();

            piece.space = this;
            this._piece = piece;
        } else {
            throw new Error("Unable to add piece to disabled space");
        }
    }

    public removePiece() {
        if (this._disabled === false) {
            if (this._piece) this._piece.space = null;
            this._piece = null;
        } else {
            throw new Error("Unable to remove piece from disabled space");
        }
    }

    public get coords(): [number, number] {
        return [this._row, this._col];
    }

    public get piece(): Piece {
        return this._piece;
    }

    public get disabled(): boolean {
        return this._disabled;
    }
}
