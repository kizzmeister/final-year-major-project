import Player from "./player";
import Space from "./space";

abstract class Piece {
    protected _rank: string;
    protected _rankNum: number;
    protected _rankName: string;
    protected _token: string;
    protected _revealed: boolean;
    protected _space: Space;
    protected _player: Player;

    constructor(player: Player, token: string = "") {
        this._player = player;
        this._space = null;
        this._revealed = false;
        this._token = token;
    }

    public get rank(): string {
        if (this._rank) {
            return this._rank;
        }

        return String(this.rankNum);
    }

    public get rankNum(): number {
        return this._rankNum;
    }

    public get rankName(): string {
        return this._rankName;
    }

    public get space(): Space {
        return this._space;
    }

    public set space(space: Space) {
        this._space = space;
    }

    public get player(): Player {
        return this._player;
    }

    public get token(): string {
        return this._token;
    }

    public set token(token: string) {
        this._token = token;
    }

    public get revealed(): boolean {
        return this._revealed;
    }

    public set revealed(revealed: boolean) {
        this._revealed = revealed;
    }

    // Default canMoveTo function, only allowing horizontal and vertical movement by one space at a time
    public canMoveTo(targetSpace: Space, spaces: Space[][], boardSize: number): boolean {
        let origin = this.space.coords;
        let target = targetSpace.coords;
        let maxCoord = boardSize - 1;

        if (targetSpace.disabled) return false;
        if (target[0] < 0 || target[1] < 0 || target[0] > maxCoord || target[1] > maxCoord) return false;

        // If the two rows are the same, check that the column is between origin-1 and origin+1
        if (origin[0] === target[0] && (target[1] >= (origin[1]-1) && target[1] <= (origin[1]+1))) {
            return true;
        }

        // If the two columns are the same, check that the row is between origin-1 and origin+1
        if (origin[1] === target[1] && (target[0] >= (origin[0]-1) && target[0] <= (origin[0]+1))) {
            return true;
        }

        return false;
    }

    // Default canDefeat function, allowing pieces to defeat ranks below or equal to them
    public canDefeat(piece: Piece): boolean {
        return this.rankNum <= piece.rankNum;
    }
}

export default Piece;
