import Player from "./player";
import Piece from "./piece";
import Marshall from "./pieces/1_marshall";
import General from "./pieces/2_general";
import Colonel from "./pieces/3_colonel";
import Major from "./pieces/4_major";
import Captain from "./pieces/5_captain";
import Lieutenant from "./pieces/6_lieutenant";
import Sergeant from "./pieces/7_sergeant";
import Miner from "./pieces/8_miner";
import Scout from "./pieces/9_scout";
import Spy from "./pieces/10_spy";
import Bomb from "./pieces/0_bomb";
import Flag from "./pieces/11_flag";
import Unknown from "./pieces/12_unknown";

export default class PieceFactory {
    //With help from: http://stackoverflow.com/questions/12964895/how-to-implement-factory-design-pattern-with-requirejs
    private static pieceMap = {
        marshall: Marshall,
        general: General,
        colonel: Colonel,
        major: Major,
        captain: Captain,
        lieutenant: Lieutenant,
        sergeant: Sergeant,
        miner: Miner,
        scout: Scout,
        spy: Spy,
        bomb: Bomb,
        flag: Flag,
        unknown: Unknown
    };

    public static getInstance(pieceName: string, player: Player, token: string = ""): Piece {
        pieceName = pieceName.toLowerCase();

        if (this.pieceMap[pieceName]) {
            return new this.pieceMap[pieceName](player, token);
        } else {
            throw new Error("Invalid pieceName - " + pieceName);
        }
    }
}
