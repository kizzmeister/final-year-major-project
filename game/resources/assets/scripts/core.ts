import Board from "./board";
import Gui from "./gui";
import Player from "./player";

export default class Core {
    private _board: Board;
    private _gui: Gui;

    constructor(
        aiType: string = "random",
        gameToken: string = "",
        gameState: string = "",
        playerNum: number = 0,
        winner: number = null,
        playerPieces: any = []
    ) {
        let players = [new Player(1), new Player(2)];

        this._board = new Board(players, aiType, gameToken, gameState, playerNum, winner, playerPieces);
        this._gui = new Gui(this.board);

        this._board.init(this._gui.update);
        this._gui.init();
    }

    public get gui(): Gui {
        return this._gui;
    }

    public get board(): Board {
        return this._board;
    }
}
