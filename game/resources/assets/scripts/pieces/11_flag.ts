import Piece from "../piece";
import Space from "../space";

export default class Flag extends Piece {
    protected readonly _rank = "F";
    protected readonly _rankNum = 11;
    protected readonly _rankName = "Flag";

    // Overriden canMoveTo function, disallowing all movement
    public canMoveTo(targetSpace: Space, spaces: Space[][], boardSize: number): boolean {
        return false;
    }
}
