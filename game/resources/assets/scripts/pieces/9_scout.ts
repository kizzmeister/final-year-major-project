import Piece from "../piece";
import Space from "../space";
import Utils from "../utils";

export default class Scout extends Piece {
    protected readonly _rankNum = 9;
    protected readonly _rankName = "Scout";

    // Custom canMoveTo function to allow movement to multiple spaces in the same direction
    public canMoveTo(targetSpace: Space, spaces: Space[][], boardSize: number): boolean {
        let origin = this.space.coords;
        let target = targetSpace.coords;
        let maxCoord = boardSize - 1;

        if (targetSpace.disabled) return false;
        if (target[0] < 0 || target[1] < 0 || target[0] > maxCoord || target[1] > maxCoord) return false;

        // If the two rows are the same, check that the column is between origin-1 and origin+1
        if (origin[0] === target[0] && (target[1] >= (origin[1]-1) && target[1] <= (origin[1]+1))) {
            return true;
        }

        // If the two columns are the same, check that the row is between origin-1 and origin+1
        if (origin[1] === target[1] && (target[0] >= (origin[0]-1) && target[0] <= (origin[0]+1))) {
            return true;
        }

        // If we're attempting to move multiple spaces
        if ((origin[0] === target[0] && origin[1] !== target[1]) || (origin[1] === target[1] && origin[0] !== target[0])) {
            let possibleCoords = Utils.calculateCoordsBetween(origin, target);

            for (let coords of possibleCoords) {
                let space = spaces[coords[0]][coords[1]];
                if (space.piece !== null || space.disabled === true) return false;
            }

            return true;
        }

        return false;
    }
}
