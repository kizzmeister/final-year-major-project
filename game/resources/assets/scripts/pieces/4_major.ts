import Piece from "../piece";

export default class Major extends Piece {
    protected readonly _rankNum = 4;
    protected readonly _rankName = "Major";
}
