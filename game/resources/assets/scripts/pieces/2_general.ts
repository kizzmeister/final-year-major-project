import Piece from "../piece";

export default class General extends Piece {
    protected readonly _rankNum = 2;
    protected readonly _rankName = "General";
}
