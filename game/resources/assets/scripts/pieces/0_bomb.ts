import Piece from "../piece";
import Space from "../space";

export default class Bomb extends Piece {
    protected readonly _rank = "B";
    protected readonly _rankNum = 0;
    protected readonly _rankName = "Bomb";

    // Overriden canMoveTo function, disallowing all movement
    public canMoveTo(targetSpace: Space, spaces: Space[][], boardSize: number): boolean {
        return false;
    }
}
