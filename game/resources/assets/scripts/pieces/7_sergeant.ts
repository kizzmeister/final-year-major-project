import Piece from "../piece";

export default class Sergeant extends Piece {
    protected readonly _rankNum = 7;
    protected readonly _rankName = "Sergeant";
}
