import Piece from "../piece";

export default class Marshall extends Piece {
    protected readonly _rankNum = 1;
    protected readonly _rankName = "Marshall";
}
