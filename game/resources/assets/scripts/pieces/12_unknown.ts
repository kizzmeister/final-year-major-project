import Piece from "../piece";

export default class Unknown extends Piece {
    protected _rank = "?";
    protected _rankNum = 12;
    protected _rankName = "Unknown";

    public revealAs(piece: Piece) : void {
        this._rank = piece.rank;
        this._rankNum = piece.rankNum;
        this._rankName = piece.rankName;
    }
}
