import Piece from "../piece";

export default class Colonel extends Piece {
    protected readonly _rankNum = 3;
    protected readonly _rankName = "Colonel";
}
