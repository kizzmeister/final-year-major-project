import Piece from "../piece";

export default class Lieutenant extends Piece {
    protected readonly _rankNum = 6;
    protected readonly _rankName = "Lieutenant";
}
