import Piece from "../piece";

export default class Captain extends Piece {
    protected readonly _rankNum = 5;
    protected readonly _rankName = "Captain";
}
