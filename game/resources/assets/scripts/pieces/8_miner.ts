import Piece from "../piece";

export default class Miner extends Piece {
    protected readonly _rankNum = 8;
    protected readonly _rankName = "Miner";

    // Overriden canDefeat function, allowing miners to defeat bombs
    public canDefeat(piece: Piece): boolean {
        if (piece.rankName === "Bomb") {
            return true;
        }

        return this.rankNum <= piece.rankNum;
    }
}
