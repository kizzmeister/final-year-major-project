import Piece from "../piece";

export default class Spy extends Piece {
    protected readonly _rank = "S";
    protected readonly _rankNum = 10;
    protected readonly _rankName = "Spy";

    // Overriden canDefeat function, allowing spies to defeat marshalls
    public canDefeat(piece: Piece): boolean {
        if (piece.rankName === "Marshall") {
            return true;
        }

        return this.rankNum <= piece.rankNum;
    }
}
