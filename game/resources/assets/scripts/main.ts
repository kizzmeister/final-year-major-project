import Core from "./core";

// Bring in the game configuration defined globally
// http://stackoverflow.com/questions/13252225/call-a-global-variable-inside-typescript-module
declare var gameConfig: any;

// Main class to handle menu and initialisation of Core Game class
class Main {
    constructor() {
        window["game"] = window["game"] || [];
        window["game"]["init"] = this.initGame;
    }

    public initGame = (
        aiType: string,
        gameToken: string = "",
        gameState: string = "",
        playerNum: number = 0,
        winner: number = null,
        playerPieces: any = []
    ): void => {
        let core = new Core(aiType, gameToken, gameState, playerNum, winner, playerPieces);
    }
}

let main = new Main();
