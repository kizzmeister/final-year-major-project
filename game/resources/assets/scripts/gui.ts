import Board from "./board";
import { GameState } from "./board";
import Player from "./player";
import Piece from "./piece";
import Space from "./space";

export default class Gui {
    private board: Board;

    constructor(board: Board) {
        this.board = board;
    }

    public init(): void {
        $("#menu").hide();
        $("#gameContainer").append('<div id="leftSide"></div>');
        $("#gameContainer").append('<div id="board"></div>');
        $("#gameContainer").append('<div id="rightSide"></div>');

        // Add event handlers to global game object
        window["game"] = window["game"] || [];
        window["game"]["pickupPiece"] = this.pickupPiece;
        window["game"]["dragPiece"] = this.dragPiece;
        window["game"]["dropPiece"] = this.dropPiece;

        this.initBoard();
        this.initSidebars();
        this.initFooter();
        this.update();
    }

    public initBoard(): void {
        this.drawTable("#board", "board", this.board.boardSize, this.board.boardSize, true);
    }

    public initSidebars(): void {
        let numCols = this.getNumSideCols();

        this.drawTable("#leftSide", "leftSide", this.board.boardSize, numCols, true);
        this.drawTable("#rightSide", "rightSide", this.board.boardSize, numCols);
    }

    public initFooter(): void {
        $("#gameFooter").append('<button id="autoComplete" class="btn btn-default">Auto Complete</button>');
        $("#gameFooter").append('<button id="randomize" class="btn btn-default">Randomize</button>');
        $("#gameFooter").append('<button id="startGame" class="btn btn-default">Start Game</button>');
        $("#gameFooter").append('<div id="stats">\
                                    <div id="winner" class="alert">Winner: <span id="winnerNum"></span></div>\
                                    <div id="turn" class="alert">Turn: <span id="turnNum"></span></div>\
                                    <div id="moves">Moves: <span id="numMoves"></span></div>\
                                 </div>');
        $("#gameFooter").append('<div>\
                                    <button id="restartGame" class="btn btn-default">Restart Game</button>\
                                    <form id="backToMenuForm" action="../../" method="GET">\
                                        <button id="backToMenu" class="btn btn-default">Back to Menu</button>\
                                    </form>\
                                 </div>');

        if (this.board.isMultiplayer()) {
            $("#gameFooter").append('<div id="multiplayerToken">Player: \
                                    ' + this.board.currentPlayer.num + '<br />\
                                    Game Token: \
                                    ' + this.board.gameToken + '\
                                    </div>');
            $("#gameFooter").append('<div id="multiplayerStatus"></div>');
        }

        // Setup the click handlers
        // Using instance arrow function to ensure we keep the right 'this' - https://github.com/Microsoft/TypeScript/wiki/'this'-in-TypeScript
        $("#autoComplete").click(() => {
            this.board.automatePlacement();
            this.update();
        });

        $("#randomize").click(() => {
            this.board.reset(this.board.currentPlayer);
            this.board.automatePlacement(true);
            this.update();
        });

        $("#startGame").click(() => {
            $("#startGame").attr("disabled", "true");
            this.board.startGame(this.update);
        });

        $("#restartGame").click(() => {
            this.board.restartGame();
            this.update();
        });
    }

    public updateBoard(): void {
        let spaces = this.board.spaces;
        let updatableStates = [GameState.Placement, GameState.Play];

        // Update each space row by row and column by column
        for (let row = 0; row < this.board.boardSize; row++) {
            for (let col = 0; col < this.board.boardSize; col++) {
                let space = spaces[row][col];
                let elem = $('#board_'+row+'_'+col);
                let content = "";

                elem.removeClass("player_0 player_1 placement_0 placement_1 revealed");

                // Disable dragging empty and opposing pieces. Also disable dragging when the board is disabled or game is finished
                if (space.piece === null || space.piece.player !== this.board.currentPlayer || $.inArray(this.board.gameState, updatableStates) === -1 || this.board.disabled) {
                    elem.removeAttr("draggable");
                    elem.removeAttr("ondragstart");
                } else {
                    elem.attr("draggable", "true");
                    elem.attr("ondragstart", "game.pickupPiece(event)");
                }

                // If the space is disabled, show 'x' and add the disabled class
                if (space.disabled === true) {
                    elem.addClass("disabled");
                    content = "x";
                } else {
                    elem.attr("ondragover", "game.dragPiece(event)");
                    elem.attr("ondrop", "game.dropPiece(event)");

                    // Highlight player pieces
                    if (space.piece && space.piece.player) {
                        if (space.piece.player === this.board.currentPlayer) {
                            elem.addClass("player_0");
                        } else {
                            elem.addClass("player_1");
                        }
                    }

                    // Highlight the player placement zones
                    for (let player of this.board.players) {
                        if (this.board.isPlacementSpace(space, player) && this.board.gameState === GameState.Placement) {
                            if (player === this.board.currentPlayer) {
                                elem.addClass("placement_0");
                            } else {
                                elem.addClass("placement_1");
                            }
                        }
                    }

                    // Highlight revealed pieces
                    if (space.piece && space.piece.revealed) {
                        elem.addClass("revealed");
                    }

                    content = this.showPiece(space.piece);
                }

                elem.html(content);
            }
        }
    }

    public updateSidebars(): void {
        let sidebarId = "";
        let numCols = this.getNumSideCols();

        for (let player of this.board.players) {
            let pieces = this.board.sortPieces(player.getPieces());
            let space = 0;
            sidebarId = (player === this.board.currentPlayer) ? "#leftSide" : "#rightSide";

            for (let row = 0; row < this.board.boardSize; row++) {
                for (let col = 0; col < numCols; col++) {
                    if (!pieces[space]) pieces[space] = null;

                    let elem = $(sidebarId+'_'+row+'_'+col);
                    elem.removeClass("player_0 player_1");

                    // Disable dragging if this is an empty space, or we're in play
                    if (pieces[space] === null || this.board.gameState !== GameState.Placement) {
                        elem.attr("draggable", "false");
                    } else if (sidebarId === "#leftSide") {
                        elem.attr("draggable", "true");
                    }

                    // Highlight player pieces
                    if (pieces[space] && pieces[space].player) {
                        if (pieces[space].player === this.board.currentPlayer) {
                            elem.addClass("player_0");
                        } else {
                            elem.addClass("player_1");
                        }
                    }

                    elem.html(this.showPiece(pieces[space], true));
                    space++;
                }
            }
        }
    }

    public updateFooter(): void {
        $("#restartGame").hide();
        $("#winner").hide();
        $("#turn").hide();
        $("#autoComplete").hide();
        $("#randomize").hide();
        $("#startGame").hide();
        $("#stats").hide();
        $("#restartGame").hide();
        $("#moves").hide();

        $("#startGame").removeAttr("disabled");
        $("#multiplayerStatus").text("");

        if (this.board.gameState === GameState.Placement) {
            $("#autoComplete").show();
            $("#randomize").show();
            $("#startGame").show();

            if (this.board.currentPlayer.getPieces().length !== 0) {
                $("#startGame").attr("disabled", "true");
            }
        } else if (this.board.gameState === GameState.Ready) {
            if (this.board.isMultiplayer()) {
                $("#multiplayerStatus").text("Waiting for Player " + this.board.opposingPlayer.num);
            }
        } else if (this.board.gameState === GameState.Play) {
            $("#stats").show();
            $("#numMoves").text(this.board.numMoves);

            $("#turn").show();
            $("#turnNum").text("Player " + this.board.turn.num);

            if (this.board.turn.num === this.board.currentPlayer.num) {
                $("#turn").removeClass("alert-info");
                $("#turn").addClass("alert-success");
            } else {
                $("#turn").removeClass("alert-success");
                $("#turn").addClass("alert-info");
            }

            if (!this.board.isMultiplayer()) {
                $("#restartGame").show();
                $("#moves").show();
            }
        } else if (this.board.gameState === GameState.End) {
            $("#stats").show();
            $("#numMoves").text(this.board.numMoves);

            $("#winner").show();

            if (this.board.winner) {
                $("#winnerNum").text("Player " + this.board.winner.num);

                if (this.board.winner.num === this.board.currentPlayer.num) {
                    $("#winner").removeClass("alert-info");
                    $("#winner").addClass("alert-success");
                } else {
                    $("#winner").removeClass("alert-success");
                    $("#winner").addClass("alert-info");
                }
            } else {
                $("#winnerNum").text("Draw");
            }

            if (!this.board.isMultiplayer()) {
                $("#restartGame").show();
                $("#moves").show();
            }
        }
    }

    // Using instance arrow function to ensure we keep the right 'this' - https://github.com/Microsoft/TypeScript/wiki/'this'-in-TypeScript
    public update = (): void => {
        this.updateBoard();
        this.updateSidebars();
        this.updateFooter();
    }

    public pickupPiece(event: any): void {
        event.dataTransfer.setData("text/plain", event.target.id);
        event.dataTransfer.dropEffect = "move";
    }

    public dragPiece(event: any): void {
        event.preventDefault();
        event.dataTransfer.dropEffect = "move";
    }

    // Using instance arrow function to ensure we keep the right 'this' - https://github.com/Microsoft/TypeScript/wiki/'this'-in-TypeScript
    public dropPiece = (event: any): void => {
        event.preventDefault();

        let originId = "#" + event.dataTransfer.getData("text/plain");
        let targetId = (event.target.id) ? event.target.id : event.target.parentElement.id;

        let originCoordParts = originId.split("_");
        let originCoords: [number, number] = [parseInt(originCoordParts[1]), parseInt(originCoordParts[2])];

        let targetCoordParts = targetId.split("_");
        let targetCoords: [number, number] = [parseInt(targetCoordParts[1]), parseInt(targetCoordParts[2])];

        this.board.movePiece(originId, originCoords, targetCoords, this.update);
        this.update();
    }

    public showPiece(piece: Piece, sidebar: boolean = false): string {
        let sidebarStates = [GameState.Play, GameState.End];

        // If a valid piece and owned by the current player (or captured), show it's details
        if (piece) {
            if (piece.player === this.board.currentPlayer || (sidebar && sidebarStates.indexOf(this.board.gameState) !== -1) || piece.revealed) {
                return '<div class="rank">'+piece.rank+'</div><div class="rankName">'+piece.rankName+'</div>';
            } else {
                return "?";
            }
        }

        return "";
    }

    private getNumSideCols(): number {
        let numPieces = this.board.currentPlayer.pieces.length;
        return numPieces/this.board.boardSize;
    }

    private drawTable(id: string, idPrefix: string, rows: number, cols: number, draggable: boolean = false): void {
        for (let row = 0; row < rows; row++) {
            let rowId = idPrefix + "_row_" + row;
            $(id).append('<div class="row" id="'+rowId+'"></div>');

            for (let col = 0; col < cols; col++) {
                let spaceId = idPrefix + "_" + row + "_" + col;
                $('#'+rowId).append('<div id="'+spaceId+'"></div>');

                let elem = $('#'+spaceId);
                elem.addClass("space row_"+row+" col_"+col);

                if (draggable === true) {
                    elem.attr("draggable", "true");
                    elem.attr("ondragstart", "game.pickupPiece(event)");
                }
            }
        }
    }
}
