import Piece from "./piece";
import PieceFactory from "./pieceFactory";
import Space from "./space";

export default class Player {
    private _num: number;
    private _pieces: Piece[];

    public static readonly numPieces: [string, number][] = [
        ["Marshall", 1],
        ["General", 1],
        ["Colonel", 2],
        ["Major", 3],
        ["Captain", 4],
        ["Lieutenant", 4],
        ["Sergeant", 4],
        ["Miner", 5],
        ["Scout", 8],
        ["Spy", 1],
        ["Bomb", 6],
        ["Flag", 1]
    ];

    constructor(playerNum: number) {
        this._num = playerNum;
        this._pieces = [];
        let token = 0;

        // As per the configuration above, initialise those pieces
        for (let pieceConfig of Player.numPieces) {
            for (let num = 0; num < pieceConfig[1]; num++) {
                let piece = PieceFactory.getInstance(pieceConfig[0], this, token.toString());
                this._pieces.push(piece);
                token++;
            }
        }
    }

    public get num(): number {
        return this._num;
    }

    public set num(num: number) {
        this._num = num;
    }

    public get pieces(): Piece[] {
        return this._pieces;
    }

    public findPiece(pieceToken: string): Piece {
        for (let piece of this.pieces) {
            if (piece.token === pieceToken) {
                return piece;
            }
        }

        return null;
    }

    public updatePieces(pieces: any, spaces: Space[][]): void {
        this._pieces = [];

        for (let pieceConfig of pieces) {
            let piece = PieceFactory.getInstance(pieceConfig.type, this, pieceConfig.token);
            this._pieces.push(piece);

            if (pieceConfig.row !== null && pieceConfig.col !== null) {
                spaces[pieceConfig.row][pieceConfig.col].addPiece(piece);
            }
        }
    }

    public getPieces(placed: boolean = false): Piece[] {
        let pieces: Piece[] = [];

        for (let piece of this._pieces) {
            if ((placed && piece.space) || (!placed && !piece.space)) {
                pieces.push(piece);
            }
        }

        return pieces;
    }
}
