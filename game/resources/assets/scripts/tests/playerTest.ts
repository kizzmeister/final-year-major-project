import Player from "../player";
import Piece from "../piece";
import Space from "../space";

export default class PlayerTest {
    public static run(): void {
        QUnit.module("player.ts");

        QUnit.test("get/set num", function(assert) {
            let player1 = new Player(1);
            assert.equal(player1.num, 1);

            let player2 = new Player(2);
            assert.equal(player2.num, 2);

            player2.num = 5;
            assert.equal(player2.num, 5);
        });

        QUnit.test("get pieces", function(assert) {
            let player = new Player(1);
            let numPieces = 0;
            for (let piece of Player.numPieces) {
                numPieces += piece[1];
            }

            // Check the number of generated pieces is correct
            assert.equal(player.pieces.length, numPieces);
            assert.ok(player.pieces instanceof Array);
            assert.ok(player.pieces[0] instanceof Piece);
        });

        QUnit.test("findPiece", function(assert) {
            let player = new Player(1);
            for (let pieceIndex in player.pieces) {
                let piece = player.pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            assert.equal(player.findPiece(""), null);
            assert.equal(player.findPiece("test"), null);
            assert.equal(player.findPiece("test0"), player.pieces[0]);
            assert.equal(player.findPiece("test39"), player.pieces[39]);
        });

        QUnit.test("updatePieces", function(assert) {
            let player = new Player(1);
            let spaces = [
                [new Space([0,0]), new Space([0,1]), new Space([0,2])],
                [new Space([1,0]), new Space([1,1]), new Space([1,2])],
                [new Space([2,0]), new Space([2,1]), new Space([2,2])],
            ];

            let pieceConfig: any = [
                {"token":"test1","type":"Marshall","row":null,"col":null},
                {"token":"test1","type":"General","row":1,"col":0},
                {"token":"test1","type":"Colonel","row":2,"col":2}
            ];

            player.updatePieces(pieceConfig, spaces);

            assert.equal(player.pieces[0].space, null);
            assert.equal(player.pieces[1].space, spaces[1][0]);
            assert.equal(player.pieces[2].space, spaces[2][2]);

            assert.equal(spaces[1][0].piece, player.pieces[1]);
            assert.equal(spaces[2][2].piece, player.pieces[2]);
        });

        QUnit.test("getPieces", function(assert) {
            let player = new Player(1);

            // First check that all pieces are initially unplaced
            assert.deepEqual(player.getPieces(), player.pieces);
            assert.deepEqual(player.getPieces(true), []);

            // Place a few pieces
            player.pieces[0].space = new Space([0,0]);
            player.pieces[1].space = new Space([0,0]);
            player.pieces[2].space = new Space([0,0]);

            // Then check that those pieces are now returned as nulls
            let unplacedPieces = player.getPieces();
            let placedPieces = player.getPieces(true);

            assert.equal(unplacedPieces[0], player.pieces[3]);
            assert.equal(placedPieces[0], player.pieces[0]);
            assert.equal(unplacedPieces.length, player.pieces.length - 3);
            assert.equal(placedPieces.length, 3);
        });
    }
}
