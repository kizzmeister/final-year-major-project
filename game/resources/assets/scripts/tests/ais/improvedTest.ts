import Board from "../../board";
import Player from "../../player";
import Piece from "../../piece";
import Space from "../../space";
import ImprovedAI from "../../ais/improved";

export default class ImprovedAiTest {
    public static run(): void {
        QUnit.module("ais/improved.ts");

        QUnit.test("placePieces", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new ImprovedAI(board);
            let disallowedFlagSpaces: [number, number][] = [
                [3,0], [3,1], [3,4], [3,5], [3,8], [3,9],
                [6,0], [6,1], [6,4], [6,5], [6,8], [6,9]
            ];

            ai.placePieces(players[1]);

            assert.equal(players[1].getPieces().length, 0);
            assert.notEqual(board.spaces[0][0].piece, null);
            assert.notOk(ImprovedAiTest.coordsInArray(
                players[1].pieces[39].space.coords,
                disallowedFlagSpaces
            ));
        });

        QUnit.test("getMovement", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new ImprovedAI(board);

            board.spaces[0][0].addPiece(players[0].pieces[0]);
            board.spaces[0][1].addPiece(players[1].pieces[0]);
            board.spaces[9][0].addPiece(players[1].pieces[1]);

            let movements: [Piece, Space][] = [
                [players[0].pieces[0], board.spaces[0][1]],
                [players[0].pieces[0], board.spaces[8][0]]
            ];
            assert.deepEqual(ai.getMovement(players[1], movements), movements[0]);
        });

        QUnit.test("getFlagSpace", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new ImprovedAI(board);

            let disallowedFlagSpaces: [number, number][] = [
                [3,0], [3,1], [3,4], [3,5], [3,8], [3,9],
                [6,0], [6,1], [6,4], [6,5], [6,8], [6,9]
            ];

            let flagSpace = ai.getFlagSpace(board.getPossiblePlacementSpaces(players[0]));
            assert.notOk(ImprovedAiTest.coordsInArray(flagSpace.coords, disallowedFlagSpaces));

            flagSpace = ai.getFlagSpace([
                new Space([6,0]),
                new Space([9,9])
            ]);
            assert.deepEqual(flagSpace.coords, [9,9]);
        });

        QUnit.test("getWinFactor", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new ImprovedAI(board);

            // Initially, no moves are productive
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][4]]), Number.MAX_VALUE);

            board.spaces[1][1].addPiece(players[0].pieces[0]);
            board.spaces[7][3].addPiece(players[0].pieces[1]);
            board.spaces[9][9].addPiece(players[0].pieces[3]);

            board.spaces[2][4].addPiece(players[1].pieces[1]);
            board.spaces[6][4].addPiece(players[1].pieces[2]);

            // Horizontal from first opposing piece
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][4]]), 0);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][3]]), 1);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][2]]), 2);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][1]]), 3);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][0]]), 4);

            // Horizontal from second opposing piece
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[6][4]]), 0);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[6][3]]), 1);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[6][2]]), 2);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[6][1]]), 3);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[6][0]]), 4);

            // Vertical between opposing pieces
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[3][4]]), 1);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[4][4]]), 2);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[5][4]]), 1);

            // Diaganal from opposing pieces
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[7][5]]), 1);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[8][6]]), 2);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[9][7]]), 3);

            // Corners
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[0][0]]), 4);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[0][9]]), 5);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[9][0]]), 4);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[0][9]]), 5);

            // Revealed weaker piece
            ai.handleReveal(players[1].pieces[1]);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][4]]), 0);
            assert.equal(ai.getWinFactor([players[0].pieces[0], board.spaces[2][3]]), 1);

            // Revealed similar piece
            assert.equal(ai.getWinFactor([players[0].pieces[1], board.spaces[2][4]]), 0);
            assert.equal(ai.getWinFactor([players[0].pieces[1], board.spaces[2][3]]), 1);

            // Revealed stronger piece
            assert.equal(ai.getWinFactor([players[0].pieces[3], board.spaces[2][4]]), Number.MAX_VALUE);
            assert.equal(ai.getWinFactor([players[0].pieces[3], board.spaces[2][3]]), 4);
        });

        QUnit.test("getRadialCoords", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new ImprovedAI(board);

            let space = board.spaces[5][5];
            assert.deepEqual(ai.getRadialCoords(space, 1), [
                [4,4], [4,5], [4,6], [5,6], [6,6], [6,5], [6,4], [5,4]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 2), [
                [3,3], [3,4], [3,5], [3,6], [3,7], [4,7], [5,7], [6,7],
                [7,7], [7,6], [7,5], [7,4], [7,3], [6,3], [5,3], [4,3]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 3), [
                [2,2], [2,3], [2,4], [2,5], [2,6], [2,7], [2,8], [3,8],
                [4,8], [5,8], [6,8], [7,8], [8,8], [8,7], [8,6], [8,5],
                [8,4], [8,3], [8,2], [7,2], [6,2], [5,2], [4,2], [3,2]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 4), [
                [1,1], [1,2], [1,3], [1,4], [1,5], [1,6], [1,7], [1,8],
                [1,9], [2,9], [3,9], [4,9], [5,9], [6,9], [7,9], [8,9],
                [9,9], [9,8], [9,7], [9,6], [9,5], [9,4], [9,3], [9,2],
                [9,1], [8,1], [7,1], [6,1], [5,1], [4,1], [3,1], [2,1]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 5), [
                [0,0], [0,1], [0,2], [0,3], [0,4], [0,5], [0,6], [0,7],
                [0,8], [0,9], [9,0], [8,0], [7,0], [6,0], [5,0], [4,0],
                [3,0], [2,0], [1,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 6), []);

            space = board.spaces[0][0];
            assert.deepEqual(ai.getRadialCoords(space, 1), [
                [0,1], [1,1], [1,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 2), [
                [0,2], [1,2], [2,2], [2,1], [2,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 3), [
                [0,3], [1,3], [2,3], [3,3], [3,2], [3,1], [3,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 4), [
                [0,4], [1,4], [2,4], [3,4], [4,4], [4,3], [4,2], [4,1],
                [4,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 5), [
                [0,5], [1,5], [2,5], [3,5], [4,5], [5,5], [5,4], [5,3],
                [5,2], [5,1], [5,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 6), [
                [0,6], [1,6], [2,6], [3,6], [4,6], [5,6], [6,6], [6,5],
                [6,4], [6,3], [6,2], [6,1], [6,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 7), [
                [0,7], [1,7], [2,7], [3,7], [4,7], [5,7], [6,7], [7,7],
                [7,6], [7,5], [7,4], [7,3], [7,2], [7,1], [7,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 8), [
                [0,8], [1,8], [2,8], [3,8], [4,8], [5,8], [6,8], [7,8],
                [8,8], [8,7], [8,6], [8,5], [8,4], [8,3], [8,2], [8,1],
                [8,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 9), [
                [0,9], [1,9], [2,9], [3,9], [4,9], [5,9], [6,9], [7,9],
                [8,9], [9,9], [9,8], [9,7], [9,6], [9,5], [9,4], [9,3],
                [9,2], [9,1], [9,0]
            ]);
            assert.deepEqual(ai.getRadialCoords(space, 10), []);
        });
    }

    private static coordsInArray(coords: [number, number], array: [number, number][]): boolean {
        for (let c of array) {
            if (coords[0] === c[0] && coords[1] === c[1]) return true;
        }

        return false;
    }
}
