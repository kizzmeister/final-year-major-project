import Board from "../../board";
import Player from "../../player";
import Piece from "../../piece";
import Space from "../../space";
import RandomAI from "../../ais/random";

export default class RandomAiTest {
    public static run(): void {
        QUnit.module("ais/random.ts");

        QUnit.test("placePieces", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            ai.placePieces(players[1]);

            assert.equal(players[1].getPieces().length, 0);
            assert.notEqual(board.spaces[0][0].piece, null);
        });

        QUnit.test("getMovement", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            let movements: [Piece, Space][] = [[players[0].pieces[0], board.spaces[0][0]]];
            assert.deepEqual(ai.getMovement(players[1], movements), movements[0]);
        });
    }
}
