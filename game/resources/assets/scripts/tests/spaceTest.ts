import Space from "../space";
import Player from "../player";
import Marshall from "../pieces/1_marshall";
import General from "../pieces/2_general";

export default class SpaceTest {
    public static run(): void {
        QUnit.module("space.ts");

        QUnit.test("addPiece", function(assert) {
            let player = new Player(1);
            let piece1 = new Marshall(player);
            let piece2 = new General(player);

            // Add a piece to an empty space
            let space = new Space([0,0]);
            assert.equal(space.piece, null);
            space.addPiece(piece1);
            assert.equal(space.piece, piece1);

            // Add another piece to the now occupied space
            space.addPiece(piece2);
            assert.equal(space.piece, piece2);

            // Attempt to add a piece to a disabled space
            let disabledSpace = new Space([0,0], true);
            assert.throws(function() {
                disabledSpace.addPiece(piece1);
            }, "Unable to add piece to disabled space");
        });

        QUnit.test("removePiece", function(assert) {
            let player = new Player(1);
            let piece = new Marshall(player);

            // Remove a piece from an empty space
            let space = new Space([0,0]);
            assert.equal(space.piece, null);
            space.removePiece();
            assert.equal(space.piece, null);

            // Remove a piece from an occupied space
            space.addPiece(piece);
            assert.equal(space.piece, piece);
            space.removePiece();
            assert.equal(space.piece, null);

            // Attempt to remove a piece from a disabled space
            let disabledSpace = new Space([0,0], true);
            assert.throws(function() {
                disabledSpace.removePiece();
            }, "Unable to remove piece to disabled space");
        });

        QUnit.test("get coords", function(assert) {
            let space = new Space([0,0]);
            assert.deepEqual(space.coords, [0,0]);

            space = new Space([10,10]);
            assert.deepEqual(space.coords, [10,10]);
        });

        QUnit.test("get disabled", function(assert) {
            let space = new Space([0,0]);
            let disabledSpace = new Space([0,0], true);

            assert.notOk(space.disabled);
            assert.ok(disabledSpace.disabled);
        });
    }
}
