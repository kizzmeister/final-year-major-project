import Board from "../board";
import { GameState } from "../board";
import Player from "../player";
import Space from "../space";

import Marshall from "../pieces/1_marshall";
import General from "../pieces/2_general";
import Colonel from "../pieces/3_colonel";
import Major from "../pieces/4_major";
import Captain from "../pieces/5_captain";
import Lieutenant from "../pieces/6_lieutenant";
import Sergeant from "../pieces/7_sergeant";
import Miner from "../pieces/8_miner";
import Scout from "../pieces/9_scout";
import Spy from "../pieces/10_spy";
import Bomb from "../pieces/0_bomb";
import Flag from "../pieces/11_flag";
import Unknown from "../pieces/12_unknown";

export default class BoardTest {
    public static run(): void {
        QUnit.module("board.ts");

        QUnit.test("get players", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            assert.deepEqual(board.players, players);
        });

        QUnit.test("get spaces", function(assert) {
            let board = new Board([new Player(1)]);

            assert.ok(board.spaces instanceof Array);
            assert.ok(board.spaces[0] instanceof Array);
            assert.ok(board.spaces[0][0] instanceof Space);

            assert.equal(board.spaces.length, board.boardSize);
            assert.equal(board.spaces[0].length, board.boardSize);
        });

        QUnit.test("get/set gameState", function(assert) {
            let board = new Board([new Player(1)]);

            assert.equal(board.gameState, GameState.Placement);
            board.gameState = GameState.Play;
            assert.equal(board.gameState, GameState.Play);
        });

        QUnit.test("get gameToken", function(assert) {
            let board = new Board([new Player(1), new Player(2)], "", "gameToken");

            assert.equal(board.gameToken, "gameToken");
        });

        QUnit.test("isMultiplayer", function(assert) {
            let board1 = new Board([new Player(1), new Player(2)]);
            assert.notOk(board1.isMultiplayer());

            let board2 = new Board([new Player(1), new Player(2)], "random", "");
            assert.notOk(board2.isMultiplayer());

            let board3 = new Board([new Player(1), new Player(2)], "", "test");
            assert.ok(board3.isMultiplayer());
        });

        QUnit.test("get/set turn", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            assert.notEqual(players.indexOf(board.turn), -1);

            board.turn = players[1];
            assert.equal(board.turn, players[1]);
        });

        QUnit.test("get/set winner", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            assert.equal(board.winner, null);
            board.winner = players[0];
            assert.equal(board.winner, players[0]);
        });

        QUnit.test("get numMoves", function(assert) {
            let board = new Board([new Player(1)]);

            assert.equal(board.numMoves, 0);
        });

        QUnit.test("get/set disabled", function(assert) {
            let board = new Board([new Player(1)]);

            assert.notOk(board.disabled);
            board.disabled = true;
            assert.ok(board.disabled);
        });

        QUnit.test("increment/decrement NumMoves", function(assert) {
            let board = new Board([new Player(1)]);

            board.incrementNumMoves();
            assert.equal(board.numMoves, 1);

            board.incrementNumMoves();
            board.incrementNumMoves();
            assert.equal(board.numMoves, 3);

            board.decrementNumMoves();
            assert.equal(board.numMoves, 2);

            board.decrementNumMoves();
            board.decrementNumMoves();
            assert.equal(board.numMoves, 0);
        });

        QUnit.test("getPlacementSpaceRange", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            // Test the range for the current player, as well as player 1 and 2
            let placementSpaceRanges = [
                board.getPlacementSpaceRange(),
                board.getPlacementSpaceRange(players[0]),
                board.getPlacementSpaceRange(players[1])
            ];

            assert.deepEqual(board.placementSpaceRange[0], placementSpaceRanges[0]);
            assert.deepEqual(board.placementSpaceRange[0], placementSpaceRanges[1]);
            assert.deepEqual(board.placementSpaceRange[1], placementSpaceRanges[2]);
        });

        QUnit.test("isPlacementSpace", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let placementSpaceRange = board.placementSpaceRange;

            for (let rangeIndex in placementSpaceRange) {
                let range = placementSpaceRange[rangeIndex];

                let belowRangeSpace = new Space([(range[0][0]-1), (range[0][1]-1)]);
                let bottomRangeSpace = new Space(range[0]);
                let midRangeSpace = new Space([(range[0][0]+1), (range[0][1]+1)]);
                let topRangeSpace = new Space(range[1]);
                let aboveRangeSpace = new Space([range[1][0]+1, range[1][1]+1]);

                assert.notOk(board.isPlacementSpace(belowRangeSpace, players[rangeIndex]));
                assert.ok(board.isPlacementSpace(bottomRangeSpace, players[rangeIndex]));
                assert.ok(board.isPlacementSpace(midRangeSpace, players[rangeIndex]));
                assert.ok(board.isPlacementSpace(topRangeSpace, players[rangeIndex]));
                assert.notOk(board.isPlacementSpace(aboveRangeSpace, players[rangeIndex]));
            }
        });

        QUnit.test("getPossiblePlacementSpaces", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let placementRange = board.placementSpaceRange;
            let possibleSpaces = [
                board.getPossiblePlacementSpaces(),
                board.getPossiblePlacementSpaces(players[0]),
                board.getPossiblePlacementSpaces(players[1])
            ];

            for (let spaceIndex in possibleSpaces) {
                let spaces = possibleSpaces[spaceIndex];
                let correctRange = placementRange[0];
                let incorrectRange = placementRange[1];

                // Switch the comparison ranges to player2 when necessary
                if (parseInt(spaceIndex) === 2) {
                    correctRange = placementRange[1];
                    incorrectRange = placementRange[0];
                }

                assert.ok(BoardTest.coordsInSpace(correctRange[0], spaces));
                assert.ok(BoardTest.coordsInSpace(correctRange[1], spaces));
                assert.notOk(BoardTest.coordsInSpace(incorrectRange[0], spaces));
                assert.notOk(BoardTest.coordsInSpace(incorrectRange[1], spaces));
            }
        });

        QUnit.test("handleMultiplayerData", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            board.handleMultiplayerData(2, null, [
                {
                    "player": 2,
                    "turn": false,
                    "pieces": [
                        {"token": "piece1", "type": "Spy", "row": null, "col": null},
                        {"token": "piece2", "type": "Scout", "row": 1, "col": 1},
                        {"token": "piece3", "type": "Sergeant", "row": 2, "col": 2},
                    ]
                },
                {
                    "player": 5,
                    "turn": true,
                    "pieces": [
                        {"token": "piece1", "type": "Spy", "row": 1, "col": 2},
                        {"token": "piece2", "type": "Scout", "row": 2, "col": 3},
                        {"token": "piece3", "type": "Sergeant", "row": null, "col": null},
                    ]
                }
            ]);

            assert.equal(board.currentPlayer.num, 2);
            assert.equal(board.opposingPlayer.num, 5);
            assert.equal(board.turn, board.opposingPlayer);
            assert.ok(board.disabled);

            assert.equal(board.spaces[1][1].piece.player, board.currentPlayer);
            assert.equal(board.spaces[1][1].piece.token, "piece2");
            assert.equal(board.spaces[2][2].piece.player, board.currentPlayer);
            assert.equal(board.spaces[2][2].piece.token, "piece3");

            assert.equal(board.spaces[1][2].piece.player, board.opposingPlayer);
            assert.equal(board.spaces[1][2].piece.token, "piece1");
            assert.equal(board.spaces[2][3].piece.player, board.opposingPlayer);
            assert.equal(board.spaces[2][3].piece.token, "piece2");
        });

        QUnit.test("movePiece (board move)", function(assert) {
            assert.expect(4);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.gameState = GameState.Play;
            board.turn = players[0];

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);

            board.movePiece("#board", [0,0], [0,1], () => {assert.ok(true)}, 10);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, piece);
        });

        QUnit.test("movePiece (board combat)", function(assert) {
            assert.expect(8);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.gameState = GameState.Play;
            board.turn = players[0];

            let pieces = [
                new Marshall(players[0]),
                new Scout(players[1]),
                new Scout(players[1])
            ];
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.spaces[9][9].addPiece(pieces[2]);

            let done = assert.async();
            let callbackCount = 0;
            board.movePiece(
                "#board",
                [0,0],
                [0,1],
                () => {
                    if (callbackCount === 0) {
                        assert.equal(board.spaces[0][0].piece, null);
                        assert.equal(board.spaces[0][1].piece, pieces[0]);
                        assert.ok(board.disabled);
                    } else {
                        if (callbackCount === 2) {
                            assert.notOk(board.disabled);
                            done();
                        } else {
                            assert.ok(board.disabled);
                        }
                    }
                    callbackCount++;
                },
                10
            );

            assert.equal(board.spaces[0][0].piece, pieces[0]);
            assert.equal(board.spaces[0][1].piece, pieces[1]);
            assert.ok(board.disabled);
        });


        QUnit.test("movePiece (sidebar)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.turn = null;

            let piece = players[0].pieces[0];

            board.movePiece("#leftSide", [0,0], [6,0], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[6][0].piece, piece);
        });

        QUnit.test("movePiece (board in placement)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.turn = null;

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);

            board.movePiece("#board", [0,0], [6,0], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[6][0].piece, piece);
        });

        QUnit.test("movePiece (board disabled)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.gameState = GameState.Play;
            board.disabled = true;
            board.turn = players[0];

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);

            board.movePiece("#board", [0,0], [0,1], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[0][1].piece, null);
        });

        QUnit.test("movePiece (target space disabled)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.gameState = GameState.Play;
            board.turn = players[0];

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);
            board.spaces[0][1] = new Space([0,1], true);

            board.movePiece("#board", [0,0], [0,1], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[0][1].piece, null);
        });

        QUnit.test("movePiece (not our turn)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.gameState = GameState.Play;
            board.turn = players[1];

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);

            board.movePiece("#board", [0,0], [0,1], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[0][1].piece, null);
        });

        QUnit.test("movePiece (invalid placement space)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.turn = players[0];

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);

            board.movePiece("#board", [0,0], [5,0], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[5][0].piece, null);
        });

        QUnit.test("movePiece (game ended)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            board.gameState = GameState.End;
            board.turn = players[0];

            let piece = new Marshall(players[0]);
            board.spaces[0][0].addPiece(piece);

            board.movePiece("#board", [0,0], [0,1], () => {assert.ok(false)}, 1);

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[0][1].piece, null);
        });

        QUnit.test("movePieceOnBoard (normal)", function(assert) {
            assert.expect(11);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Marshall(players[0]),
                new Scout(players[1]),
                new Scout(players[1])
            ];

            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.spaces[9][9].addPiece(pieces[2]);
            board.gameState = GameState.Play;

            let done = assert.async();
            let callbackCount = 0;
            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][1],
                () => {
                    if (callbackCount === 0) {
                        assert.equal(board.spaces[0][0].piece, null);
                        assert.equal(board.spaces[0][1].piece, pieces[0]);
                        assert.notOk(pieces[0].revealed);
                        assert.notOk(pieces[1].revealed);
                    } else if (callbackCount === 1) {
                        assert.ok(board.disabled);
                    } else {
                        assert.notOk(board.disabled);
                        done();
                    }
                    callbackCount++;
                },
                10
            );

            assert.equal(board.spaces[0][0].piece, pieces[0]);
            assert.equal(board.spaces[0][1].piece, pieces[1]);

            assert.ok(pieces[0].revealed);
            assert.ok(pieces[1].revealed);
            assert.notEqual(board.numMoves, 0);
        });

        QUnit.test("movePieceOnBoard (opposing pieces)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let piece = new Marshall(players[1]);

            // Unable to move opposing pieces
            board.spaces[0][0].addPiece(piece);
            board.gameState = GameState.Play;

            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][1],
                () => {assert.ok(false)}, // Will cause an error if run
                1
            );

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[0][1].piece, null);
        });

        QUnit.test("movePieceOnBoard (invalid move)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let piece = new Marshall(players[0]);

            // Unable to move if not valid
            board.spaces[0][0].addPiece(piece);
            board.gameState = GameState.Play;

            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][9],
                () => {assert.ok(false)}, // Will cause an error if run
                1
            );

            assert.equal(board.spaces[0][0].piece, piece);
            assert.equal(board.spaces[0][9].piece, null);
        });

        QUnit.test("movePieceOnBoard (invalid move when placing)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let piece = new Marshall(players[0]);

            // Able to move if not valid, but in placement stage
            board.spaces[0][0].addPiece(piece);
            board.gameState = GameState.Placement;

            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][9],
                () => {assert.ok(false)}, // Will cause an error if run
                1
            );

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][9].piece, piece);
        });

        QUnit.test("movePieceOnBoard (take own piece)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Marshall(players[0]),
                new Scout(players[0])
            ];

            // Unable to take own piece if in play
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.gameState = GameState.Play;

            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][1],
                () => {assert.ok(false)}, // Will cause an error if run
                1
            );

            assert.equal(board.spaces[0][0].piece, pieces[0]);
            assert.equal(board.spaces[0][1].piece, pieces[1]);
        });

        QUnit.test("movePieceOnBoard (scouts move to nearest piece before combat - single space)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Scout(players[0]),
                new Spy(players[1]),
                new Scout(players[1])
            ];

            // Scouts move to nearest piece before combat
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.spaces[9][9].addPiece(pieces[2]);
            board.gameState = GameState.Play;

            let done = assert.async();
            let callbackCount = 0;
            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][1],
                () => {
                    if (callbackCount === 0) {
                        assert.equal(board.spaces[0][0].piece, null);
                        assert.equal(board.spaces[0][1].piece, pieces[0]);
                    } else if (callbackCount === 1) {
                        assert.ok(true);
                    } else {
                        assert.ok(true);
                        done();
                    }
                    callbackCount++;
                },
                10
            );

            assert.equal(board.spaces[0][0].piece, pieces[0]);
            assert.equal(board.spaces[0][1].piece, pieces[1]);
        });

        QUnit.test("movePieceOnBoard (scouts move to nearest piece before combat - multiple spaces)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Scout(players[0]),
                new Spy(players[1]),
                new Scout(players[1])
            ];

            // Scouts move to nearest piece before combat
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][9].addPiece(pieces[1]);
            board.spaces[9][9].addPiece(pieces[2]);
            board.gameState = GameState.Play;

            let done = assert.async();
            let callbackCount = 0;
            board.movePieceOnBoard(
                board.spaces[0][0],
                board.spaces[0][9],
                () => {
                    if (callbackCount === 0) {
                        assert.equal(board.spaces[0][8].piece, null);
                        assert.equal(board.spaces[0][9].piece, pieces[0]);
                    } else if (callbackCount === 1) {
                        assert.ok(true);
                    } else {
                        assert.ok(true);
                        done();
                    }
                    callbackCount++;
                },
                10
            );

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][8].piece, pieces[0]);
            assert.equal(board.spaces[0][9].piece, pieces[1]);
        });

        QUnit.test("getCombatSpace (empty)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            let origin = board.spaces[0][0];
            let target = board.spaces[0][1];

            assert.equal(board.getCombatSpace(origin, target), origin);
        });

        QUnit.test("getCombatSpace (normal)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            let origin = board.spaces[0][0];
            let target = board.spaces[0][1];

            origin.addPiece(new Marshall(players[0]));
            target.addPiece(new General(players[1]));

            assert.equal(board.getCombatSpace(origin, target), origin);
        });

        QUnit.test("getCombatSpace (scout - single space)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Scout(players[0]),
                new General(players[1])
            ];

            let origin = board.spaces[0][0];
            let target = board.spaces[0][1];

            origin.addPiece(pieces[0]);
            target.addPiece(pieces[1]);

            assert.equal(board.getCombatSpace(origin, target), origin);
            assert.equal(origin.piece, pieces[0]);
            assert.equal(target.piece, pieces[1]);
        });

        QUnit.test("getCombatSpace (scout - multiple space)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Scout(players[0]),
                new General(players[1])
            ];

            let origin = board.spaces[0][0];
            let target = board.spaces[0][9];

            origin.addPiece(pieces[0]);
            target.addPiece(pieces[1]);

            let combat = board.spaces[0][8];

            assert.equal(board.getCombatSpace(origin, target), combat);
            assert.equal(origin.piece, null);
            assert.equal(combat.piece, pieces[0]);
            assert.equal(target.piece, pieces[1]);
        });

        QUnit.test("handleCombat (defeat)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Scout(players[0]),
                new Marshall(players[1]),
                new Scout(players[1])
            ];

            // Piece defeated (Scout -> Marshall)
            board.gameState = GameState.Play;
            pieces[0].revealed = true;
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.spaces[0][2].addPiece(pieces[2]);

            board.handleCombat(board.spaces[0][0], board.spaces[0][1]);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, pieces[1]);
            assert.notOk(board.spaces[0][1].piece.revealed);
            assert.equal(board.gameState, GameState.Play);
        });

        QUnit.test("handleCombat (win)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Marshall(players[0]),
                new Scout(players[1]),
                new Scout(players[1]),
            ];

            // Piece wins (Marshall -> Scout)
            board.gameState = GameState.Play;
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.spaces[0][2].addPiece(pieces[2]);

            board.handleCombat(board.spaces[0][0], board.spaces[0][1]);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, pieces[0]);
            assert.equal(board.gameState, GameState.Play);
        });

        QUnit.test("handleCombat (draw)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Marshall(players[0]),
                new Marshall(players[1]),
                new Scout(players[0]),
                new Scout(players[1])
            ];

            // Pieces drawn (Marshall -> Marshall)
            board.gameState = GameState.Play;
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);
            board.spaces[0][2].addPiece(pieces[2]);
            board.spaces[0][3].addPiece(pieces[3]);

            board.handleCombat(board.spaces[0][0], board.spaces[0][1]);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, null);
            assert.equal(board.gameState, GameState.Play);
        });

        QUnit.test("handleCombat (flag capture)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let playerPieces = [
                [
                    new Marshall(players[0]),
                    new Flag(players[1]),
                    new Scout(players[1])
                ],
                [
                    new Marshall(players[1]),
                    new Flag(players[0]),
                    new Scout(players[0])
                ]
            ];

            for (let pieceIndex in playerPieces) {
                let pieces = playerPieces[pieceIndex];
                let board = new Board(players);

                // Game Ends when Flag Captured
                board.gameState = GameState.Play;
                board.turn = players[pieceIndex];

                board.spaces[0][0].addPiece(pieces[0]);
                board.spaces[0][1].addPiece(pieces[1]);
                board.spaces[0][2].addPiece(pieces[2]);

                board.handleCombat(board.spaces[0][0], board.spaces[0][1]);

                assert.equal(board.spaces[0][0].piece, null);
                assert.equal(board.spaces[0][1].piece, pieces[0]);
                assert.equal(board.gameState, GameState.End);
                assert.equal(board.winner, players[pieceIndex]);
            }
        });

        QUnit.test("handleCombat (captured all movable pieces)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let playerPieces = [
                [
                    new Marshall(players[0]),
                    new Scout(players[1])
                ],
                [
                    new Marshall(players[1]),
                    new Scout(players[0])
                ]
            ];

            for (let pieceIndex in playerPieces) {
                let pieces = playerPieces[pieceIndex];
                let board = new Board(players);

                // Game Ends when no more movable pieces
                board.gameState = GameState.Play;
                board.spaces[0][0].addPiece(pieces[0]);
                board.spaces[0][1].addPiece(pieces[1]);

                board.handleCombat(board.spaces[0][0], board.spaces[0][1]);

                assert.equal(board.spaces[0][0].piece, null);
                assert.equal(board.spaces[0][1].piece, pieces[0]);
                assert.equal(board.gameState, GameState.End);
                assert.equal(board.winner, players[pieceIndex]);
            }
        });

        QUnit.test("handleCombat (captured all movable pieces draw)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let pieces = [
                new Marshall(players[0]),
                new Marshall(players[1])
            ];

            // Game Ends when no more movable pieces
            board.gameState = GameState.Play;
            board.spaces[0][0].addPiece(pieces[0]);
            board.spaces[0][1].addPiece(pieces[1]);

            board.handleCombat(board.spaces[0][0], board.spaces[0][1]);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, null);
            assert.equal(board.gameState, GameState.End);
            assert.equal(board.winner, null);
        });

        QUnit.test("movePieceOnSide", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let space = board.spaces[0][0];

            assert.equal(space.piece, null);

            // Move Marshall
            board.movePieceOnSide([0,0], space);
            assert.equal(space.piece, board.currentPlayer.pieces[0]);

            // Move Miner (taking into account shifted spaces)
            board.movePieceOnSide([4,2], space);
            assert.equal(space.piece, board.currentPlayer.pieces[19]);

            // Move Bomb (taking into account shifted spaces)
            board.movePieceOnSide([9,1], space);
            assert.equal(space.piece, board.currentPlayer.pieces[38]);
        });

        QUnit.test("hasMovablePieces", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            // Initially, there will be no movable spaces
            assert.notOk(board.hasMovablePieces());
            assert.notOk(board.hasMovablePieces(players[0]));
            assert.notOk(board.hasMovablePieces(players[1]));

            // Even if a Bomb and/or a Flag is added, still no movable spaces
            board.spaces[0][0].addPiece(new Bomb(players[0]));
            board.spaces[0][1].addPiece(new Flag(players[0]));
            assert.notOk(board.hasMovablePieces());
            assert.notOk(board.hasMovablePieces(players[0]));
            assert.notOk(board.hasMovablePieces(players[1]));

            board.spaces[0][2].addPiece(new Bomb(players[1]));
            board.spaces[0][3].addPiece(new Flag(players[1]));
            assert.notOk(board.hasMovablePieces());
            assert.notOk(board.hasMovablePieces(players[0]));
            assert.notOk(board.hasMovablePieces(players[1]));

            // But if a Marshall is added, there will be movable spaces
            board.spaces[0][4].addPiece(new Marshall(players[0]));
            assert.ok(board.hasMovablePieces());
            assert.ok(board.hasMovablePieces(players[0]));
            assert.notOk(board.hasMovablePieces(players[1]));

            board.spaces[0][5].addPiece(new Marshall(players[1]));
            assert.ok(board.hasMovablePieces());
            assert.ok(board.hasMovablePieces(players[0]));
            assert.ok(board.hasMovablePieces(players[1]));
        });

        QUnit.test("automatePlacement (autoComplete when empty)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            for (let playerIndex in players) {
                let unplacedPieces = [];
                let placedPieces = [];

                let player = players[playerIndex];
                let placementRange = board.placementSpaceRange[playerIndex];
                let startRow = placementRange[0][0];
                let startCol = placementRange[0][1];
                let endRow = placementRange[1][0];
                let endCol = placementRange[1][1];

                // At this point there should be some unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space === null) unplacedPieces.push(piece);
                }
                assert.notEqual(unplacedPieces.length, 0);

                board.automatePlacement(false, player);

                // At this point there should be no unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space !== null) placedPieces.push(piece);
                }
                assert.deepEqual(placedPieces, unplacedPieces);

                // Also check that pieces have been placed correctly
                assert.equal(board.spaces[startRow][startCol].piece, unplacedPieces[0]);
                assert.equal(board.spaces[startRow][startCol+1].piece, unplacedPieces[1]);
                assert.equal(board.spaces[endRow][endCol].piece, unplacedPieces[unplacedPieces.length-1]);
            }
        });

        QUnit.test("automatePlacement (autoComplete partial)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            for (let playerIndex in players) {
                let unplacedPieces = [];
                let placedPieces = [];

                let player = players[playerIndex];
                let placementRange = board.placementSpaceRange[playerIndex];
                let startRow = placementRange[0][0];
                let startCol = placementRange[0][1];
                let endRow = placementRange[1][0];
                let endCol = placementRange[1][1];

                // At this point there should be some unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space === null) unplacedPieces.push(piece);
                }
                assert.notEqual(unplacedPieces.length, 0);

                // Add some pieces to the board
                board.spaces[startRow][startCol].addPiece(unplacedPieces[0]);
                board.spaces[startRow][startCol+2].addPiece(unplacedPieces[4]);
                board.spaces[endRow][endCol].addPiece(unplacedPieces[9]);

                board.automatePlacement(false, player);

                // At this point there should be no unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space !== null) placedPieces.push(piece);
                }
                assert.deepEqual(placedPieces, unplacedPieces);

                // Also check that pieces have been placed correctly
                assert.equal(board.spaces[startRow][startCol].piece, unplacedPieces[0]);
                assert.equal(board.spaces[startRow][startCol+1].piece, unplacedPieces[1]);
                assert.equal(board.spaces[startRow][startCol+2].piece, unplacedPieces[4]);
                assert.equal(board.spaces[startRow][startCol+3].piece, unplacedPieces[2]);
                assert.equal(board.spaces[endRow][endCol].piece, unplacedPieces[9]);
            }
        });

        QUnit.test("automatePlacement (randomize when empty)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            for (let playerIndex in players) {
                let unplacedPieces = [];
                let placedPieces = [];

                let player = players[playerIndex];
                let placementRange = board.placementSpaceRange[playerIndex];
                let startRow = placementRange[0][0];
                let startCol = placementRange[0][1];
                let endRow = placementRange[1][0];
                let endCol = placementRange[1][1];

                // At this point there should be some unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space === null) unplacedPieces.push(piece);
                }
                assert.notEqual(unplacedPieces.length, 0);

                board.automatePlacement(true, player);

                // At this point there should be no unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space !== null) placedPieces.push(piece);
                }
                assert.deepEqual(placedPieces, unplacedPieces);

                // Also check that pieces have been placed correctly
                assert.notEqual(placedPieces.indexOf(board.spaces[startRow][startCol].piece), -1);
                assert.notEqual(placedPieces.indexOf(board.spaces[startRow][startCol+1].piece), -1);
                assert.notEqual(placedPieces.indexOf(board.spaces[endRow][endCol].piece), -1);
            }
        });

        QUnit.test("automatePlacement (randomize partial)", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            for (let playerIndex in players) {
                let unplacedPieces = [];
                let placedPieces = [];

                let player = players[playerIndex];
                let placementRange = board.placementSpaceRange[playerIndex];
                let startRow = placementRange[0][0];
                let startCol = placementRange[0][1];
                let endRow = placementRange[1][0];
                let endCol = placementRange[1][1];

                // At this point there should be some unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space === null) unplacedPieces.push(piece);
                }
                assert.notEqual(unplacedPieces.length, 0);

                // Add a piece to the board
                let newPiece = player.pieces[0];
                if (playerIndex === "0") {
                    board.spaces[9][0].addPiece(newPiece);
                } else {
                    board.spaces[0][0].addPiece(newPiece);
                }

                board.automatePlacement(true, player);

                // Ensure the existing piece has remained
                if (playerIndex === "0") {
                    assert.equal(board.spaces[9][0].piece, newPiece);
                } else {
                    assert.equal(board.spaces[0][0].piece, newPiece);
                }

                // At this point there should be no unplaced pieces
                for (let piece of player.pieces) {
                    if (piece.space !== null) placedPieces.push(piece);
                }
                assert.deepEqual(placedPieces, unplacedPieces);

                // Also check that pieces have been placed correctly
                assert.notEqual(placedPieces.indexOf(board.spaces[startRow][startCol].piece), -1);
                assert.notEqual(placedPieces.indexOf(board.spaces[startRow][startCol+1].piece), -1);
                assert.notEqual(placedPieces.indexOf(board.spaces[endRow][endCol].piece), -1);
            }
        });

        QUnit.test("startGame", function(assert) {
            let player = new Player(1);
            let board = new Board([player]);

            board.startGame(() => assert.ok(false));
            assert.equal(board.gameState, GameState.Placement);

            // Add a dummy space to each of the players pieces
            for (let piece of player.pieces) {
                piece.space = new Space([0,0]);
            }

            board.startGame(() => assert.ok(true));
            assert.equal(board.gameState, GameState.Play);
        });

        QUnit.test("endGame", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            board.gameState = GameState.Play;
            board.endGame(null);
            assert.equal(board.gameState, GameState.End);
            assert.equal(board.winner, null);

            board.gameState = GameState.Play;
            board.endGame(players[0]);
            assert.equal(board.gameState, GameState.End);
            assert.equal(board.winner, players[0]);

            board.gameState = GameState.Play;
            board.endGame(players[1]);
            assert.equal(board.gameState, GameState.End);
            assert.equal(board.winner, players[1]);
        });

        QUnit.test("restartGame", function(assert) {
            let player = new Player(1);
            let board = new Board([player]);

            board.spaces[0][0].addPiece(new Marshall(player));
            board.incrementNumMoves();
            board.gameState = GameState.End;
            board.winner = player;

            board.restartGame();

            assert.equal(board.spaces[0][0].piece, null)
            assert.equal(board.numMoves, 0);
            assert.equal(board.gameState, GameState.Placement);
            assert.equal(board.winner, null);
        });

        QUnit.test("reset", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let spaces = board.spaces;

            // Add some pieces to the board
            spaces[0][0].addPiece(new Marshall(players[0]));
            spaces[1][1].addPiece(new Marshall(players[0]));
            spaces[2][2].addPiece(new Marshall(players[1]));

            board.reset(players[0]);
            assert.equal(spaces[0][0].piece, null);
            assert.equal(spaces[1][1].piece, null);
            assert.notEqual(spaces[2][2].piece, null);
            assert.equal(spaces[3][3].piece, null);

            board.reset(players[1]);
            assert.equal(spaces[2][2].piece, null);
        });

        QUnit.test("sortPieces", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            let unknownPiece = new Unknown(null, "btoken");
            unknownPiece.revealAs(new Spy(null));

            let unknownPiece2 = new Unknown(null, "ctoken");
            unknownPiece2.revealAs(new Marshall(null));

            let pieces = [
                new Colonel(null),
                new Bomb(null),
                new Unknown(null, "atoken"),
                new Marshall(null),
                new Major(null, "atoken"),
                new Lieutenant(null),
                new Spy(null),
                unknownPiece2,
                new Sergeant(null),
                new Flag(null),
                new Scout(null),
                unknownPiece,
                new General(null),
                new Captain(null),
                new Miner(null),
                new Major(null, "btoken")
            ];

            assert.deepEqual(board.sortPieces(pieces), [
                pieces[3],  //Marshall
                pieces[7],  //Revealed Marshall
                pieces[12], //General
                pieces[0],  //Colonel
                pieces[4],  //Major
                pieces[15], //Major
                pieces[13], //Captain
                pieces[5],  //Lieutenant
                pieces[8],  //Sergeant
                pieces[14], //Miner
                pieces[10], //Scout
                pieces[6],  //Spy
                pieces[11], //Revealed Spy
                pieces[1],  //Bomb
                pieces[9],  //Flag
                pieces[2]   //Unknown
            ]);
        });

        QUnit.test("isDisabledSpace", function(assert) {
            let board = new Board([new Player(1)]);
            let disabledSpaces = board.disabledSpaces;

            assert.notOk(board.isDisabledSpace([-1,-1]));
            assert.ok(board.isDisabledSpace(disabledSpaces[0]));
            assert.ok(board.isDisabledSpace(disabledSpaces[disabledSpaces.length - 1]));
        });
    }

    private static coordsInSpace(coords: [number, number], spaces: Space[]): boolean {
        for (let space of spaces) {
            let spaceCoords = space.coords;
            if (coords[0] === spaceCoords[0] && coords[1] === spaceCoords[1]) return true;
        }

        return false;
    }
}
