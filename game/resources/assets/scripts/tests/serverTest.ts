import Player from "../player";
import Board from "../board";
import { GameState } from "../board";
import Server from "../server";
import Unknown from "../pieces/12_unknown";

export default class ServerTest {
    public static run(): void {
        QUnit.module("server.ts");

        QUnit.test("get/set inCombat", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            assert.notOk(server.inCombat);
            server.inCombat = true;
            assert.ok(server.inCombat);
        });

        QUnit.test("piecePlacement - valid event", function(assert) {
            assert.expect(4);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.piecePlacement({
                "player": 1,
                "pieces": [{
                    "token": "test0",
                    "row": 1,
                    "column": 2
                }]
            });

            assert.equal(board.spaces[1][2].piece, players[0].pieces[0]);
            assert.equal(board.spaces[8][2].piece, null);
            assert.deepEqual(players[0].pieces[0].space.coords, [1,2]);
        });

        QUnit.test("piecePlacement - unknown token", function(assert) {
            assert.expect(4);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.piecePlacement({
                "player": 1,
                "pieces": [{
                    "token": "test99",
                    "row": 1,
                    "column": 2
                }]
            });

            assert.equal(board.spaces[1][2].piece, null);
            assert.equal(board.spaces[8][2].piece, null);
            assert.equal(players[0].pieces[0].space, null);
        });

        QUnit.test("piecePlacement - invalid event", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.piecePlacement({
                "pieces": [{
                    "token": "test0",
                    "row": 1,
                    "column": 2
                }]
            });

            assert.equal(board.spaces[1][2].piece, null);
            assert.equal(board.spaces[8][2].piece, null);
            assert.equal(players[0].pieces[0].space, null);
        });

        QUnit.test("move - valid event", function(assert) {
            assert.expect(4);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            let piece = players[0].pieces[0];
            board.spaces[0][0].addPiece(piece);

            server.move({
                "player": 1,
                "piece": {
                    "token": "test0",
                    "row": 1,
                    "column": 2
                }
            });

            assert.equal(board.spaces[1][2].piece, piece);
            assert.equal(board.spaces[0][0].piece, null);
            assert.deepEqual(players[0].pieces[0].space.coords, [1,2]);
        });

        QUnit.test("move - unknown player", function(assert) {
            assert.expect(4);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            let piece = players[0].pieces[0];
            board.spaces[0][0].addPiece(piece);

            server.move({
                "player": 999,
                "piece": {
                    "token": "test0",
                    "row": 1,
                    "column": 2
                }
            });

            assert.equal(board.spaces[1][2].piece, null);
            assert.equal(board.spaces[0][0].piece, piece);
            assert.deepEqual(players[0].pieces[0].space.coords, [0,0]);
        });

        QUnit.test("move - unknown token", function(assert) {
            assert.expect(4);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            let piece = players[0].pieces[0];
            board.spaces[0][0].addPiece(piece);

            server.move({
                "player": 1,
                "piece": {
                    "token": "test99",
                    "row": 1,
                    "column": 2
                }
            });

            assert.equal(board.spaces[1][2].piece, null);
            assert.equal(board.spaces[0][0].piece, piece);
            assert.deepEqual(players[0].pieces[0].space.coords, [0,0]);
        });

        QUnit.test("move - invalid event", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            let piece = players[0].pieces[0];
            board.spaces[0][0].addPiece(piece);

            server.move({
                "piece": {
                    "token": "test99",
                    "row": 1,
                    "column": 2
                }
            });

            assert.equal(board.spaces[1][2].piece, null);
            assert.equal(board.spaces[0][0].piece, piece);
            assert.deepEqual(players[0].pieces[0].space.coords, [0,0]);
        });

        QUnit.test("movePieces - multiple player 1", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.movePieces(players[0], [
                {
                    "token": "test0",
                    "row": 1,
                    "column": 2
                },
                {
                    "token": "test39",
                    "row": 2,
                    "column": 3
                }
            ]);

            assert.equal(board.spaces[1][2].piece, players[0].pieces[0]);
            assert.deepEqual(players[0].pieces[0].space.coords, [1,2]);
            assert.equal(board.spaces[2][3].piece, players[0].pieces[39]);
            assert.deepEqual(players[0].pieces[39].space.coords, [2,3]);
        });

        QUnit.test("movePieces - multiple player 2", function(assert) {
            let players = [new Player(2), new Player(1)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.movePieces(players[0], [
                {
                    "token": "test0",
                    "row": 1,
                    "column": 2
                },
                {
                    "token": "test39",
                    "row": 2,
                    "column": 3
                }
            ]);

            assert.equal(board.spaces[8][7].piece, players[0].pieces[0]);
            assert.deepEqual(players[0].pieces[0].space.coords, [8,7]);
            assert.equal(board.spaces[7][6].piece, players[0].pieces[39]);
            assert.deepEqual(players[0].pieces[39].space.coords, [7,6]);
        });

        QUnit.test("movePieces - multiple existing piece", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            board.spaces[4][0].addPiece(players[0].pieces[0]);
            board.spaces[4][1].addPiece(players[0].pieces[39]);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.movePieces(players[0], [
                {
                    "token": "test0",
                    "row": 1,
                    "column": 2
                },
                {
                    "token": "test39",
                    "row": 2,
                    "column": 3
                }
            ]);

            assert.equal(board.spaces[1][2].piece, players[0].pieces[0]);
            assert.deepEqual(players[0].pieces[0].space.coords, [1,2]);
            assert.equal(board.spaces[2][3].piece, players[0].pieces[39]);
            assert.deepEqual(players[0].pieces[39].space.coords, [2,3]);

            assert.equal(board.spaces[4][0].piece, null);
            assert.equal(board.spaces[4][1].piece, null);
        });

        QUnit.test("movePieces - single", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
            }

            server.movePieces(players[0], {
                "token": "test0",
                "row": 1,
                "column": 2
            });

            assert.equal(board.spaces[1][2].piece, players[0].pieces[0]);
            assert.deepEqual(players[0].pieces[0].space.coords, [1,2]);
        });

        QUnit.test("turn - valid player", function(assert) {
            assert.expect(6);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "test", "", 1, null, []);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            server.turn({
                "turn": players[0].num
            });
            assert.equal(board.turn, players[0]);
            assert.notOk(board.disabled);

            server.turn({
                "turn": players[1].num
            });
            assert.equal(board.turn, players[1]);
            assert.ok(board.disabled);
        });

        QUnit.test("turn - in combat", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "test", "", 1, null, []);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            server.inCombat = true;

            server.turn({
                "turn": players[0].num
            });
            assert.equal(board.turn, players[0]);
            assert.notOk(board.disabled);

            server.turn({
                "turn": players[1].num
            });
            assert.equal(board.turn, players[1]);
            assert.ok(board.disabled);
        });

        QUnit.test("turn - unknown player", function(assert) {
            assert.expect(2);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "test", "", 1, null, []);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            server.turn({
                "turn": 9
            });
            assert.equal(board.turn, null);
        });

        QUnit.test("turn - no player", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "test", "", 1, null, []);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            server.turn({});
            assert.equal(board.turn, null);
        });

        QUnit.test("gameState - valid gameState", function(assert) {
            assert.expect(9);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            server.gameState({
                "gameState": "Play"
            });
            assert.equal(board.gameState, GameState.Play);
            assert.equal(board.winner, null);

            server.gameState({
                "gameState": "End",
                "winner": null
            });
            assert.equal(board.gameState, GameState.End);
            assert.equal(board.winner, null);

            server.gameState({
                "gameState": "End",
                "winner": 2
            });
            assert.equal(board.gameState, GameState.End);
            assert.equal(board.winner, players[1]);
        });

        QUnit.test("gameState - unknown gameState", function(assert) {
            assert.expect(2);

            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            server.gameState({
                "gameState": "Test"
            });
            assert.equal(board.gameState, GameState.Placement);
        });

        QUnit.test("gameState - no gameState", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            server.gameState({});
            assert.equal(board.gameState, GameState.Placement);
        });

        QUnit.test("combat - valid event", function(assert) {
            assert.expect(13);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Unknown","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Unknown","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            let done = assert.async();
            server.combat(
                {
                    "attackingPiece": {
                        "player": 1,
                        "type": "Marshall",
                        "winner": true,
                        "token": "attack",
                        "row": 0,
                        "column": 0
                    },
                    "defendingPiece": {
                        "player": 2,
                        "type": "Miner",
                        "winner": false,
                        "token": "defend",
                        "row": 0,
                        "column": 1
                    }
                },
                10,
                () => {
                    assert.equal(board.spaces[0][0].piece, null);
                    assert.equal(board.spaces[0][1].piece, attackingPiece);
                    assert.notOk(server.inCombat);

                    assert.notOk(attackingPiece.revealed);
                    assert.notOk(defendingPiece.revealed);

                    done();
                }
            );

            assert.equal(board.spaces[0][0].piece, attackingPiece);
            assert.equal(board.spaces[0][1].piece, defendingPiece);
            assert.ok(server.inCombat);

            assert.ok(attackingPiece.revealed);
            assert.equal(attackingPiece.rankName, "Marshall");

            assert.ok(defendingPiece.revealed);
            assert.equal(defendingPiece.rankName, "Miner");
        });

        QUnit.test("combat - valid event (scout)", function(assert) {
            assert.expect(15);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Unknown","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Unknown","row":0,"col":9}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            let done = assert.async();
            server.combat(
                {
                    "attackingPiece": {
                        "player": 1,
                        "type": "Scout",
                        "winner": false,
                        "token": "attack",
                        "row": 0,
                        "column": 0
                    },
                    "defendingPiece": {
                        "player": 2,
                        "type": "Bomb",
                        "winner": true,
                        "token": "defend",
                        "row": 0,
                        "column": 9
                    }
                },
                10,
                () => {
                    assert.equal(board.spaces[0][0].piece, null);
                    assert.equal(board.spaces[0][8].piece, null);
                    assert.equal(board.spaces[0][9].piece, defendingPiece);
                    assert.notOk(server.inCombat);

                    assert.notOk(attackingPiece.revealed);
                    assert.notOk(defendingPiece.revealed);

                    done();
                }
            );

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][8].piece, attackingPiece);
            assert.equal(board.spaces[0][9].piece, defendingPiece);
            assert.ok(server.inCombat);

            assert.ok(attackingPiece.revealed);
            assert.equal(attackingPiece.rankName, "Scout");

            assert.ok(defendingPiece.revealed);
            assert.equal(defendingPiece.rankName, "Bomb");
        });

        QUnit.test("combat - unknown token", function(assert) {
            assert.expect(8);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Unknown","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Unknown","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            server.combat(
                {
                    "attackingPiece": {
                        "player": 1,
                        "type": "Marshall",
                        "winner": true,
                        "token": "attack",
                        "row": 0,
                        "column": 0
                    },
                    "defendingPiece": {
                        "player": 2,
                        "type": "Miner",
                        "winner": false,
                        "token": "testing",
                        "row": 0,
                        "column": 1
                    }
                },
                10,
                () => {
                    assert.ok(false);
                }
            );

            assert.equal(board.spaces[0][0].piece, attackingPiece);
            assert.equal(board.spaces[0][1].piece, defendingPiece);
            assert.notOk(server.inCombat);

            assert.notOk(attackingPiece.revealed);
            assert.notEqual(attackingPiece.rankName, "Marshall");

            assert.notOk(defendingPiece.revealed);
            assert.notEqual(defendingPiece.rankName, "Miner");
        });

        QUnit.test("combat - unknown player", function(assert) {
            assert.expect(8);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Unknown","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Unknown","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            server.combat(
                {
                    "attackingPiece": {
                        "player": 1,
                        "type": "Marshall",
                        "winner": true,
                        "token": "attack",
                        "row": 0,
                        "column": 0
                    },
                    "defendingPiece": {
                        "player": 999,
                        "type": "Miner",
                        "winner": false,
                        "token": "defend",
                        "row": 0,
                        "column": 1
                    }
                },
                10,
                () => {
                    assert.ok(false);
                }
            );

            assert.equal(board.spaces[0][0].piece, attackingPiece);
            assert.equal(board.spaces[0][1].piece, defendingPiece);
            assert.notOk(server.inCombat);

            assert.notOk(attackingPiece.revealed);
            assert.notEqual(attackingPiece.rankName, "Marshall");

            assert.notOk(defendingPiece.revealed);
            assert.notEqual(defendingPiece.rankName, "Miner");
        });

        QUnit.test("combat - invalid event", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Unknown","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Unknown","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            server.combat(
                {
                    "attackingPiece": {
                        "player": 1,
                        "type": "Marshall",
                        "winner": true,
                        "token": "attack",
                        "row": 0,
                        "column": 0
                    }
                },
                10,
                () => {
                    assert.ok(false);
                }
            );

            assert.equal(board.spaces[0][0].piece, attackingPiece);
            assert.equal(board.spaces[0][1].piece, defendingPiece);
            assert.notOk(server.inCombat);

            assert.notOk(attackingPiece.revealed);
            assert.notEqual(attackingPiece.rankName, "Marshall");

            assert.notOk(defendingPiece.revealed);
            assert.notEqual(defendingPiece.rankName, "Miner");
        });

        QUnit.test("handleCombat - attack win", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Marshall","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Miner","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            attackingPiece.revealed = true;
            defendingPiece.revealed = true;
            server.inCombat = true;

            let event = {
                "attackingPiece": {
                    "player": 1,
                    "type": "Marshall",
                    "winner": true,
                    "token": "attack",
                    "row": 0,
                    "column": 0
                },
                "defendingPiece": {
                    "player": 2,
                    "type": "Miner",
                    "winner": false,
                    "token": "defend",
                    "row": 0,
                    "column": 1
                }
            };

            server.handleCombat(attackingPiece, defendingPiece, event);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, attackingPiece);
            assert.equal(defendingPiece.space, null);
            assert.notOk(attackingPiece.revealed);
            assert.notOk(defendingPiece.revealed);
            assert.notOk(server.inCombat);
        });

        QUnit.test("handleCombat - defend win", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Miner","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Marshall","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            attackingPiece.revealed = true;
            defendingPiece.revealed = true;
            server.inCombat = true;

            let event = {
                "attackingPiece": {
                    "player": 1,
                    "type": "Miner",
                    "winner": false,
                    "token": "attack",
                    "row": 0,
                    "column": 0
                },
                "defendingPiece": {
                    "player": 2,
                    "type": "Marshall",
                    "winner": true,
                    "token": "defend",
                    "row": 0,
                    "column": 1
                }
            };

            server.handleCombat(attackingPiece, defendingPiece, event);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, defendingPiece);
            assert.equal(attackingPiece.space, null);
            assert.notOk(attackingPiece.revealed);
            assert.notOk(defendingPiece.revealed);
            assert.notOk(server.inCombat);
        });

        QUnit.test("handleCombat - draw", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            players[0].updatePieces(
                [{"token":"attack","type":"Marshall","row":0,"col":0}],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"defend","type":"Marshall","row":0,"col":1}],
                board.spaces
            );

            let attackingPiece = players[0].pieces[0];
            let defendingPiece = players[1].pieces[0];

            attackingPiece.revealed = true;
            defendingPiece.revealed = true;
            server.inCombat = true;

            let event = {
                "attackingPiece": {
                    "player": 1,
                    "type": "Marshall",
                    "winner": false,
                    "token": "attack",
                    "row": 0,
                    "column": 0
                },
                "defendingPiece": {
                    "player": 2,
                    "type": "Marshall",
                    "winner": false,
                    "token": "defend",
                    "row": 0,
                    "column": 1
                }
            };

            server.handleCombat(attackingPiece, defendingPiece, event);

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, null);
            assert.equal(attackingPiece.space, null);
            assert.equal(defendingPiece.space, null);
            assert.notOk(attackingPiece.revealed);
            assert.notOk(defendingPiece.revealed);
            assert.notOk(server.inCombat);
        });

        QUnit.test("placePieces - success", function(assert) {
            $.mockjax.clear();
            assert.expect(3);
            let done = assert.async();

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "testing");
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            let placementSpaces = board.getPossiblePlacementSpaces(players[0]);
            let pieces = [];
            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
                placementSpaces[pieceIndex].addPiece(piece);

                pieces.push({
                    "token": piece.token,
                    "row": piece.space.coords[0],
                    "col": piece.space.coords[1]
                })
            }

            let expectedData = {
                "gameToken": board.gameToken,
                "pieces": pieces
            };

            $.mockjax({
                type: "POST",
                url: "/game/placePieces",
                data: (json: any) => {
                    assert.deepEqual(JSON.parse(json), expectedData);
                    return true;
                },
                responseTime: 50,
                responseText: {
                    "success": "Success!"
                },
                onAfterSuccess: () => {
                    assert.equal(board.gameState, GameState.Ready);
                    done();
                }
            });

            server.placePieces();
        });

        QUnit.test("placePieces - error", function(assert) {
            $.mockjax.clear();
            let done = assert.async();

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "testing");
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            let placementSpaces = board.getPossiblePlacementSpaces(players[0]);
            for (let pieceIndex in players[0].pieces) {
                let piece = players[0].pieces[pieceIndex];
                piece.token = "test" + pieceIndex;
                placementSpaces[pieceIndex].addPiece(piece);
            }

            $.mockjax({
                type: "POST",
                url: "/game/placePieces",
                responseTime: 50,
                status: 500,
                responseText: {
                    "error": "Unable to setup piece placement"
                },
                onAfterError: () => {
                    assert.equal(board.gameState, GameState.Placement);
                    done();
                }
            });

            server.placePieces();
        });

        QUnit.test("movePiece - success", function(assert) {
            $.mockjax.clear();
            assert.expect(5);
            let done = assert.async();

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "testing");
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            let piece = players[0].pieces[0];
            let originSpace = board.spaces[0][0];
            let targetSpace = board.spaces[0][1];
            originSpace.addPiece(piece);
            piece.token = "test";

            let expectedData = {
                "gameToken": board.gameToken,
                "piece": {
                    "token": piece.token,
                    "row": targetSpace.coords[0],
                    "col": targetSpace.coords[1]
                }
            };

            $.mockjax({
                type: "POST",
                url: "/game/movePiece",
                data: (json: any) => {
                    assert.deepEqual(JSON.parse(json), expectedData);
                    return true;
                },
                responseTime: 50,
                responseText: {
                    "success": "Success!"
                },
                onAfterSuccess: () => {
                    assert.equal(originSpace.piece, null);
                    assert.equal(targetSpace.piece, piece);
                    assert.equal(board.numMoves, 1);
                    assert.ok(board.disabled);
                    done();
                }
            });

            // Actually move the piece
            originSpace.removePiece();
            targetSpace.addPiece(piece);
            board.incrementNumMoves();
            board.disabled = true;

            server.movePiece(originSpace, targetSpace, piece, null);
        });

        QUnit.test("movePiece - error with empty target", function(assert) {
            $.mockjax.clear();
            assert.expect(5);
            let done = assert.async();

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "testing");
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            let piece = players[0].pieces[0];
            let originSpace = board.spaces[0][0];
            let targetSpace = board.spaces[0][1];
            originSpace.addPiece(piece);
            piece.token = "test";

            $.mockjax({
                type: "POST",
                url: "/game/movePiece",
                responseTime: 50,
                status: 500,
                responseText: {
                    "error": "Unable to move piece"
                },
                onAfterError: () => {
                    assert.equal(originSpace.piece, piece);
                    assert.equal(targetSpace.piece, null);
                    assert.equal(board.numMoves, 0);
                    assert.notOk(board.disabled);
                    done();
                }
            });

            // Actually move the piece
            originSpace.removePiece();
            targetSpace.addPiece(piece);
            board.incrementNumMoves();
            board.disabled = true;

            // The move should be reverted when movePiece's AJAX call errors
            server.movePiece(originSpace, targetSpace, piece, null);
        });

        QUnit.test("movePiece - error with target", function(assert) {
            $.mockjax.clear();
            assert.expect(5);
            let done = assert.async();

            let players = [new Player(1), new Player(2)];
            let board = new Board(players, "", "testing");
            let server = new Server(board, () => {
                assert.ok(true);
            }, false);

            let piece = players[0].pieces[0];
            let targetPiece = players[1].pieces[0];
            let originSpace = board.spaces[0][0];
            let targetSpace = board.spaces[0][1];
            originSpace.addPiece(piece);
            targetSpace.addPiece(targetPiece);
            piece.token = "test";

            $.mockjax({
                type: "POST",
                url: "/game/movePiece",
                responseTime: 50,
                status: 500,
                responseText: {
                    "error": "Unable to move piece"
                },
                onAfterError: () => {
                    assert.equal(originSpace.piece, piece);
                    assert.equal(targetSpace.piece, targetPiece);
                    assert.equal(board.numMoves, 0);
                    assert.notOk(board.disabled)
                    done();
                }
            });

            // Actually move the piece
            originSpace.removePiece();
            targetSpace.removePiece();
            targetSpace.addPiece(piece);
            board.incrementNumMoves();
            board.disabled = true;

            // The move should be reverted when movePiece's AJAX call errors
            server.movePiece(originSpace, targetSpace, piece, targetPiece);
        });

        QUnit.test("findPlayer", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            assert.equal(server.findPlayer(0), null);
            assert.equal(server.findPlayer(1), players[0]);
            assert.equal(server.findPlayer(2), players[1]);
        });

        QUnit.test("getPiecePosition", function(assert) {
            let player1 = new Player(1);
            let player2 = new Player(2);
            let board = new Board([player1, player2]);
            let server = new Server(board, () => {
                assert.ok(false);
            }, false);

            let pieces: [number, number][] = [
                [0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],[0,9],
                [1,0],[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],
                [2,0],[2,1],[2,2],[2,3],[2,4],[2,5],[2,6],[2,7],[2,8],[2,9],
                [3,0],[3,1],[3,2],[3,3],[3,4],[3,5],[3,6],[3,7],[3,8],[3,9],
                [4,0],[4,1],[4,2],[4,3],[4,4],[4,5],[4,6],[4,7],[4,8],[4,9],
                [5,0],[5,1],[5,2],[5,3],[5,4],[5,5],[5,6],[5,7],[5,8],[5,9],
                [6,0],[6,1],[6,2],[6,3],[6,4],[6,5],[6,6],[6,7],[6,8],[6,9],
                [7,0],[7,1],[7,2],[7,3],[7,4],[7,5],[7,6],[7,7],[7,8],[7,9],
                [8,0],[8,1],[8,2],[8,3],[8,4],[8,5],[8,6],[8,7],[8,8],[8,9],
                [9,0],[9,1],[9,2],[9,3],[9,4],[9,5],[9,6],[9,7],[9,8],[9,9]
            ];
            let flipped: [number, number][] = [
                [9,9],[9,8],[9,7],[9,6],[9,5],[9,4],[9,3],[9,2],[9,1],[9,0],
                [8,9],[8,8],[8,7],[8,6],[8,5],[8,4],[8,3],[8,2],[8,1],[8,0],
                [7,9],[7,8],[7,7],[7,6],[7,5],[7,4],[7,3],[7,2],[7,1],[7,0],
                [6,9],[6,8],[6,7],[6,6],[6,5],[6,4],[6,3],[6,2],[6,1],[6,0],
                [5,9],[5,8],[5,7],[5,6],[5,5],[5,4],[5,3],[5,2],[5,1],[5,0],
                [4,9],[4,8],[4,7],[4,6],[4,5],[4,4],[4,3],[4,2],[4,1],[4,0],
                [3,9],[3,8],[3,7],[3,6],[3,5],[3,4],[3,3],[3,2],[3,1],[3,0],
                [2,9],[2,8],[2,7],[2,6],[2,5],[2,4],[2,3],[2,2],[2,1],[2,0],
                [1,9],[1,8],[1,7],[1,6],[1,5],[1,4],[1,3],[1,2],[1,1],[1,0],
                [0,9],[0,8],[0,7],[0,6],[0,5],[0,4],[0,3],[0,2],[0,1],[0,0]
            ];

            let result = [];
            for (let piece of pieces) {
                result.push(server.getPiecePosition(player1, piece));
            }
            assert.deepEqual(result, pieces);

            result = [];
            for (let piece of pieces) {
                result.push(server.getPiecePosition(player2, piece));
            }
            assert.deepEqual(result, flipped);
        });
    }
}
