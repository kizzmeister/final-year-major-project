import Gui from "../gui";
import Board from "../board";
import Player from "../player";
import Marshall from "../pieces/1_marshall";
import General from "../pieces/2_general";

export default class GuiTest {
    public static run(): void {
        QUnit.module("gui.ts");

        QUnit.test("showPiece", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let gui = new Gui(board);

            // Define some pieces
            let piece1 = new Marshall(players[0]);
            let piece2 = new General(players[1]);
            let piece3 = null;

            assert.equal(
                gui.showPiece(piece1),
                '<div class="rank">1</div><div class="rankName">Marshall</div>'
            );

            assert.equal(gui.showPiece(piece2), "?");
            assert.equal(gui.showPiece(piece3), "");
        });
    }
}
