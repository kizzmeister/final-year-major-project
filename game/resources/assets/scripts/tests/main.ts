import AiTest from "./aiTest";
import AiFactoryTest from "./aiFactoryTest";
import ImprovedAITest from "./ais/improvedTest";
import RandomAITest from "./ais/randomTest";
import BoardTest from "./boardTest";
import CoreTest from "./coreTest";
import GuiTest from "./guiTest";
import PieceTest from "./pieceTest";
import PieceFactoryTest from "./pieceFactoryTest";
import PlayerTest from "./playerTest";
import SpaceTest from "./spaceTest";
import ServerTest from "./serverTest";
import UtilsTest from "./utilsTest";

class Tests {
    public static run(): void {
        QUnit.config.maxDepth = 1; //Prevent recursive dumps

        AiTest.run();
        AiFactoryTest.run();
        ImprovedAITest.run();
        RandomAITest.run();
        BoardTest.run();
        CoreTest.run();
        GuiTest.run();
        PieceTest.run();
        PieceFactoryTest.run();
        PlayerTest.run();
        SpaceTest.run();
        ServerTest.run();
        UtilsTest.run();
    }
}

Tests.run();
