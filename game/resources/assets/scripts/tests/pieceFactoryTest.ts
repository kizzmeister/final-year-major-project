import PieceFactory from "../pieceFactory";
import Player from "../player";
import Marshall from "../pieces/1_marshall";
import General from "../pieces/2_general";
import Colonel from "../pieces/3_colonel";
import Major from "../pieces/4_major";
import Captain from "../pieces/5_captain";
import Lieutenant from "../pieces/6_lieutenant";
import Sergeant from "../pieces/7_sergeant";
import Miner from "../pieces/8_miner";
import Scout from "../pieces/9_scout";
import Spy from "../pieces/10_spy";
import Bomb from "../pieces/0_bomb";
import Flag from "../pieces/11_flag";
import Unknown from "../pieces/12_unknown";

export default class PieceFactoryTest {
    public static run(): void {
        QUnit.module("pieceFactory.ts");

        QUnit.test("getInstance", function(assert) {
            let players = [new Player(1), new Player(2)];
            let ranks = [
                Marshall,
                General,
                Colonel,
                Major,
                Captain,
                Lieutenant,
                Sergeant,
                Miner,
                Scout,
                Spy,
                Bomb,
                Flag,
                Unknown
            ];

            // Create various pieces with different players
            let pieces = [
                PieceFactory.getInstance("marshall", players[0]),
                PieceFactory.getInstance("general", players[0]),
                PieceFactory.getInstance("colonel", players[0]),
                PieceFactory.getInstance("major", players[0]),
                PieceFactory.getInstance("captain", players[0]),
                PieceFactory.getInstance("lieutenant", players[0]),
                PieceFactory.getInstance("sergeant", players[0]),
                PieceFactory.getInstance("miner", players[0]),
                PieceFactory.getInstance("scout", players[0]),
                PieceFactory.getInstance("spy", players[0]),
                PieceFactory.getInstance("bomb", players[0]),

                PieceFactory.getInstance("flag", players[1]),
                PieceFactory.getInstance("unknown", players[1], "test"),
            ];

            for (let pieceIndex in pieces) {
                let piece = pieces[pieceIndex];
                let rank = ranks[pieceIndex];
                let player = (parseInt(pieceIndex) >= 11) ? players[1] : players[0];

                assert.ok(piece instanceof rank);
                assert.equal(piece.player, player);

                if (parseInt(pieceIndex) === 12) assert.equal(piece.token, "test");
            }

            // Also check we get an exception for a non-existent piece
            assert.throws(function() {
                PieceFactory.getInstance("test", players[0]);
            }, "Invalid pieceName - test");
        });
    }
}
