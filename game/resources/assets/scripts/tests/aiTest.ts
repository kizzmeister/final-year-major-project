import Board from "../board";
import { GameState } from "../board";
import Player from "../player";
import Piece from "../piece";
import Space from "../space";
import RandomAI from "../ais/random";

export default class AiTest {
    public static run(): void {
        QUnit.module("ai.ts");

        QUnit.test("handleReveal", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            assert.deepEqual(ai.knownPieces, []);

            ai.handleReveal(players[1].pieces[0]);
            assert.deepEqual(ai.knownPieces, [players[1].pieces[0]]);

            ai.handleReveal(players[1].pieces[1]);
            ai.handleReveal(players[1].pieces[2]);
            assert.deepEqual(ai.knownPieces, [
                players[1].pieces[0],
                players[1].pieces[1],
                players[1].pieces[2]
            ]);
        });

        QUnit.test("getPossibleMovements", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            players[0].updatePieces(
                [
                    {"token":"","type":"Marshall","row":null,"col":null},
                    {"token":"","type":"Bomb","row":null,"col":null},
                    {"token":"","type":"Flag","row":null,"col":null},
                    {"token":"","type":"Marshall","row":null,"col":null}
                ],
                board.spaces
            );
            players[1].updatePieces(
                [{"token":"","type":"Scout","row":null,"col":null}],
                board.spaces
            );

            assert.deepEqual(ai.getPossibleMovements(players[0]), []);
            assert.deepEqual(ai.getPossibleMovements(players[1]), []);

            board.spaces[0][0].addPiece(players[0].pieces[0]);
            board.spaces[0][1].addPiece(players[0].pieces[1]);
            board.spaces[1][0].addPiece(players[0].pieces[2]);

            assert.deepEqual(ai.getPossibleMovements(players[0]), []);
            assert.deepEqual(ai.getPossibleMovements(players[1]), []);

            let marshall = players[0].pieces[3];
            board.spaces[1][4].addPiece(marshall);
            assert.deepEqual(
                ai.getPossibleMovements(players[0]),
                [
                    [marshall, board.spaces[0][4]],
                    [marshall, board.spaces[1][3]],
                    [marshall, board.spaces[1][5]],
                    [marshall, board.spaces[2][4]]
                ]
            );

            let scout = players[1].pieces[0];
            board.spaces[8][4].addPiece(scout);
            assert.deepEqual(
                ai.getPossibleMovements(players[1]),
                [
                    [scout, board.spaces[1][4]],
                    [scout, board.spaces[2][4]],
                    [scout, board.spaces[3][4]],
                    [scout, board.spaces[4][4]],
                    [scout, board.spaces[5][4]],
                    [scout, board.spaces[6][4]],
                    [scout, board.spaces[7][4]],
                    [scout, board.spaces[8][0]],
                    [scout, board.spaces[8][1]],
                    [scout, board.spaces[8][2]],
                    [scout, board.spaces[8][3]],
                    [scout, board.spaces[8][5]],
                    [scout, board.spaces[8][6]],
                    [scout, board.spaces[8][7]],
                    [scout, board.spaces[8][8]],
                    [scout, board.spaces[8][9]],
                    [scout, board.spaces[9][4]]
                ]
            );
        });

        QUnit.test("takeTurn - movement", function(assert) {
            assert.expect(5);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            board.gameState = GameState.Play;
            board.turn = players[1];
            board.disabled = true;

            let possibleMoves: [number, number][] = [[0,4], [1,3], [1,5], [2,4]];
            let marshall = players[1].pieces[0];
            board.spaces[1][4].addPiece(marshall);

            let callbackCount = 0;
            ai.takeTurn(players[1], () => {
                if (callbackCount === 0) {
                    assert.equal(board.turn, players[1]);
                    assert.ok(board.disabled);
                } else {
                    assert.ok(AiTest.coordsInArray(marshall.space.coords, possibleMoves));
                    assert.equal(board.turn, players[0]);
                    assert.notOk(board.disabled);
                }
                callbackCount++;
            }, 10);
        });

        QUnit.test("takeTurn - combat", function(assert) {
            assert.expect(7);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            board.gameState = GameState.Play;
            board.turn = players[1];
            board.disabled = true;

            let possibleMoves: [number, number][] = [[0,4], [1,3], [1,5], [2,4]];
            let marshall = players[1].pieces[0];
            board.spaces[1][4].addPiece(marshall);

            // Add some lesser opposing pieces into all possible spaces
            for (let moveIndex in possibleMoves) {
                let move = possibleMoves[moveIndex];
                board.spaces[move[0]][move[1]].addPiece(players[0].pieces[parseInt(moveIndex) + 1]);
            }

            let callbackCount = 0;
            let done = assert.async();
            ai.takeTurn(players[1], () => {
                if (callbackCount === 0) {
                    assert.equal(board.turn, players[1]);
                    assert.ok(board.disabled);
                } else if (callbackCount === 1) {
                    assert.equal(board.spaces[1][4].piece, marshall);
                } else {
                    assert.ok(AiTest.coordsInArray(marshall.space.coords, possibleMoves));
                    assert.equal(players[0].getPieces(true).length, possibleMoves.length - 1);
                    assert.equal(board.turn, players[0]);
                    assert.notOk(board.disabled);
                    done();
                }
                callbackCount++;
            }, 10);
        });

        QUnit.test("takeTurn - game ended", function(assert) {
            assert.expect(0);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            board.gameState = GameState.End;
            ai.takeTurn(
                players[1],
                () => {
                    assert.ok(false)
                },
                10
            );
        });

        QUnit.test("handleMovement - movement", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            let marshall = players[1].pieces[0];
            board.spaces[0][0].addPiece(marshall);

            board.turn = players[1];
            board.disabled = true;
            ai.handleMovement(
                [
                    marshall,
                    board.spaces[0][1]
                ],
                () => {
                    assert.ok(false)
                },
                10
            );

            assert.equal(board.turn, players[0]);
            assert.notOk(board.disabled);
            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][1].piece, marshall);
        });

        QUnit.test("handleMovement - combat (normal piece)", function(assert) {
            assert.expect(14);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            let marshall = players[1].pieces[0];
            let general = players[0].pieces[1];

            board.spaces[0][0].addPiece(marshall);
            board.spaces[0][1].addPiece(general);
            board.turn = players[1];
            board.disabled = true;

            let done = assert.async();
            ai.handleMovement(
                [
                    marshall,
                    board.spaces[0][1]
                ],
                () => {
                    assert.equal(board.spaces[0][0].piece, null);
                    assert.equal(board.spaces[0][1].piece, marshall);
                    assert.equal(general.space, null);
                    assert.notOk(marshall.revealed);
                    assert.notOk(general.revealed);
                    assert.equal(board.turn, players[0]);
                    assert.notOk(board.disabled);
                    done();
                },
                10
            );

            assert.equal(board.spaces[0][0].piece, marshall);
            assert.equal(board.spaces[0][1].piece, general);
            assert.ok(marshall.revealed);
            assert.ok(general.revealed);
            assert.deepEqual(ai.knownPieces, [general]);
            assert.equal(board.turn, players[1]);
            assert.ok(board.disabled);
        });

        QUnit.test("handleMovement - combat (scout)", function(assert) {
            assert.expect(16);
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);
            let ai = new RandomAI(board);

            let scout = players[1].pieces[31];
            let spy = players[0].pieces[32];

            board.spaces[0][0].addPiece(scout);
            board.spaces[0][9].addPiece(spy);
            board.turn = players[1];
            board.disabled = true;

            let done = assert.async();
            ai.handleMovement(
                [
                    scout,
                    board.spaces[0][9]
                ],
                () => {
                    assert.equal(board.spaces[0][0].piece, null);
                    assert.equal(board.spaces[0][8].piece, null);
                    assert.equal(board.spaces[0][9].piece, scout);
                    assert.equal(spy.space, null);
                    assert.notOk(scout.revealed);
                    assert.notOk(spy.revealed);
                    assert.equal(board.turn, players[0]);
                    assert.notOk(board.disabled);
                    done();
                },
                10
            );

            assert.equal(board.spaces[0][0].piece, null);
            assert.equal(board.spaces[0][8].piece, scout);
            assert.equal(board.spaces[0][9].piece, spy);
            assert.ok(scout.revealed);
            assert.ok(spy.revealed);
            assert.deepEqual(ai.knownPieces, [spy]);
            assert.equal(board.turn, players[1]);
            assert.ok(board.disabled);
        });
    }

    private static coordsInArray(coords: [number, number], array: [number, number][]): boolean {
        for (let c of array) {
            if (coords[0] === c[0] && coords[1] === c[1]) return true;
        }

        return false;
    }
}
