import AiFactory from "../aiFactory";
import Board from "../board";
import Player from "../player";
import RandomAI from "../ais/random";
import ImprovedAI from "../ais/improved";

export default class AiFactoryTest {
    public static run(): void {
        QUnit.module("aiFactory.ts");

        QUnit.test("getInstance", function(assert) {
            let players = [new Player(1), new Player(2)];
            let board = new Board(players);

            let randomAI = AiFactory.getInstance("random", board);
            assert.ok(randomAI instanceof RandomAI);

            let improvedAI = AiFactory.getInstance("improved", board);
            assert.ok(improvedAI instanceof ImprovedAI);

            // Also check we get an exception for a non-existent AI
            assert.throws(function() {
                AiFactory.getInstance("test", board);
            }, "Invalid aiName - test");
        });
    }
}
