import Piece from "../piece";
import Player from "../player";
import Space from "../space";

import Marshall from "../pieces/1_marshall";
import General from "../pieces/2_general";
import Colonel from "../pieces/3_colonel";
import Major from "../pieces/4_major";
import Captain from "../pieces/5_captain";
import Lieutenant from "../pieces/6_lieutenant";
import Sergeant from "../pieces/7_sergeant";
import Miner from "../pieces/8_miner";
import Scout from "../pieces/9_scout";
import Spy from "../pieces/10_spy";
import Bomb from "../pieces/0_bomb";
import Flag from "../pieces/11_flag";
import Unknown from "../pieces/12_unknown";

export default class PieceTest {
    public static run(): void {
        QUnit.module("piece.ts");

        QUnit.test("get rank", function(assert) {
            let piece1 = new Marshall(null);
            let piece2 = new Bomb(null);
            let piece3 = new Flag(null);

            assert.equal(piece1.rank, "1");
            assert.equal(piece2.rank, "B");
            assert.equal(piece3.rank, "F");
        });

        QUnit.test("get rankNum", function(assert) {
            let piece1 = new Marshall(null);
            let piece2 = new Flag(null);

            assert.equal(piece1.rankNum, "1");
            assert.equal(piece2.rankNum, "11");
        });

        QUnit.test("get rankName", function(assert) {
            let piece1 = new Marshall(null);
            let piece2 = new Flag(null);

            assert.equal(piece1.rankName, "Marshall");
            assert.equal(piece2.rankName, "Flag");
        });

        QUnit.test("get/set space", function(assert) {
            let piece = new Marshall(null);
            let space = new Space([0,0]);

            assert.equal(piece.space, null);
            piece.space = space;
            assert.equal(piece.space, space);
        });

        QUnit.test("get player", function(assert) {
            let player = new Player(1);
            let piece = new Marshall(player);

            assert.equal(piece.player, player);
        });

        QUnit.test("get/set token", function(assert) {
            let piece = new Marshall(null);
            assert.equal(piece.token, "");

            let piece2 = new Marshall(null, "test");
            assert.equal(piece2.token, "test");

            piece2.token = "testing";
            assert.equal(piece2.token, "testing");
        });

        QUnit.test("get/set revealed", function(assert) {
            let piece = new Marshall(null);

            assert.notOk(piece.revealed);
            piece.revealed = true;
            assert.ok(piece.revealed);
        });

        QUnit.test("canMoveTo - default", function(assert) {
            let spaces: Space[][] = [];
            let ranks = [
                Marshall,
                General,
                Colonel,
                Major,
                Captain,
                Lieutenant,
                Sergeant,
                Miner,
                Spy
            ];

            // Test each of the ranks which should exhibit the default movement functionality
            for (let rank of ranks) {
                let piece = new rank(null);
                let space = new Space([5,5]);
                space.addPiece(piece);

                // Can move by one space
                assert.ok(piece.canMoveTo(new Space([5,5]), spaces, 10));
                assert.ok(piece.canMoveTo(new Space([4,5]), spaces, 10));
                assert.ok(piece.canMoveTo(new Space([5,4]), spaces, 10));
                assert.ok(piece.canMoveTo(new Space([6,5]), spaces, 10));
                assert.ok(piece.canMoveTo(new Space([5,6]), spaces, 10));

                // Cannot move onto a disabled space
                assert.notOk(piece.canMoveTo(new Space([5,5], true), spaces, 10));

                // Cannot move to far away spaces or diagonally
                assert.notOk(piece.canMoveTo(new Space([0,0]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([4,4]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([4,6]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([6,4]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([6,6]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([9,9]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([0,5]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([-1,-1]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([10,10]), spaces, 10));

                // Cannot move beyond board boundaries
                space.removePiece();
                space = new Space([0,0]);
                space.addPiece(piece);

                assert.notOk(piece.canMoveTo(new Space([-1,0]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([0,-1]), spaces, 10));

                space.removePiece();
                space = new Space([9,9]);
                space.addPiece(piece);

                assert.notOk(piece.canMoveTo(new Space([10,9]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([9,10]), spaces, 10));
            }
        });

        QUnit.test("canMoveTo - scout", function(assert) {
            let spaces: Space[][] = [];
            let piece = new Scout(null);
            let space = new Space([5,5]);
            space.addPiece(piece);

            // Can move by one space
            assert.ok(piece.canMoveTo(new Space([5,5]), spaces, 10));
            assert.ok(piece.canMoveTo(new Space([4,5]), spaces, 10));
            assert.ok(piece.canMoveTo(new Space([5,4]), spaces, 10));
            assert.ok(piece.canMoveTo(new Space([6,5]), spaces, 10));
            assert.ok(piece.canMoveTo(new Space([5,6]), spaces, 10));

            // Cannot move to far away spaces or diagonally
            assert.notOk(piece.canMoveTo(new Space([0,0]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([4,4]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([4,6]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([6,4]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([6,6]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([9,9]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([-1,-1]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([10,10]), spaces, 10));

            // Cannot move beyond board boundaries
            assert.notOk(piece.canMoveTo(new Space([-1,5]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([10,5]), spaces, 10));

            space.removePiece();
            space = new Space([0,0]);
            space.addPiece(piece);

            assert.notOk(piece.canMoveTo(new Space([-1,0]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([0,-1]), spaces, 10));

            space.removePiece();
            space = new Space([9,9]);
            space.addPiece(piece);

            assert.notOk(piece.canMoveTo(new Space([10,9]), spaces, 10));
            assert.notOk(piece.canMoveTo(new Space([9,10]), spaces, 10));

            // Can move by multiple spaces in the same direction
            for (let row = 0; row < 10; row++) {
                for (let col = 0; col < 10; col++) {
                    if (!spaces[row]) spaces[row] = [];
                    spaces[row][col] = new Space([row, col]);
                }
            }
            spaces[5][5].addPiece(piece);

            assert.ok(piece.canMoveTo(spaces[0][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[9][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[5][0], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[5][9], spaces, 10));

            // Cannot move multiple spaces if a piece blocks the path
            spaces[2][5].addPiece(new Bomb(null));
            spaces[5][8].addPiece(new Bomb(null));

            assert.notOk(piece.canMoveTo(spaces[0][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[2][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[3][5], spaces, 10));

            assert.notOk(piece.canMoveTo(spaces[5][9], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[5][8], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[5][7], spaces, 10));

            assert.ok(piece.canMoveTo(spaces[9][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[5][0], spaces, 10));

            // Cannot move multiple spaces if a disabled piece blocks the path
            spaces[1][5] = new Space([1,5], true);
            spaces[5][7] = new Space([5,7], true);
            spaces[8][5] = new Space([8,5], true);

            assert.notOk(piece.canMoveTo(spaces[0][5], spaces, 10));
            assert.notOk(piece.canMoveTo(spaces[1][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[2][5], spaces, 10));

            assert.notOk(piece.canMoveTo(spaces[5][9], spaces, 10));
            assert.notOk(piece.canMoveTo(spaces[5][8], spaces, 10));
            assert.notOk(piece.canMoveTo(spaces[5][7], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[5][6], spaces, 10));

            assert.notOk(piece.canMoveTo(spaces[9][5], spaces, 10));
            assert.notOk(piece.canMoveTo(spaces[8][5], spaces, 10));
            assert.ok(piece.canMoveTo(spaces[7][5], spaces, 10));
        });

        QUnit.test("canMoveTo - unmovable", function(assert) {
            let spaces: Space[][] = [];
            let ranks = [
                Bomb,
                Flag
            ];

            // Test each of the ranks which should be unmovable
            for (let rank of ranks) {
                let piece = new rank(null);
                let space = new Space([5,5]);
                space.addPiece(piece);

                // Cannot move to any space
                assert.notOk(piece.canMoveTo(new Space([5,5]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([4,5]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([5,4]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([6,5]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([5,6]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([0,0]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([9,9]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([0,5]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([-1,-1]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([10,10]), spaces, 10));

                // Cannot move beyond board boundaries
                space.removePiece();
                space = new Space([0,0]);
                space.addPiece(piece);

                assert.notOk(piece.canMoveTo(new Space([-1,0]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([0,-1]), spaces, 10));

                space.removePiece();
                space = new Space([9,9]);
                space.addPiece(piece);

                assert.notOk(piece.canMoveTo(new Space([10,9]), spaces, 10));
                assert.notOk(piece.canMoveTo(new Space([9,10]), spaces, 10));
            }
        });

        QUnit.test("canDefeat - default", function(assert) {
            let pieces = [
                new Marshall(null),
                new General(null),
                new Colonel(null),
                new Major(null),
                new Captain(null),
                new Lieutenant(null),
                new Sergeant(null),
                new Miner(null),
                new Scout(null),
                new Spy(null)
            ];
            let bomb = new Bomb(null);
            let flag = new Flag(null);

            for (let pieceIndex in pieces) {
                // Skip Miners and Spies as we test those separately
                if (pieceIndex === "7" || pieceIndex === "9") continue;
                let piece = pieces[pieceIndex];

                // For each piece, loop through the list of pieces, checking higher ranks and defeat lower ones
                for (let pIndex in pieces) {
                    if (pieceIndex !== pIndex) {
                        let p = pieces[pIndex];
                        assert.equal(piece.canDefeat(p), pieceIndex <= pIndex);
                    }
                }

                assert.ok(piece.canDefeat(piece));
                assert.notOk(piece.canDefeat(bomb));
                assert.ok(bomb.canDefeat(piece));
                assert.ok(piece.canDefeat(flag));
                assert.notOk(flag.canDefeat(piece));
            }
        });

        QUnit.test("canDefeat - miner", function(assert) {
            let pieces = [
                new Marshall(null),
                new General(null),
                new Colonel(null),
                new Major(null),
                new Captain(null),
                new Lieutenant(null),
                new Sergeant(null)
            ];
            let miner = new Miner(null);
            let scout = new Scout(null);
            let spy = new Spy(null);
            let bomb = new Bomb(null);
            let flag = new Flag(null);

            for (let piece of pieces) {
                assert.notOk(miner.canDefeat(piece));
            }

            assert.ok(miner.canDefeat(miner));
            assert.ok(miner.canDefeat(scout));
            assert.ok(miner.canDefeat(spy));

            assert.ok(miner.canDefeat(bomb));
            assert.ok(bomb.canDefeat(miner));

            assert.ok(miner.canDefeat(flag));
            assert.notOk(flag.canDefeat(miner));
        });

        QUnit.test("canDefeat - spy", function(assert) {
            let pieces = [
                new General(null),
                new Colonel(null),
                new Major(null),
                new Captain(null),
                new Lieutenant(null),
                new Sergeant(null),
                new Miner(null),
                new Scout(null)
            ];
            let spy = new Spy(null);
            let marshall = new Marshall(null);
            let bomb = new Bomb(null);
            let flag = new Flag(null);

            for (let piece of pieces) {
                assert.notOk(spy.canDefeat(piece));
            }

            assert.ok(spy.canDefeat(spy));
            assert.ok(spy.canDefeat(marshall));

            assert.notOk(spy.canDefeat(bomb));
            assert.ok(bomb.canDefeat(spy));

            assert.ok(spy.canDefeat(flag));
            assert.notOk(flag.canDefeat(spy));
        });

        QUnit.test("revealAs - unknown only", function(assert) {
            let piece = new Unknown(null);

            assert.equal(piece.rank, "?");
            assert.equal(piece.rankNum, 12);
            assert.equal(piece.rankName, "Unknown");

            let marshall = new Marshall(null);
            piece.revealAs(marshall);

            assert.equal(piece.rank, "1");
            assert.equal(piece.rankNum, 1);
            assert.equal(piece.rankName, "Marshall");

            let bomb = new Bomb(null);
            piece.revealed = false;
            piece.revealAs(bomb);

            assert.equal(piece.rank, "B");
            assert.equal(piece.rankNum, 0);
            assert.equal(piece.rankName, "Bomb");
        });
    }
}
