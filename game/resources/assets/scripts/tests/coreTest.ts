import Core from "../core";
import Gui from "../gui";
import Board from "../board";

export default class CoreTest {
    public static run(): void {
        QUnit.module("core.ts");

        QUnit.test("get gui", function(assert) {
            let core = new Core();
            assert.ok(core.gui instanceof Gui);
        });

        QUnit.test("get board", function(assert) {
            let core = new Core();
            assert.ok(core.board instanceof Board);
        });
    }
}
