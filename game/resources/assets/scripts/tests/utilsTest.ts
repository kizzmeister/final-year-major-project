import Utils from "../utils";

export default class UtilsTest {
    public static run(): void {
        QUnit.module("utils.ts");

        QUnit.test("calculateCoordsBetween", function(assert) {
            assert.deepEqual(Utils.calculateCoordsBetween([5,5], [5,5]), []);

            assert.deepEqual(Utils.calculateCoordsBetween([5,4], [5,6]), [[5,5]]);

            assert.deepEqual(Utils.calculateCoordsBetween([8,7], [8,2]), [[8,6], [8,5], [8,4], [8,3]]);

            assert.deepEqual(Utils.calculateCoordsBetween([3,5], [7,5]), [[4,5], [5,5], [6,5]]);
            assert.deepEqual(Utils.calculateCoordsBetween([9,0], [5,0]), [[8,0], [7,0], [6,0]]);

            assert.throws(function() {
                Utils.calculateCoordsBetween([0,0], [9,9]);
            }, "Unable to calculate coordinates");
        });
    }
}
