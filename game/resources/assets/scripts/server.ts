/// <reference path="./declarations.d.ts"/>
// Using a 'namespace import' to fix errors - https://github.com/Microsoft/TypeScript/issues/5565
import * as Echo from "laravel-echo";
import Board from "./board";
import { GameState } from "./board";
import Player from "./player";
import Space from "./space";
import Piece from "./piece";
import PieceFactory from "./pieceFactory";
import Unknown from "./pieces/12_unknown";
import Utils from "./utils";

// Bring in the game configuration defined globally
// http://stackoverflow.com/questions/13252225/call-a-global-variable-inside-typescript-module
declare var gameConfig: any;

export default class Server {
    private board: Board;
    private guiCallback: any;
    private echo: any;
    private _inCombat: boolean = false;

    constructor(board: Board, guiCallback: any, setupConnection: boolean = true) {
        this.board = board;
        this.guiCallback = guiCallback;

        if (setupConnection) {
            this.echo = new Echo({
                broadcaster: "socket.io",
                host: gameConfig.webSocketServer
            });

            // Setup all AJAX requests to send the CSRF header
            // https://laravel.com/docs/5.4/csrf#csrf-x-csrf-token
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                contentType: "application/json"
            });

            this.setupListeners();
        }
    }

    public get inCombat(): boolean {
        return this._inCombat;
    }

    public set inCombat(inCombat: boolean) {
        this._inCombat = inCombat;
    }

    public setupListeners(): void {
        this.echo.join("game." + this.board.gameToken)
                 .listen("PiecePlacement", (e: any) => {
                     this.piecePlacement(e);
                 })
                 .listen("Turn", (e: any) => {
                     this.turn(e);
                 })
                 .listen("GameState", (e: any) => {
                     this.gameState(e);
                 })
                 .listen("Move", (e: any) => {
                     this.move(e);
                 })
                 .listen("Combat", (e: any) => {
                     this.combat(e);
                 })
    }

    public piecePlacement(event: any) : void {
        if (!event.player || !event.pieces) {
            return;
        }

        let player = this.findPlayer(event.player);
        if (player) this.movePieces(player, event.pieces);

        this.guiCallback();
    }

    public turn(event: any) : void {
        if (!event.turn) {
            return;
        }

        let player = this.findPlayer(event.turn);

        if (player) {
            this.board.turn = player;

            if (player.num === this.board.currentPlayer.num) {
                this.board.disabled = false;
            } else {
                this.board.disabled = true;
            }
        }

        // If we're waiting for combat to complete, delay updating the GUI
        if (!this.inCombat) {
            this.guiCallback();
        }
    }

    public gameState(event: any) : void {
        if (!event.gameState) {
            return;
        }

        let gameState: string = event.gameState;
        if (GameState[gameState]) {
            this.board.gameState = GameState[gameState];
        }

        if (event.winner) {
            this.board.winner = this.findPlayer(event.winner);
        }

        this.guiCallback();
    }

    public move(event: any) : void {
        if (!event.player || !event.piece) {
            return;
        }

        let player = this.findPlayer(event.player);
        if (player) this.movePieces(player, event.piece);

        this.guiCallback();
    }

    public combat(event: any, delay: number = 3000, delayCallback: any = null) : void {
        if (!event.attackingPiece || !event.defendingPiece) {
            return;
        }

        this.inCombat = true;

        let attack = event.attackingPiece;
        let defend = event.defendingPiece;

        let attackingPlayer = this.findPlayer(attack.player);
        let defendingPlayer = this.findPlayer(defend.player);

        if (attackingPlayer && defendingPlayer) {
            let attackingPiece = attackingPlayer.findPiece(attack.token);
            let defendingPiece = defendingPlayer.findPiece(defend.token);

            if (attackingPiece && defendingPiece) {
                // Reveal the pieces if necessary
                attackingPiece.revealed = true;
                defendingPiece.revealed = true;

                if (attackingPiece instanceof Unknown) {
                    let dummyPiece = PieceFactory.getInstance(attack.type, null);
                    attackingPiece.revealAs(dummyPiece);
                }
                if (defendingPiece instanceof Unknown) {
                    let dummyPiece = PieceFactory.getInstance(defend.type, null);
                    defendingPiece.revealAs(dummyPiece);
                }

                // When attacking with the scout, move the piece next to the opponent (if not already there)
                if (attackingPiece.rankName === "Scout") {
                    let originSpace = attackingPiece.space;
                    let targetSpace = defendingPiece.space;

                    let possibleCoords = Utils.calculateCoordsBetween(originSpace.coords, targetSpace.coords);

                    if (possibleCoords.length > 0) {
                        // Move the scout to the 'battle' coordinates, updating the origin space at the same time
                        let battleCoords = possibleCoords[possibleCoords.length-1];
                        let combatSpace = this.board.spaces[battleCoords[0]][battleCoords[1]];

                        originSpace.removePiece();
                        combatSpace.addPiece(attackingPiece);
                    }
                }

                setTimeout(() => {
                    this.handleCombat(attackingPiece, defendingPiece, event);

                    if (delayCallback) {
                        delayCallback();
                    } else {
                        this.guiCallback();
                    }
                }, delay);
            } else {
                this.inCombat = false;
            }
        } else {
            this.inCombat = false;
        }

        this.guiCallback();
    }

    public handleCombat(attackingPiece: Piece, defendingPiece: Piece, event: any) {
        let attack = event.attackingPiece;
        let defend = event.defendingPiece;
        let originSpace = attackingPiece.space;
        let targetSpace = defendingPiece.space;

        attackingPiece.revealed = false;
        defendingPiece.revealed = false;

        originSpace.removePiece();
        targetSpace.removePiece();

        if (attack.winner) {
            targetSpace.addPiece(attackingPiece);
        } else if (defend.winner) {
            targetSpace.addPiece(defendingPiece);
        }

        this.inCombat = false;
    }

    public movePieces(player: Player, pieces: any) : void {
        if (!(pieces instanceof Array)) pieces = [pieces];

        for (let p of pieces) {
            let piece = player.findPiece(p.token);

            if (piece) {
                if (piece.space) piece.space.removePiece();

                let position = this.getPiecePosition(this.board.currentPlayer, [p.row, p.column]);
                this.board.spaces[position[0]][position[1]].addPiece(piece);
            }
        }
    }

    public placePieces(): void {
        let pieces = [];
        for (let piece of this.board.currentPlayer.pieces) {
            pieces.push({
                token: piece.token,
                row: piece.space.coords[0],
                col: piece.space.coords[1]
            });
        }

        // Send the piece positions as JSON - http://stackoverflow.com/questions/6323338/jquery-ajax-posting-json-to-webservice
        $.post("/game/placePieces", JSON.stringify({
            "gameToken": this.board.gameToken,
            "pieces": pieces
        }), (data) => {
             this.board.gameState = GameState.Ready;
             this.guiCallback();
        });
    }

    public movePiece(originSpace: Space, targetSpace: Space, origin: Piece, target: Piece): void {
        // Send the data as JSON - http://stackoverflow.com/questions/6323338/jquery-ajax-posting-json-to-webservice
        $.post({
            "url": "/game/movePiece",
            "data": JSON.stringify({
                "gameToken": this.board.gameToken,
                "piece": {
                    "token": origin.token,
                    "row": targetSpace.coords[0],
                    "col": targetSpace.coords[1]
                }
            }),
            "error": (data) => {
                // If for some reason we recieve an error, revert the move
                origin.space.removePiece(); // The origin may have been changed, so explicity remove the piece from the assigned space
                targetSpace.removePiece();
                originSpace.addPiece(origin);

                if (target !== null) {
                    targetSpace.addPiece(target);
                }

                this.board.decrementNumMoves();
                this.board.disabled = false;
                this.guiCallback();
            }
        });
    }

    public findPlayer(num: number) : Player {
        for (let player of this.board.players) {
            if (player.num === num) {
                return player;
            }
        }

        return null;
    }

    public getPiecePosition(player: Player, position: [number, number]): [number, number] {
        // If player 2, flip the row and column value
        if (player.num === 2) {
            position[0] = Math.abs(position[0] - (this.board.boardSize - 1));
            position[1] = Math.abs(position[1] - (this.board.boardSize - 1));
        }

        return position;
    }
}
