@extends('layouts.bootstrap')

@section('content')
    @include('loginMenu')
    <div id="gameContainer"></div>
    <div id="gameFooter"></div>
@endsection

@section("scripts_extra")
    @if (!empty($gameToken))
        <script src="{{ Config::get("app.web_socket_server")}}/socket.io/socket.io.js"></script>
    @endif
    <script>
        gameConfig = {!! json_encode([
            "webSocketServer" => Config::get("app.web_socket_server"),
        ]) !!};

        $(document).ready(() => {
            @if (empty($gameToken))
                game.init("{{ $aiType }}");
            @else
                game.init(
                    "",
                    "{{ $gameToken }}",
                    "{{ $gameState }}",
                    {{ $playerNum }},
                    @if ($winner)
                        {{ $winner }},
                    @else
                        null,
                    @endif
                    {!! json_encode($playerPieces) !!}
                );
            @endif
        });
    </script>
@endsection
