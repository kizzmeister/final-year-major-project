<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @yield("css_extra")
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <h1>Kieran Dunbar MMP - HTML5 Stratego</h1>

    <div id="container">
        @yield("content")
    </div>

    <!-- Scripts -->
    <script src="{{ asset('external/jquery-3.1.1.js') }}"></script>
    <script src="{{ asset('external/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/game.js') }}"></script>
    @yield("scripts_extra")
</body>
</html>
