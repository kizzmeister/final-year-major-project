<div id="loginMenu">
    @if (Auth::check())
        Logged in as {{ Auth::user()->username }}&nbsp;
        <form action="{{ url("/logout") }}" method="POST">
            {{ csrf_field() }}
            <button id="logout" class="btn btn-default btn-sm">Logout</button>
        </form>
    @else
        <a href="{{ url("/login") }}">Login</a> |
        <a href="{{ url("/register") }}">Register</a>
    @endif
</div>
