@extends('layouts.bootstrap')

@section('content')
    @include('loginMenu')
    <div id="menuContainer">
        @if (session('error'))
            <div class="alert alert-warning">
                {{ session('error') }}
            </div>
        @endif

        <div id="menu">
            <div class="left">
                <form action="{{ url("/game") }}" method="GET">
                    <div class="radio">
                        <label>
                            <input type="radio" name="ai" value="random" checked="checked">
                            Random AI
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="ai" value="improved">
                            Improved AI
                        </label>
                    </div>

                    <button id="singlePlayerGame" class="btn btn-default">Single Player Game</button>
                </form>
            </div>
            <div class="center">
                <form action="{{ url("/game/host") }}" method="POST">
                    {{ csrf_field() }}
                    <button id="hostMultiplayerGame" class="btn btn-default">Host Multiplayer Game</button>
                </form>
            </div>
            <div class="right">
                <form action="{{ url("/game/join") }}" method="POST">
                    {{ csrf_field() }}
                    <div>
                        <input type="text" name="token" class="form-control" placeholder="Game Token" />
                    </div>
                    <br />
                    <button id="joinMultiplayerGame" class="btn btn-default">Join Multiplayer Game</button>
                </form>
            </div>
        </div>
    </div>
@endsection
