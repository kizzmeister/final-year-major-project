<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Game;
use App\Player;

class GameControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * GameController::hostGame test (guest)
     *
     * @return void
     */
    public function testHostGame_guest()
    {
        $response = $this->post("/game/host");
        $response->assertRedirect("/login");

        $this->assertEquals(0, Game::count());
    }

    /**
     * GameController::hostGame test (user)
     *
     * @return void
     */
    public function testHostGame_user()
    {
        $user = factory(\App\User::class)->create();

        $response = $this->actingAs($user)->post("/game/host");
        $game = Game::findOrFail(1);

        $response->assertRedirect("/game/" . $game->token);
        $this->assertDatabaseHas("games", ['token' => $game->token]);

        $player1 = Player::findOrFail(1);
        $this->assertDatabaseHas("players", [
            "id" => $player1->id,
            "game_id" => $game->id,
            "user_id" => $user->id,
            "num" => 1
        ]);

        $player2 = Player::findOrFail(2);
        $this->assertDatabaseHas("players", [
            "id" => $player2->id,
            "game_id" => $game->id,
            "user_id" => null,
            "num" => 2
        ]);

        $playerCount = $game->players()->whereNotNull("user_id")->count();
        $this->assertEquals(1, $playerCount);
    }

    /**
    * GameController::joinGame test (guest)
    *
    * @return void
    */
    public function testJoinGame_guest()
    {
        $game = new Game();
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $response = $this->post("/game/join", ["token" => $game->token]);
        $response->assertRedirect("/login");

        $playerCount = $game->players()->whereNotNull("user_id")->count();
        $this->assertEquals(0, $playerCount);
    }

    /**
    * GameController::joinGame test (logged in)
    *
    * @return void
    */
    public function testJoinGame_user()
    {
        $user = factory(\App\User::class)->create();
        $game = new Game();
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $response = $this->actingAs($user)
                         ->post("/game/join", ["token" => $game->token]);

        $response->assertRedirect("/game/" . $game->token);
        $this->assertDatabaseHas("players", [
            "id" => $player1->id,
            "game_id" => $game->id,
            "user_id" => $user->id,
            "num" => $player1->num
        ]);

        $playerCount = $game->players()->whereNotNull("user_id")->count();
        $this->assertEquals(1, $playerCount);
    }

    /**
    * GameController::joinGame test (refresh while logged in)
    *
    * @return void
    */
    public function testJoinGame_refresh()
    {
        $user = factory(\App\User::class)->create();
        $game = new Game();
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $this->actingAs($user)
             ->post("/game/join", ["token" => $game->token]);
        $response = $this->actingAs($user)
                         ->post("/game/join", ["token" => $game->token]);

        $response->assertRedirect("/game/" . $game->token);
        $this->assertDatabaseHas("players", [
            "id" => $player1->id,
            "game_id" => $game->id,
            "user_id" => $user->id,
            "num" => $player1->num
        ]);

        $playerCount = $game->players()->whereNotNull("user_id")->count();
        $this->assertEquals(1, $playerCount);
    }

    /**
    * GameController::joinGame test (second user joins existing game)
    *
    * @return void
    */
    public function testJoinGame_existing()
    {
        $users = factory(\App\User::class, 2)->create();
        $game = new Game();
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $this->actingAs($users[0])
             ->post("/game/join", ["token" => $game->token]);
        $response = $this->actingAs($users[1])
                         ->post("/game/join", ["token" => $game->token]);

        $response->assertRedirect("/game/" . $game->token);
        $this->assertDatabaseHas("players", [
            "id" => $player2->id,
            "game_id" => $game->id,
            "user_id" => $users[1]->id,
            "num" => $player2->num
        ]);

        $playerCount = $game->players()->whereNotNull("user_id")->count();
        $this->assertEquals(2, $playerCount);
    }

    /**
    * GameController::joinGame test (maximum number of players)
    *
    * @return void
    */
    public function testJoinGame_maxPlayers()
    {
        $users = factory(\App\User::class, 3)->create();
        $game = new Game();
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $this->actingAs($users[0])
             ->post("/game/join", ["token" => $game->token]);
        $this->actingAs($users[1])
             ->post("/game/join", ["token" => $game->token]);

        $response = $this->actingAs($users[2])
                         ->post("/game/join", ["token" => $game->token]);

        $response->assertRedirect("/");
        $response->assertSessionHas("error", "Unable to join Game - Already at maximum number of players");
    }

    /**
    * GameController::joinGame test (no token)
    *
    * @return void
    */
    public function testJoinGame_noToken()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->post("/game/join");

        $response->assertRedirect("/");
        $response->assertSessionHas("error", "Unable to join Game - Invalid Token");
    }

    /**
    * GameController::joinGame test (invalid token)
    *
    * @return void
    */
    public function testJoinGame_invalidToken()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->post("/game/join", ["token" => "test"]);

        $response->assertRedirect("/");
        $response->assertSessionHas("error", "Unable to join Game - Invalid Token");
    }

    /**
     * GameController::playGame test (guest)
     *
     * @return void
     */
    public function testPlayGame_guest()
    {
        $response = $this->get("/game");

        $response->assertRedirect("/login");
    }

    /**
     * GameController::playGame test (single player)
     *
     * @return void
     */
    public function testPlayGame_singlePlayer()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->get("/game");

        $response->assertStatus(200);
        $response->assertViewHas("gameToken", "");
    }

    /**
     * GameController::playGame test (multiplayer)
     *
     * @return void
     */
    public function testPlayGame_multiplayer()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        foreach([$player1, $player2] as $index => $player) {
            $response = $this->actingAs($users[$index])->get("/game/" . $game->token);

            $response->assertStatus(200);
            $response->assertViewHas("gameToken", $game->token);
            $response->assertViewHas("gameState", $game->state);
            $response->assertViewHas("playerNum", $player->num);
            $response->assertViewHas("winner", null);
            $response->assertViewHas("playerPieces");
        }
    }

    /**
     * GameController::playGame test (multiplayer ended)
     *
     * @return void
     */
    public function testPlayGame_multiplayerEnded()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->state = "End";
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $game->winner()->associate($player2);
        $game->save();

        foreach([$player1, $player2] as $index => $player) {
            $response = $this->actingAs($users[$index])->get("/game/" . $game->token);

            $response->assertStatus(200);
            $response->assertViewHas("gameToken", $game->token);
            $response->assertViewHas("gameState", $game->state);
            $response->assertViewHas("playerNum", $player->num);
            $response->assertViewHas("winner", $player2->num);
            $response->assertViewHas("playerPieces");
        }
    }

    /**
     * GameController::playGame test (invalid token)
     *
     * @return void
     */
    public function testPlayGame_invalidToken()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->actingAs($user)->get("/game/test");

        $response->assertRedirect("/");
        $response->assertSessionHas("error", "Unable to join Game - Invalid Token");
    }

    /**
     * GameController::playGame test (no assigned player)
     *
     * @return void
     */
    public function testPlayGame_noPlayer()
    {
        $user = factory(\App\User::class)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $response = $this->actingAs($user)->get("/game/" . $game->token);

        $response->assertRedirect("/");
        $response->assertSessionHas("error", "Unable to join Game - Invalid Token");
    }
}
