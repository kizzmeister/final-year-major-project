<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;

use App\Events\PiecePlacement;
use App\Events\Turn;
use App\Events\GameState;
use App\Events\Move;
use App\Events\Combat;

use App\Http\Controllers\PieceController;
use App\Game;
use App\Player;

class PieceControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $user1;
    private $user2;
    private $game;
    private $player1;
    private $player2;

    private function setupGame()
    {
        $this->user1 = factory(\App\User::class)->create();
        $this->user2 = factory(\App\User::class)->create();

        $this->game = new Game;
        $this->game->generateToken();
        $this->game->save();

        $this->player1 = new Player;
        $this->player1->num = 1;
        $this->player1->game()->associate($this->game);
        $this->player1->user()->associate($this->user1);
        $this->player1->save();
        $this->player1->setupPieces();

        $this->player2 = new Player;
        $this->player2->num = 2;
        $this->player2->game()->associate($this->game);
        $this->player2->user()->associate($this->user2);
        $this->player2->save();
        $this->player2->setupPieces();

        $this->game->turn()->associate($this->player1);
        $this->game->save();
    }

    private function placePieces()
    {
        $players = [$this->player1, $this->player2];

        foreach ($players as $index => $player) {
            $order = ($index === 0) ? "asc" : "desc";
            $row = ($index === 0) ? 6 : 0;
            $col = 0;

            // Place all of the pieces on the board
            foreach ($player->pieces()->orderBy("id", $order)->get() as $piece) {
                $piece->row = $row;
                $piece->column = $col;
                $piece->save();

                if ($col === 9) {
                    $col = 0;
                    $row++;
                } else {
                    $col++;
                }
            }
        }

        $this->game->state = "Play";
        $this->game->save();

        $this->player1 = $this->player1->fresh();
        $this->player2 = $this->player2->fresh();
    }

    /**
     * PieceController::placePieces test (guest)
     *
     * @return void
     */
    public function testPlacePieces_guest()
    {
        $response = $this->post("/game/placePieces");
        $response->assertRedirect("/login");
    }

    /**
     * PieceController::placePieces test (logged in)
     *
     * @return void
     */
    public function testPlacePieces_user()
    {
        $this->setupGame();
        $game = $this->game;
        $players = [$this->player1, $this->player2];
        $users = [$this->user1, $this->user2];
        $pieceCont = new PieceController();

        foreach ($players as $index => $player) {
            $data = [
                "gameToken" => $game->token,
                "pieces" => []
            ];
            $row = 6;
            $col = 0;

            foreach($player->pieces as $piece) {
                $data["pieces"][] = [
                    "token" => $piece->token,
                    "row" => $row,
                    "col" => $col
                ];

                if ($col === 9) {
                    $col = 0;
                    $row++;
                } else {
                    $col++;
                }
            }

            Event::fake();
            $response = $this->actingAs($users[$index])
                             ->json("POST", "/game/placePieces", $data);

            $response->assertStatus(200);
            $response->assertJson(["success" => "Success"]);

            $pos = $pieceCont->getPiecePosition($player, [$data["pieces"][0]["row"], $data["pieces"][0]["col"]]);
            $this->assertDatabaseHas("pieces", [
                "player_id" => $player->id,
                "token" => $data["pieces"][0]["token"],
                "row" => $pos[0],
                "column" => $pos[1]
            ]);

            // Get the updated game and player instances
            $game = $game->fresh();
            $player = $player->fresh();

            Event::assertDispatched(PiecePlacement::class, function($e) use ($game, $player) {
                return $e->game->token === $game->token &&
                       $e->player->token === $player->token &&
                       $e->player->pieces->toArray() === $player->pieces->toArray();
            });

            if ($index === 1) {
                $this->assertDatabaseHas("games", [
                    "token" => $game->token,
                    "state" => "Play",
                    "turn_id" => $game->turn->num
                ]);

                Event::assertDispatched(Turn::class, function($e) use ($game) {
                    return $e->game->turn->num === $game->turn->num;
                });
                Event::assertDispatched(GameState::class, function($e) use ($game) {
                    return $e->game->state === $game->state;
                });
            } else {
                Event::assertNotDispatched(Turn::class);
                Event::assertNotDispatched(GameState::class);
            }
        }
    }

    /**
     * PieceController::placePieces test (invalid game state)
     *
     * @return void
     */
    public function testPlacePieces_invalidGameState()
    {
        $this->setupGame();
        $this->game->state = "Play";
        $this->game->save();

        $data = [
            "gameToken" => $this->game->token,
            "pieces" => []
        ];
        $row = 6;
        $col = 0;

        foreach($this->player1->pieces as $piece) {
            $data["pieces"][] = [
                "token" => $piece->token,
                "row" => $row,
                "col" => $col
            ];

            if ($col === 9) {
                $col = 0;
                $row++;
            } else {
                $col++;
            }
        }

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/placePieces", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to setup piece placement"]);
    }

    /**
     * PieceController::placePieces test (no game token)
     *
     * @return void
     */
    public function testPlacePieces_noToken()
    {
        $this->setupGame();

        $data = [
            "pieces" => []
        ];
        $row = 6;
        $col = 0;

        foreach($this->player1->pieces as $piece) {
            $data["pieces"][] = [
                "token" => $piece->token,
                "row" => $row,
                "col" => $col
            ];

            if ($col === 9) {
                $col = 0;
                $row++;
            } else {
                $col++;
            }
        }

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/placePieces", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to setup piece placement"]);
    }

    /**
     * PieceController::placePieces test (invalid game token)
     *
     * @return void
     */
    public function testPlacePieces_invalidToken()
    {
        $this->setupGame();

        $data = [
            "gameToken" => "testing",
            "pieces" => []
        ];
        $row = 6;
        $col = 0;

        foreach($this->player1->pieces as $piece) {
            $data["pieces"][] = [
                "token" => $piece->token,
                "row" => $row,
                "col" => $col
            ];

            if ($col === 9) {
                $col = 0;
                $row++;
            } else {
                $col++;
            }
        }

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/placePieces", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to setup piece placement"]);
    }

    /**
     * PieceController::placePieces test (invalid piece tokens)
     *
     * @return void
     */
    public function testPlacePieces_invalidPieceToken()
    {
        $this->setupGame();

        $data = [
            "gameToken" => $this->game->token,
            "pieces" => []
        ];
        $row = 6;
        $col = 0;

        foreach($this->player1->pieces as $piece) {
            $data["pieces"][] = [
                "token" => "test",
                "row" => $row,
                "col" => $col
            ];

            if ($col === 9) {
                $col = 0;
                $row++;
            } else {
                $col++;
            }
        }

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/placePieces", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to setup piece placement"]);
    }

    /**
     * PieceController::placePieces test (invalid piece positions)
     *
     * @return void
     */
    public function testPlacePieces_invalidPiecePosition()
    {
        $this->setupGame();

        $data = [
            "gameToken" => $this->game->token,
            "pieces" => []
        ];
        $row = 6;
        $col = 0;

        foreach($this->player1->pieces as $piece) {
            $data["pieces"][] = [
                "token" => $piece->token,
                "row" => "test",
                "col" => null
            ];

            if ($col === 9) {
                $col = 0;
                $row++;
            } else {
                $col++;
            }
        }

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/placePieces", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to setup piece placement"]);
    }

    /**
     * PieceController::placePieces test (invalid piece range)
     *
     * @return void
     */
    public function testPlacePieces_invalidPieceRange()
    {
        $this->setupGame();
        $players = [$this->player1, $this->player2];
        $users = [$this->user1, $this->user2];

        foreach ($players as $index => $player) {
            $data = [
                "gameToken" => $this->game->token,
                "pieces" => []
            ];
            $row = 0;
            $col = 0;

            foreach($player->pieces as $piece) {
                $data["pieces"][] = [
                    "token" => $piece->token,
                    "row" => $row,
                    "col" => $col
                ];

                if ($col === 9) {
                    $col = 0;
                    $row++;
                } else {
                    $col++;
                }
            }

            $response = $this->actingAs($users[$index])
                             ->json("POST", "/game/placePieces", $data);

            $response->assertStatus(500);
            $response->assertJson(["error" => "Unable to setup piece placement"]);
        }
    }

    /**
     * PieceController::movePiece test (guest)
     *
     * @return void
     */
    public function testMovePiece_guest()
    {
        $response = $this->post("/game/movePiece");
        $response->assertRedirect("/login");
    }

    /**
     * PieceController::movePiece test (logged in with no combat)
     *
     * @return void
     */
    public function testMovePiece_userNoCombat()
    {
        $this->setupGame();
        $this->placePieces();

        $game = $this->game;
        $players = [$this->player1, $this->player2];
        $users = [$this->user1, $this->user2];
        $pieceCont = new PieceController();

        foreach ($players as $index => $player) {
            $piece = $player->pieces()->orderBy("id")->first();

            $row = ($index === 0) ? $piece->row - 1 : $piece->row + 1;
            $pos = $pieceCont->getPiecePosition($player, [$row, $piece->column]);
            $data = [
                "gameToken" => $this->game->token,
                "piece" => [
                    "token" => $piece->token,
                    "row" => $pos[0],
                    "col" => $pos[1]
                ]
            ];

            Event::fake();
            $response = $this->actingAs($users[$index])
                             ->json("POST", "/game/movePiece", $data);

            $response->assertStatus(200);
            $response->assertJson(["success" => "Success"]);

            $pos = $pieceCont->getPiecePosition($player, [$data["piece"]["row"], $data["piece"]["col"]]);
            $this->assertDatabaseHas("pieces", [
                "player_id" => $player->id,
                "token" => $data["piece"]["token"],
                "row" => $pos[0],
                "column" => $pos[1]
            ]);

            // Get the updated game instance and the next player
            $game = $game->fresh();
            $nextPlayer = $pieceCont->getNextPlayer($game, $player);

            Event::assertDispatched(Move::class, function($e) use ($game, $data, $pos) {
                $piece = [
                    "token" => $data['piece']['token'],
                    "row" => $pos[0],
                    "column" => $pos[1]
                ];

                return $e->game->token === $game->token &&
                       $e->piece->toArray() === $piece;
            });

            $this->assertDatabaseHas("games", [
                "token" => $game->token,
                "turn_id" => $nextPlayer->num
            ]);

            Event::assertDispatched(Turn::class, function($e) use ($game, $nextPlayer) {
                return $e->game->turn->num === $nextPlayer->num;
            });
        }
    }

    /**
     * PieceController::movePiece test (logged in with combat)
     *
     * @return void
     */
    public function testMovePiece_userCombat()
    {
        $this->setupGame();
        $this->placePieces();

        $game = $this->game;
        $players = [$this->player1, $this->player2];
        $users = [$this->user1, $this->user2];
        $pieceCont = new PieceController();

        $attackingPiece = $players[0]->pieces->where("type", "Marshall")->first();
        $defendingPiece = $players[1]->pieces->where("type", "Miner")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        $data = [
            "gameToken" => $game->token,
            "piece" => [
                "token" => $attackingPiece->token,
                "row" => 0,
                "col" => 1
            ]
        ];

        Event::fake();
        $response = $this->actingAs($users[0])
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(200);
        $response->assertJson(["success" => "Success"]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $players[0]->id,
            "token" => $data["piece"]["token"],
            "row" => $data["piece"]["row"],
            "column" => $data["piece"]["col"]
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $players[1]->id,
            "token" => $defendingPiece->token,
            "row" => null,
            "column" => null
        ]);

        // Defeat and draw scenarios of this event are tested by handleCombat tests
        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner->token === $attackingPiece->token;
        });

        // Get the updated game instance and the next player
        $game = $game->fresh();
        $nextPlayer = $pieceCont->getNextPlayer($game, $players[0]);

        $this->assertDatabaseHas("games", [
            "token" => $game->token,
            "turn_id" => $nextPlayer->num
        ]);

        Event::assertDispatched(Turn::class, function($e) use ($game, $nextPlayer) {
            return $e->game->turn->num === $nextPlayer->num;
        });
    }

    /**
     * PieceController::movePiece test (invalid game state)
     *
     * @return void
     */
    public function testMovePiece_invalidGameState()
    {
        $this->setupGame();
        $this->placePieces();

        $this->game->state = "Placement";
        $this->game->save();

        $piece = $this->player1->pieces()->orderBy("id")->first();
        $data = [
            "gameToken" => $this->game->token,
            "piece" => [
                "token" => $piece->token,
                "row" => $piece->row - 1,
                "col" => $piece->column
            ]
        ];

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to move piece"]);
    }

    /**
     * PieceController::movePiece test (no game token)
     *
     * @return void
     */
    public function testMovePiece_noToken()
    {
        $this->setupGame();
        $this->placePieces();

        $piece = $this->player1->pieces()->orderBy("id")->first();
        $data = [
            "piece" => [
                "token" => $piece->token,
                "row" => $piece->row - 1,
                "col" => $piece->column
            ]
        ];

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to move piece"]);
    }

    /**
     * PieceController::movePiece test (invalid game token)
     *
     * @return void
     */
    public function testMovePiece_invalidToken()
    {
        $this->setupGame();
        $this->placePieces();

        $piece = $this->player1->pieces()->orderBy("id")->first();
        $data = [
            "gameToken" => "testing",
            "piece" => [
                "token" => $piece->token,
                "row" => $piece->row - 1,
                "col" => $piece->column
            ]
        ];

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to move piece"]);
    }

    /**
     * PieceController::movePiece test (invalid piece token)
     *
     * @return void
     */
    public function testMovePiece_invalidPieceToken()
    {
        $this->setupGame();
        $this->placePieces();

        $piece = $this->player1->pieces()->orderBy("id")->first();
        $data = [
            "gameToken" => $this->game->token,
            "piece" => [
                "token" => "test",
                "row" => $piece->row - 1,
                "col" => $piece->column
            ]
        ];

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to move piece"]);
    }

    /**
     * PieceController::movePiece test (invalid piece position)
     *
     * @return void
     */
    public function testMovePiece_invalidPiecePosition()
    {
        $this->setupGame();
        $this->placePieces();

        // This will be a Marshall, so moving more than one space is not allowed
        $piece = $this->player1->pieces()->orderBy("id")->first();
        $data = [
            "gameToken" => $this->game->token,
            "piece" => [
                "token" => $piece->token,
                "row" => $piece->row - 2,
                "col" => $piece->column
            ]
        ];

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to move piece"]);
    }

    /**
     * PieceController::movePiece test (invalid turn)
     *
     * @return void
     */
    public function testMovePiece_invalidTurn()
    {
        $this->setupGame();
        $this->placePieces();

        $this->game->turn()->associate($this->player2);
        $this->game->save();

        $piece = $this->player1->pieces()->orderBy("id")->first();
        $data = [
            "gameToken" => $this->game->token,
            "piece" => [
                "token" => $piece->token,
                "row" => $piece->row - 1,
                "col" => $piece->column
            ]
        ];

        $response = $this->actingAs($this->user1)
                         ->json("POST", "/game/movePiece", $data);

        $response->assertStatus(500);
        $response->assertJson(["error" => "Unable to move piece"]);
    }
}
