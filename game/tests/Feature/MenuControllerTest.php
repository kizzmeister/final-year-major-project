<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MenuControllerTest extends TestCase
{
    /**
     * MenuController:index test
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get("/");
        $response->assertStatus(200);
    }
}
