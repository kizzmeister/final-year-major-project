<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Piece;

class PieceTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Piece::canDefeat (default)
     *
     * @return void
     */
    public function testCanDefeat_default()
    {
        $pieces = [];
        foreach (array_slice(Piece::RANKS, 1, 10) as $index => $rank) {
            $pieces[$index] = new Piece;
            $pieces[$index]->type = $rank;
        }

        $bomb = new Piece;
        $bomb->type = "Bomb";
        $flag = new Piece;
        $flag->type = "Flag";

        foreach ($pieces as $index => $piece) {
            // Skip Miners and Spies as we test those separately
            if ($index === 7 || $index === 9) continue;

            foreach ($pieces as $pIndex => $p) {
                if ($index !== $pIndex) {
                    $this->assertEquals($index <= $pIndex, $piece->canDefeat($p));
                }
            }

            $this->assertTrue($piece->canDefeat($piece));
            $this->assertFalse($piece->canDefeat($bomb));
            $this->assertTrue($bomb->canDefeat($piece));
            $this->assertTrue($piece->canDefeat($flag));
            $this->assertFalse($flag->canDefeat($piece));
        }
    }

    /**
     * Piece::canDefeat (miner)
     *
     * @return void
     */
    public function testCanDefeat_miner()
    {
        $pieces = [];
        foreach (array_slice(Piece::RANKS, 1, 7) as $index => $rank) {
            $pieces[$index] = new Piece;
            $pieces[$index]->type = $rank;
        }

        $miner = new Piece;
        $miner->type = "Miner";
        $scout = new Piece;
        $scout->type = "Scout";
        $spy = new Piece;
        $spy->type = "Spy";
        $bomb = new Piece;
        $bomb->type = "Bomb";
        $flag = new Piece;
        $flag->type = "Flag";

        foreach ($pieces as $piece) {
            $this->assertFalse($miner->canDefeat($piece));
        }

        $this->assertTrue($miner->canDefeat($miner));
        $this->assertTrue($miner->canDefeat($scout));
        $this->assertTrue($miner->canDefeat($spy));

        $this->assertTrue($miner->canDefeat($bomb));
        $this->assertTrue($bomb->canDefeat($miner));

        $this->assertTrue($miner->canDefeat($flag));
        $this->assertFalse($flag->canDefeat($miner));
    }

    /**
     * Piece::canDefeat (spy)
     *
     * @return void
     */
    public function testCanDefeat_spy()
    {
        $pieces = [];
        foreach (array_slice(Piece::RANKS, 2, 8) as $index => $rank) {
            $pieces[$index] = new Piece;
            $pieces[$index]->type = $rank;
        }

        $spy = new Piece;
        $spy->type = "Spy";
        $marshall = new Piece;
        $marshall->type = "Marshall";
        $bomb = new Piece;
        $bomb->type = "Bomb";
        $flag = new Piece;
        $flag->type = "Flag";

        foreach ($pieces as $piece) {
            $this->assertFalse($spy->canDefeat($piece));
        }

        $this->assertTrue($spy->canDefeat($spy));
        $this->assertTrue($spy->canDefeat($marshall));

        $this->assertFalse($spy->canDefeat($bomb));
        $this->assertTrue($bomb->canDefeat($spy));

        $this->assertTrue($spy->canDefeat($flag));
        $this->assertFalse($flag->canDefeat($spy));
    }
}
