<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Events\Turn;
use App\Game;
use App\Player;

class TurnTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Turn::broadcastWith test
     *
     * @return void
     */
    public function testBroadcastWith()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $game->turn()->associate($player2);
        $game->save();

        $event = new Turn($game);
        $result = $event->broadcastWith();

        $this->assertEquals($player2->num, $result['turn']);
    }

    /**
     * PiecePlacement::broadcastOn test
     *
     * @return void
     */
    public function testBroadcastOn()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $game->turn()->associate($player2);

        $event = new Turn($game);
        $channel = $event->broadcastOn();

        $this->assertEquals("presence-game." . $game->token, $channel->name);
    }
}
