<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Events\GameState;
use App\Game;
use App\Player;

class GameStateTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Turn::broadcastWith test
     *
     * @return void
     */
    public function testBroadcastWith()
    {
        $game = new Game;
        $game->state = "Play";
        $game->generateToken();
        $game->save();

        $event = new GameState($game);
        $result = $event->broadcastWith();

        $this->assertEquals($game->state, $result['gameState']);
    }

    /**
     * Turn::broadcastWith test (end gamestate)
     *
     * @return void
     */
    public function testBroadcastWith_End()
    {
        $game = new Game;
        $game->state = "End";
        $game->generateToken();
        $game->save();

        $player = new Player;
        $player->num = 2;
        $player->game()->associate($game);
        $player->save();

        $game->winner()->associate($player);
        $game->save();

        $event = new GameState($game);
        $result = $event->broadcastWith();

        $this->assertEquals($game->state, $result['gameState']);
        $this->assertEquals($player->num, $result['winner']);
    }

    /**
     * Turn::broadcastWith test (end gamestate and no winner)
     *
     * @return void
     */
    public function testBroadcastWith_EndDraw()
    {
        $game = new Game;
        $game->state = "End";
        $game->generateToken();
        $game->save();

        $event = new GameState($game);
        $result = $event->broadcastWith();

        $this->assertEquals($game->state, $result['gameState']);
        $this->assertEquals(null, $result['winner']);
    }

    /**
     * PiecePlacement::broadcastOn test
     *
     * @return void
     */
    public function testBroadcastOn()
    {
        $game = new Game;
        $game->state = "Play";
        $game->generateToken();
        $game->save();

        $event = new GameState($game);
        $channel = $event->broadcastOn();

        $this->assertEquals("presence-game." . $game->token, $channel->name);
    }
}
