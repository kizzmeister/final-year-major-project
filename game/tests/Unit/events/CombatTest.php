<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Events\Combat;
use App\Game;
use App\Player;

class CombatTest extends TestCase
{
    use DatabaseMigrations;

    private $game;
    private $player1;
    private $player2;
    private $attackingPiece;
    private $defendingPiece;

    private function setupGame()
    {
        $users = factory(\App\User::class, 2)->create();

        $this->game = new Game;
        $this->game->generateToken();
        $this->game->save();

        $this->player1 = new Player;
        $this->player1->num = 1;
        $this->player1->game()->associate($this->game);
        $this->player1->user()->associate($users[0]);
        $this->player1->save();
        $this->player1->setupPieces();

        $this->player2 = new Player;
        $this->player2->num = 2;
        $this->player2->game()->associate($this->game);
        $this->player2->user()->associate($users[1]);
        $this->player2->save();
        $this->player2->setupPieces();

        $this->attackingPiece = $this->player1->pieces->first();
        $this->attackingPiece->row = 3;
        $this->attackingPiece->column = 5;
        $this->attackingPiece->save();

        $this->defendingPiece = $this->player2->pieces->first();
        $this->defendingPiece->row = 3;
        $this->defendingPiece->column = 6;
        $this->defendingPiece->save();
    }

    /**
     * Move::broadcastWith test (attack win)
     *
     * @return void
     */
    public function testBroadcastWith_attackWin()
    {
        $this->setupGame();

        $event = new Combat(
            $this->game,
            $this->attackingPiece,
            $this->defendingPiece,
            $this->attackingPiece
        );
        $result = $event->broadcastWith();

        $this->assertEquals([
            "attackingPiece" => [
                "player" => $this->player1->num,
                "type" => $this->attackingPiece->type,
                "winner" => true,
                "token" => $this->attackingPiece->token,
                "row" => $this->attackingPiece->row,
                "column" => $this->attackingPiece->column
            ],
            "defendingPiece" => [
                "player" => $this->player2->num,
                "type" => $this->defendingPiece->type,
                "winner" => false,
                "token" => $this->defendingPiece->token,
                "row" => $this->defendingPiece->row,
                "column" => $this->defendingPiece->column
            ]
        ], $result);
    }

    /**
     * Move::broadcastWith test (attack loss)
     *
     * @return void
     */
    public function testBroadcastWith_attackLoss()
    {
        $this->setupGame();

        $event = new Combat(
            $this->game,
            $this->attackingPiece,
            $this->defendingPiece,
            $this->defendingPiece
        );
        $result = $event->broadcastWith();

        $this->assertEquals([
            "attackingPiece" => [
                "player" => $this->player1->num,
                "type" => $this->attackingPiece->type,
                "winner" => false,
                "token" => $this->attackingPiece->token,
                "row" => $this->attackingPiece->row,
                "column" => $this->attackingPiece->column
            ],
            "defendingPiece" => [
                "player" => $this->player2->num,
                "type" => $this->defendingPiece->type,
                "winner" => true,
                "token" => $this->defendingPiece->token,
                "row" => $this->defendingPiece->row,
                "column" => $this->defendingPiece->column
            ]
        ], $result);
    }

    /**
     * Move::broadcastWith test (draw)
     *
     * @return void
     */
    public function testBroadcastWith_draw()
    {
        $this->setupGame();

        $event = new Combat(
            $this->game,
            $this->attackingPiece,
            $this->defendingPiece,
            null
        );
        $result = $event->broadcastWith();

        $this->assertEquals([
            "attackingPiece" => [
                "player" => $this->player1->num,
                "type" => $this->attackingPiece->type,
                "winner" => false,
                "token" => $this->attackingPiece->token,
                "row" => $this->attackingPiece->row,
                "column" => $this->attackingPiece->column
            ],
            "defendingPiece" => [
                "player" => $this->player2->num,
                "type" => $this->defendingPiece->type,
                "winner" => false,
                "token" => $this->defendingPiece->token,
                "row" => $this->defendingPiece->row,
                "column" => $this->defendingPiece->column
            ]
        ], $result);
    }

    /**
     * PiecePlacement::broadcastOn test
     *
     * @return void
     */
    public function testBroadcastOn()
    {
        $this->setupGame();

        $event = new Combat(
            $this->game,
            $this->attackingPiece,
            $this->defendingPiece,
            null
        );
        $channel = $event->broadcastOn();

        $this->assertEquals("presence-game." . $this->game->token, $channel->name);
    }
}
