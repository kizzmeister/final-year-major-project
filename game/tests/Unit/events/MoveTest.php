<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Events\Move;
use App\Game;
use App\Player;

class MoveTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Move::broadcastWith test
     *
     * @return void
     */
    public function testBroadcastWith()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();
        $player1->setupPieces();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $piece = $player1->pieces->first();
        $piece->row = 3;
        $piece->column = 5;
        $piece->save();

        $event = new Move($game, $piece);
        $result = $event->broadcastWith();

        $this->assertEquals([
            "player" => $player1->num,
            "piece" => [
                "token" => $piece->token,
                "row" => $piece->row,
                "column" => $piece->column
            ]
        ], $result);
    }

    /**
     * PiecePlacement::broadcastOn test
     *
     * @return void
     */
    public function testBroadcastOn()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();
        $player1->setupPieces();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $piece = $player1->pieces->first();

        $event = new Move($game, $piece);
        $channel = $event->broadcastOn();

        $this->assertEquals("presence-game." . $game->token, $channel->name);
    }
}
