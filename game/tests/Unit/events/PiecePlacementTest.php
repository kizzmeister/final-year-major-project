<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Events\PiecePlacement;
use App\Game;
use App\Player;

class PiecePlacementTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * PiecePlacement::broadcastWith test
     *
     * @return void
     */
    public function testBroadcastWith()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();
        $player1->setupPieces();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $event = new PiecePlacement($game, $player1);
        $result = $event->broadcastWith();

        $this->assertEquals($player1->num, $result['player']);
        $this->assertEquals($player1->pieces()->count(), count($result['pieces']));
        $this->assertEquals(["token", "row", "column"], array_keys($result['pieces'][0]));
    }

    /**
     * PiecePlacement::broadcastOn test
     *
     * @return void
     */
    public function testBroadcastOn()
    {
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();
        $player1->setupPieces();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $event = new PiecePlacement($game, $player1);
        $channel = $event->broadcastOn();

        $this->assertEquals("presence-game." . $game->token, $channel->name);
    }
}
