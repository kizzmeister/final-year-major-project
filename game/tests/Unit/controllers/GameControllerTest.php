<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Http\Controllers\GameController;
use App\Game;
use App\Player;
use App\Piece;

class GameControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $game;
    private $players;
    private $users;

    private function setupGame()
    {
        $this->users = factory(\App\User::class, 2)->create();

        $this->game = new Game;
        $this->game->generateToken();
        $this->game->save();

        $this->players[0] = new Player;
        $this->players[0]->num = 1;
        $this->players[0]->game()->associate($this->game);
        $this->players[0]->user()->associate($this->users[0]);
        $this->players[0]->save();
        $this->players[0]->setupPieces();

        $this->players[1] = new Player;
        $this->players[1]->num = 2;
        $this->players[1]->game()->associate($this->game);
        $this->players[1]->user()->associate($this->users[1]);
        $this->players[1]->save();
        $this->players[1]->setupPieces();
    }

    /**
     * GameController::getPlayerPieces test (when game in placement)
     *
     * @return void
     */
    public function testGetPlayerPieces_placement()
    {
        $gameController = new GameController();
        $this->setupGame();

        $this->game->turn()->associate($this->players[1]);
        $this->game->save();

        $result = $gameController->getPlayerPieces($this->game, $this->players[0], false);

        $this->assertFalse($result[0]["turn"]);
        $this->assertTrue($result[1]["turn"]);

        foreach ($result as $index => $value) {
            $piece = Piece::where("player_id", $this->players[$index]->id)->orderBy("id")->firstOrFail();
            $type = $piece->type;

            if ($index === 1) {
                $type = "Unknown";
            }

            $this->assertEquals($this->players[$index]->num, $value["player"]);
            $this->assertEquals($piece->token, $value["pieces"][0]["token"]);
            $this->assertEquals($type, $value["pieces"][0]["type"]);
            $this->assertEquals($piece->row, $value["pieces"][0]["row"]);
            $this->assertEquals($piece->column, $value["pieces"][0]["col"]);
        }
    }

    /**
     * GameController::getPlayerPieces test (when game in play)
     *
     * @return void
     */
    public function testGetPlayerPieces_play()
    {
        $gameController = new GameController();
        $this->setupGame();

        $this->game->state = "Play";
        $this->game->turn()->associate($this->players[1]);
        $this->game->save();

        $result = $gameController->getPlayerPieces($this->game, $this->players[0], false);

        $this->assertFalse($result[0]["turn"]);
        $this->assertTrue($result[1]["turn"]);

        foreach ($result as $index => $value) {
            $piece = Piece::where("player_id", $this->players[$index]->id)->orderBy("id")->firstOrFail();

            $this->assertEquals($this->players[$index]->num, $value["player"]);
            $this->assertEquals($piece->token, $value["pieces"][0]["token"]);
            $this->assertEquals($piece->type, $value["pieces"][0]["type"]);
            $this->assertEquals($piece->row, $value["pieces"][0]["row"]);
            $this->assertEquals($piece->column, $value["pieces"][0]["col"]);
        }
    }
}
