<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

use App\Http\Controllers\Controller;
use App\Events\GameState;
use App\Game;
use App\Player;

class ControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $game;
    private $players;
    private $users;

    private function setupGame()
    {
        $this->users = factory(\App\User::class, 2)->create();

        $this->game = new Game;
        $this->game->generateToken();
        $this->game->save();

        $this->players[0] = new Player;
        $this->players[0]->num = 1;
        $this->players[0]->game()->associate($this->game);
        $this->players[0]->user()->associate($this->users[0]);
        $this->players[0]->save();
        $this->players[0]->setupPieces();

        $this->players[1] = new Player;
        $this->players[1]->num = 2;
        $this->players[1]->game()->associate($this->game);
        $this->players[1]->user()->associate($this->users[1]);
        $this->players[1]->save();
        $this->players[1]->setupPieces();
    }

    /**
     * Controller::getPlayerFromGame test
     *
     * @return void
     */
    public function testGetPlayerPieces()
    {
        $controller = new Controller();
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $this->assertFalse($controller->getPlayerFromGame(null));
        $this->assertFalse($controller->getPlayerFromGame($game));

        Auth::login($users[0]);
        $this->assertEquals($player1->id, $controller->getPlayerFromGame($game)->id);

        Auth::login($users[1]);
        $this->assertEquals($player2->id, $controller->getPlayerFromGame($game)->id);
    }

    /**
     * Controller::getRandomPlayer test
     *
     * @return void
     */
    public function testGetRandomPlayer()
    {
        $controller = new Controller();
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $randomPlayer = $controller->getRandomPlayer($game);
        $this->assertEquals($player1->id, $randomPlayer->id);

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $randomPlayer = $controller->getRandomPlayer($game);
        $players = [$player1->id, $player2->id];
        $this->assertTrue(in_array($randomPlayer->id, $players));
    }

    /**
     * Controller::getNextPlayer test
     *
     * @return void
     */
    public function testGetNextPlayer()
    {
        $controller = new Controller();
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();

        $nextPlayer = $controller->getNextPlayer($game, $player1);
        $this->assertEquals($player1->id, $nextPlayer->id);

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();

        $nextPlayer = $controller->getNextPlayer($game, $player1);
        $this->assertEquals($player2->id, $nextPlayer->id);

        $nextPlayer = $controller->getNextPlayer($game, $player2);
        $this->assertEquals($player1->id, $nextPlayer->id);
    }

    /**
     * Controller::gameHasUnplacedPieces test
     *
     * @return void
     */
    public function testGameHasUnplacedPieces()
    {
        $controller = new Controller();
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();
        $player1->setupPieces();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();
        $player2->setupPieces();

        $this->assertTrue($controller->gameHasUnplacedPieces($game));

        foreach ($player1->pieces as $piece) {
            $piece->row = 0;
            $piece->column = 0;
            $piece->save();
        }
        $this->assertTrue($controller->gameHasUnplacedPieces($game));

        foreach ($player2->pieces as $piece) {
            $piece->row = 0;
            $piece->column = 0;
            $piece->save();
        }
        $this->assertFalse($controller->gameHasUnplacedPieces($game));
    }

    /**
     * Controller::playerHasUnplacedPieces test
     *
     * @return void
     */
    public function testPlayerHasUnplacedPieces()
    {
        $controller = new Controller();
        $users = factory(\App\User::class, 2)->create();

        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->user()->associate($users[0]);
        $player1->save();
        $player1->setupPieces();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->user()->associate($users[1]);
        $player2->save();
        $player2->setupPieces();

        $this->assertTrue($controller->playerHasUnplacedPieces($player1));
        $this->assertTrue($controller->playerHasUnplacedPieces($player2));

        foreach ($player1->pieces as $piece) {
            $piece->row = 0;
            $piece->column = 0;
            $piece->save();
        }
        $this->assertFalse($controller->playerHasUnplacedPieces($player1));
    }

    /**
     * Controller:playerHasMovablePieces test
     *
     * @return void
     */
    public function testPlayerHasMovablePieces()
    {
        $this->setupGame();
        $controller = new Controller();

        foreach ($this->players as $player) {
            foreach ($player->pieces()->get() as $piece) {
                $piece->row = 0;
                $piece->column = 0;
                $piece->save();
            }

            $this->assertTrue($controller->playerHasMovablePieces($player));

            foreach ($player->pieces()->whereNotIn("type", ["Flag", "Bomb"])->get() as $piece) {
                $piece->row = null;
                $piece->column = null;
                $piece->save();
            }

            $this->assertFalse($controller->playerHasMovablePieces($player));
        }
    }

    /**
     * Controller::endGame test (with winner)
     *
     * @return void
     */
    public function testEndGame_winner()
    {
        $this->setupGame();
        $controller = new Controller();
        Event::fake();

        $game = $this->game;
        $player = $this->players[1];
        $controller->endGame($game, $player);

        Event::assertDispatched(GameState::class, function($e) use ($game, $player) {
            return $e->game->token === $game->token &&
                   $e->game->state === $game->state &&
                   $e->game->winner->token === $player->token;
        });

        $this->assertDatabaseHas("games", [
            "id" => $game->id,
            "state" => "End",
            "winner_id" => $player->id
        ]);
    }

    /**
     * Controller::endGame test (draw)
     *
     * @return void
     */
    public function testEndGame_draw()
    {
        $this->setupGame();
        $controller = new Controller();
        Event::fake();

        $game = $this->game;
        $controller->endGame($game);

        Event::assertDispatched(GameState::class, function($e) use ($game) {
            return $e->game->token === $game->token &&
                   $e->game->state === $game->state &&
                   $e->game->winner === null;
        });

        $this->assertDatabaseHas("games", [
            "id" => $game->id,
            "state" => "End",
            "winner_id" => null
        ]);
    }

    /**
     * Controller::getPiecePosition test
     *
     * @return void
     */
    public function testGetPiecePosition()
    {
        $game = new Game;
        $game->generateToken();
        $game->save();

        $player1 = new Player;
        $player1->num = 1;
        $player1->game()->associate($game);
        $player1->save();

        $player2 = new Player;
        $player2->num = 2;
        $player2->game()->associate($game);
        $player2->save();

        $controller = new Controller();
        $board = [
            [0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],[0,9],
            [1,0],[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],
            [2,0],[2,1],[2,2],[2,3],[2,4],[2,5],[2,6],[2,7],[2,8],[2,9],
            [3,0],[3,1],[3,2],[3,3],[3,4],[3,5],[3,6],[3,7],[3,8],[3,9],
            [4,0],[4,1],[4,2],[4,3],[4,4],[4,5],[4,6],[4,7],[4,8],[4,9],
            [5,0],[5,1],[5,2],[5,3],[5,4],[5,5],[5,6],[5,7],[5,8],[5,9],
            [6,0],[6,1],[6,2],[6,3],[6,4],[6,5],[6,6],[6,7],[6,8],[6,9],
            [7,0],[7,1],[7,2],[7,3],[7,4],[7,5],[7,6],[7,7],[7,8],[7,9],
            [8,0],[8,1],[8,2],[8,3],[8,4],[8,5],[8,6],[8,7],[8,8],[8,9],
            [9,0],[9,1],[9,2],[9,3],[9,4],[9,5],[9,6],[9,7],[9,8],[9,9]
        ];
        $flipped = [
            [9,9],[9,8],[9,7],[9,6],[9,5],[9,4],[9,3],[9,2],[9,1],[9,0],
            [8,9],[8,8],[8,7],[8,6],[8,5],[8,4],[8,3],[8,2],[8,1],[8,0],
            [7,9],[7,8],[7,7],[7,6],[7,5],[7,4],[7,3],[7,2],[7,1],[7,0],
            [6,9],[6,8],[6,7],[6,6],[6,5],[6,4],[6,3],[6,2],[6,1],[6,0],
            [5,9],[5,8],[5,7],[5,6],[5,5],[5,4],[5,3],[5,2],[5,1],[5,0],
            [4,9],[4,8],[4,7],[4,6],[4,5],[4,4],[4,3],[4,2],[4,1],[4,0],
            [3,9],[3,8],[3,7],[3,6],[3,5],[3,4],[3,3],[3,2],[3,1],[3,0],
            [2,9],[2,8],[2,7],[2,6],[2,5],[2,4],[2,3],[2,2],[2,1],[2,0],
            [1,9],[1,8],[1,7],[1,6],[1,5],[1,4],[1,3],[1,2],[1,1],[1,0],
            [0,9],[0,8],[0,7],[0,6],[0,5],[0,4],[0,3],[0,2],[0,1],[0,0]
        ];

        $result = [];
        foreach ($board as $piece) {
            $result[] = $controller->getPiecePosition($player1, $piece);
        }
        $this->assertEquals($board, $result);

        $result = [];
        foreach ($board as $piece) {
            $result[] = $controller->getPiecePosition($player2, $piece);
        }
        $this->assertEquals($flipped, $result);

        $this->assertEquals([null, null], $controller->getPiecePosition($player1, [null, null]));
        $this->assertEquals([null, null], $controller->getPiecePosition($player2, [null, null]));
    }
}
