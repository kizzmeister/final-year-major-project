<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;

use App\Http\Controllers\PieceController;
use App\Events\Combat;
use App\Events\GameState;
use App\Game;
use App\Player;
use App\Piece;

class PieceControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $game;
    private $players;
    private $users;

    private function setupGame()
    {
        $this->users = factory(\App\User::class, 2)->create();

        $this->game = new Game;
        $this->game->generateToken();
        $this->game->save();

        $this->players[0] = new Player;
        $this->players[0]->num = 1;
        $this->players[0]->game()->associate($this->game);
        $this->players[0]->user()->associate($this->users[0]);
        $this->players[0]->save();
        $this->players[0]->setupPieces();

        $this->players[1] = new Player;
        $this->players[1]->num = 2;
        $this->players[1]->game()->associate($this->game);
        $this->players[1]->user()->associate($this->users[1]);
        $this->players[1]->save();
        $this->players[1]->setupPieces();
    }

    /**
     * PieceController:handleCombat test (attack win)
     *
     * @return void
     */
    public function testHandleCombat_attackWin()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        // Place all of the pieces, to ensure the game doesn't end
        foreach ($this->players as $player) {
            foreach ($player->pieces as $piece) {
                $piece->row = 9;
                $piece->column = 9;
                $piece->save();
            }
        }

        $attackingPiece = $this->players[0]->pieces->where("type", "Marshall")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Miner")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => 0,
            "column" => 1
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => null,
            "column" => null
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner->token === $attackingPiece->token;
        });
    }

    /**
     * PieceController:handleCombat test (defender win)
     *
     * @return void
     */
    public function testHandleCombat_defendWin()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        // Place all of the pieces, to ensure the game doesn't end
        foreach ($this->players as $player) {
            foreach ($player->pieces as $piece) {
                $piece->row = 9;
                $piece->column = 9;
                $piece->save();
            }
        }

        $attackingPiece = $this->players[0]->pieces->where("type", "Miner")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Marshall")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => null,
            "column" => null
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => 0,
            "column" => 1
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner->token === $defendingPiece->token;
        });
    }

    /**
     * PieceController:handleCombat test (draw)
     *
     * @return void
     */
    public function testHandleCombat_draw()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        // Place all of the pieces, to ensure the game doesn't end
        foreach ($this->players as $player) {
            foreach ($player->pieces as $piece) {
                $piece->row = 9;
                $piece->column = 9;
                $piece->save();
            }
        }

        $attackingPiece = $this->players[0]->pieces->where("type", "Marshall")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Marshall")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => null,
            "column" => null
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => null,
            "column" => null
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner === null;
        });
    }

    /**
     * PieceController:handleCombat test (flag captured)
     *
     * @return void
     */
    public function testHandleCombat_flagCaptured()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        // Place all of the pieces, to ensure the game doesn't end due to lack of movable pieces
        foreach ($this->players as $player) {
            foreach ($player->pieces as $piece) {
                $piece->row = 9;
                $piece->column = 9;
                $piece->save();
            }
        }

        $attackingPiece = $this->players[0]->pieces->where("type", "Marshall")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Flag")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => 0,
            "column" => 1
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => null,
            "column" => null
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner->token === $attackingPiece->token;
        });

        // Check that endGame has been called, and a GameState event has been broadcast. The DB updates are tested in the endGame tests.
        Event::assertDispatched(GameState::class, function($e) use ($game, $attackingPiece) {
            return $e->game->token === $game->token &&
                   $e->game->state === "End" &&
                   $e->game->winner->token === $attackingPiece->player->token;
        });
    }

    /**
     * PieceController:handleCombat test (attacking movable pieces captured)
     *
     * @return void
     */
    public function testHandleCombat_attackingMovablePieces()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $attackingPiece = $this->players[0]->pieces->where("type", "Scout")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Marshall")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => null,
            "column" => null
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => 0,
            "column" => 1
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner->token === $defendingPiece->token;
        });

        // Check that endGame has been called, and a GameState event has been broadcast. The DB updates are tested in the endGame tests.
        Event::assertDispatched(GameState::class, function($e) use ($game, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->game->state === "End" &&
                   $e->game->winner->token === $defendingPiece->player->token;
        });
    }

    /**
     * PieceController:handleCombat test (defending movable pieces captured)
     *
     * @return void
     */
    public function testHandleCombat_defendingMovablePieces()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $attackingPiece = $this->players[0]->pieces->where("type", "Marshall")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Scout")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => 0,
            "column" => 1
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => null,
            "column" => null
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner->token === $attackingPiece->token;
        });

        // Check that endGame has been called, and a GameState event has been broadcast. The DB updates are tested in the endGame tests.
        Event::assertDispatched(GameState::class, function($e) use ($game, $attackingPiece) {
            return $e->game->token === $game->token &&
                   $e->game->state === "End" &&
                   $e->game->winner->token === $attackingPiece->player->token;
        });
    }

    /**
     * PieceController:handleCombat test (all movable pieces captured)
     *
     * @return void
     */
    public function testHandleCombat_drawMovablePieces()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $attackingPiece = $this->players[0]->pieces->where("type", "Marshall")->first();
        $defendingPiece = $this->players[1]->pieces->where("type", "Marshall")->first();

        $attackingPiece->row = 0;
        $attackingPiece->column = 0;
        $attackingPiece->save();

        $defendingPiece->row = 0;
        $defendingPiece->column = 1;
        $defendingPiece->save();

        Event::fake();
        $pieceController->handleCombat($game, $attackingPiece, $defendingPiece, [0,1]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[0]->id,
            "token" => $attackingPiece->token,
            "row" => null,
            "column" => null
        ]);

        $this->assertDatabaseHas("pieces", [
            "player_id" => $this->players[1]->id,
            "token" => $defendingPiece->token,
            "row" => null,
            "column" => null
        ]);

        Event::assertDispatched(Combat::class, function($e) use ($game, $attackingPiece, $defendingPiece) {
            return $e->game->token === $game->token &&
                   $e->attackingPiece->token === $attackingPiece->token &&
                   $e->defendingPiece->token === $defendingPiece->token &&
                   $e->winner === null;
        });

        // Check that endGame has been called, and a GameState event has been broadcast. The DB updates are tested in the endGame tests.
        Event::assertDispatched(GameState::class, function($e) use ($game) {
            return $e->game->token === $game->token &&
                   $e->game->state === "End" &&
                   $e->game->winner === null;
        });
    }

    /**
     * PieceController:processPiecePosition test
     *
     * @return void
     */
    public function testIsPlacementSpace()
    {
        $this->setupGame();
        $pieceController = new PieceController();

        foreach (PieceController::PLACEMENT_RANGE as $index => $range) {
            $player = $this->players[$index];
            $belowRangeSpace = [$range[0][0]-1, $range[0][1]-1];
            $bottomRangeSpace = $range[0];
            $midRangeSpace = [$range[0][0]+1, $range[0][1]+1];
            $topRangeSpace = $range[1];
            $aboveRangeSpace = [$range[1][0]+1, $range[1][1]+1];

            $this->assertFalse($pieceController->isPlacementSpace($player, $belowRangeSpace));
            $this->assertTrue($pieceController->isPlacementSpace($player, $bottomRangeSpace));
            $this->assertTrue($pieceController->isPlacementSpace($player, $midRangeSpace));
            $this->assertTrue($pieceController->isPlacementSpace($player, $topRangeSpace));
            $this->assertFalse($pieceController->isPlacementSpace($player, $aboveRangeSpace));
        }
    }

    /**
     * PieceController:processPiecePosition test
     *
     * @return void
     */
    public function testIsDisabledSpace()
    {
        $pieceController = new PieceController();
        $disabledSpaces = PieceController::DISABLED_SPACES;
        $lastDisabledSpace = $disabledSpaces[count($disabledSpaces) - 1];

        $this->assertFalse($pieceController->isDisabledSpace([-1, -1]));
        $this->assertTrue($pieceController->isDisabledSpace($disabledSpaces[0]));
        $this->assertTrue($pieceController->isDisabledSpace($lastDisabledSpace));
    }

    /**
     * PieceController::isValidMove test (default rules)
     *
     * @return void
     */
    public function testIsValidMove_default()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $ranks = ["Marshall", "General", "Colonel", "Major", "Captain", "Lieutenant", "Sergeant", "Miner", "Spy"];

        foreach ($ranks as $rank) {
            $piece = $this->players[0]->pieces()->where("type", $rank)->first();
            $piece->row = 2;
            $piece->column = 4;
            $piece->save();

            // Can move by one space
            $this->assertTrue($pieceController->isValidMove($game, $piece, [2,4]));
            $this->assertTrue($pieceController->isValidMove($game, $piece, [1,4]));
            $this->assertTrue($pieceController->isValidMove($game, $piece, [2,3]));
            $this->assertTrue($pieceController->isValidMove($game, $piece, [3,4]));
            $this->assertTrue($pieceController->isValidMove($game, $piece, [2,5]));

            // Cannot move to far away spaces or diagonally
            $this->assertFalse($pieceController->isValidMove($game, $piece, [0,0]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [1,3]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [1,5]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [3,3]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [3,5]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [9,9]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,-1]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [10,10]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [0,4]));

            // Cannot move outside of the board boundaries
            $piece->row = 0;
            $piece->column = 0;
            $piece->save();

            $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,0]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [0,-1]));

            $piece->row = 9;
            $piece->column = 9;
            $piece->save();

            $this->assertFalse($pieceController->isValidMove($game, $piece, [10,9]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [9,10]));

            // Cannot move onto a disabled space
            $piece->row = 5;
            $piece->column = 5;
            $piece->save();

            $this->assertFalse($pieceController->isValidMove($game, $piece, [5,6]));
        }
    }

    /**
     * PieceController::isValidMove test (scout)
     *
     * @return void
     */
    public function testIsValidMove_scout()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $piece = $this->players[0]->pieces()->where("type", "Scout")->first();
        $piece->row = 2;
        $piece->column = 4;
        $piece->save();

        // Can move by one space
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [1,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,3]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [3,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,5]));

        // Cannot move to far away spaces or diagonally
        $this->assertFalse($pieceController->isValidMove($game, $piece, [0,0]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [1,3]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [1,5]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [3,3]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [3,5]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [9,9]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,-1]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [10,10]));

        // Can move by multiple spaces in the same direction
        $this->assertTrue($pieceController->isValidMove($game, $piece, [0,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [9,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,0]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,9]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,4]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [10,4]));

        // Cannot move multiple spaces if a piece blocks the path
        $bombs = $this->players[0]->pieces()->where("type", "Bomb")->take(2)->get();
        foreach ($bombs as $bombIndex => $bomb) {
            if ($bombIndex === 0) {
                $bomb->row = 2;
                $bomb->column = 8;
            } else {
                $bomb->row = 7;
                $bomb->column = 4;
            }

            $bomb->save();
        }

        $this->assertFalse($pieceController->isValidMove($game, $piece, [2,9]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,8]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,7]));

        $this->assertFalse($pieceController->isValidMove($game, $piece, [9,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [7,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [6,4]));

        $this->assertTrue($pieceController->isValidMove($game, $piece, [2,0]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [0,4]));

        // Cannot move outside of the board boundaries
        $piece->row = 0;
        $piece->column = 0;
        $piece->save();

        $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,0]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [0,-1]));

        $piece->row = 9;
        $piece->column = 9;
        $piece->save();

        $this->assertFalse($pieceController->isValidMove($game, $piece, [10,9]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [9,10]));

        // Cannot move multiple spaces if a disabled piece blocks the path
        $piece->row = 5;
        $piece->column = 5;
        $piece->save();

        $this->assertTrue($pieceController->isValidMove($game, $piece, [4,5]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [5,4]));
        $this->assertTrue($pieceController->isValidMove($game, $piece, [6,5]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [5,6]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [5,9]));
        $this->assertFalse($pieceController->isValidMove($game, $piece, [5,0]));
    }

    /**
     * PieceController::isValidMove test (non-movable, flag and bomb)
     *
     * @return void
     */
    public function testIsValidMove_nonMovable()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $pieces = [];
        $pieces[0] = $this->players[0]->pieces()->where("type", "Flag")->first();
        $pieces[0]->row = 5;
        $pieces[0]->column = 5;
        $pieces[0]->save();

        $pieces[1] = $this->players[0]->pieces()->where("type", "Bomb")->first();
        $pieces[1]->row = 5;
        $pieces[1]->column = 5;
        $pieces[1]->save();

        foreach ($pieces as $piece) {
            $this->assertFalse($pieceController->isValidMove($game, $piece, [5,5]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [4,5]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [5,4]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [6,5]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [5,6]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [0,0]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [9,9]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [0,5]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,-1]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [10,10]));

            // Cannot move outside of the board boundaries
            $piece->row = 0;
            $piece->column = 0;
            $piece->save();

            $this->assertFalse($pieceController->isValidMove($game, $piece, [-1,0]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [0,-1]));

            $piece->row = 9;
            $piece->column = 9;
            $piece->save();

            $this->assertFalse($pieceController->isValidMove($game, $piece, [10,9]));
            $this->assertFalse($pieceController->isValidMove($game, $piece, [9,10]));
        }
    }

    /**
     * PieceController:getOccupyingPiece test
     *
     * @return void
     */
    public function testGetOccupyingPiece()
    {
        $this->setupGame();
        $game = $this->game;
        $pieceController = new PieceController();

        $piece = $this->players[0]->pieces->first();
        $piece->row = 5;
        $piece->column = 5;
        $piece->save();

        $this->assertFalse($pieceController->getOccupyingPiece($game, [4,4]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [4,5]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [4,6]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [5,4]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [5,6]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [6,4]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [6,5]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [6,6]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [0,0]));
        $this->assertFalse($pieceController->getOccupyingPiece($game, [9,9]));

        $this->assertEquals($piece, $pieceController->getOccupyingPiece($game, [5,5]));
    }

    /**
     * PieceController:calculateCoordsBetween test
     *
     * @return void
     */
    public function testCalculateCoordsBetween()
    {
        $pieceController = new PieceController();

        $this->assertEquals([], $pieceController->calculateCoordsBetween([5,5], [5,5]));

        $this->assertEquals([[5,5]], $pieceController->calculateCoordsBetween([5,4], [5,6]));
        $this->assertEquals([[8,6], [8,5], [8,4], [8,3]], $pieceController->calculateCoordsBetween([8,7], [8,2]));

        $this->assertEquals([[4,5], [5,5], [6,5]], $pieceController->calculateCoordsBetween([3,5], [7,5]));
        $this->assertEquals([[8,0], [7,0], [6,0]], $pieceController->calculateCoordsBetween([9,0], [5,0]));

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Unable to calculate coordinates");
        $pieceController->calculateCoordsBetween([0,0], [9,9]);
    }
}
