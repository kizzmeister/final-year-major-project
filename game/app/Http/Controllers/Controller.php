<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

use App\Events\GameState;
use App\Game;
use App\Player;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * Gets the current player for a game
    *
    * @param Game $game the game instance
    *
    * @return Player|bool the current players instance, or false
    */
    public function getPlayerFromGame(Game $game = null)
    {
        if ($game && Auth::check()) {
            $player = $game->players()->where("user_id", Auth::user()->id)->first();

            if ($player) {
                return $player;
            }
        }

        return false;
    }

    /**
    * Gets a random player from a game
    *
    * @param Game $game the game instance
    *
    * @return Player the randomly chosen player instance
    */
    public function getRandomPlayer(Game $game)
    {
        return $game->players()->get()->random();
    }

    /**
    * Gets the next player for a game (for two players, the opposing player)
    *
    * @param Game $game the game instance
    * @param Player $player the current player
    *
    * @return Player the instance of the next player for the game
    */
    public function getNextPlayer(Game $game, Player $player)
    {
        $game = Game::where("id", $game->id)->with("players")->first();
        $numPlayers = $game->players->count();

        if ($player->num === $numPlayers) {
            return $game->players->where("num", 1)->first();
        } else {
            return $game->players->where("num", $player->num + 1)->first();
        }
    }

    /**
    * Checks whether all pieces have been placed for a particular game
    *
    * @param Game $game the game in question
    *
    * @return boolean true if all pieces have been placed by all players, otherwise false
    */
    public function gameHasUnplacedPieces(Game $game)
    {
        // Ensure we actually query the database using players() rather than players
        // https://github.com/laravel/framework/issues/12180
        foreach ($game->players()->get() as $player) {
            if ($this->playerHasUnplacedPieces($player)) {
                return true;
            }
        }

        return false;
    }

    /**
    * Checks whether all of a players pieces have been placed on the board
    *
    * @param Player $player the player in question
    *
    * @return boolean true if all pieces have been placed, otherwise false
    */
    public function playerHasUnplacedPieces(Player $player)
    {
        // Ensure we actually query the database using pieces() rather than pieces
        // https://github.com/laravel/framework/issues/12180
        foreach ($player->pieces()->get() as $piece) {
            if ($piece->row === null || $piece->column === null) {
                return true;
            }
        }

        return false;
    }

    /**
    * Checks if a particular player has any remaining movable pieces
    *
    * @param Player $player the player
    *
    * @return boolean true if the player has some remaining movable pieces, otherwise false
    */
    public function playerHasMovablePieces(Player $player)
    {
        $unMovablePieces = ["Flag", "Bomb"];

        // Ensure we actually query the database using pieces() rather than pieces
        // https://github.com/laravel/framework/issues/12180
        foreach ($player->pieces()->get() as $piece) {
            if ($piece->row === null || $piece->column === null) continue;

            if (!in_array($piece->type, $unMovablePieces)) {
                return true;
            }
        }

        return false;
    }

    /**
    * Ends a particular game, broadcasting relevant events to the clients
    *
    * @param Game   $game   the game
    * @param Player $winner the winner
    *
    * @return void
    */
    public function endGame(Game $game, Player $winner = null)
    {
        if ($winner) {
            $game->winner()->associate($winner);
        }
        $game->state = "End";
        $game->save();

        broadcast(new GameState($game));
    }

    /**
    * Get the position for a piece, ready for sending back to the client or storing in the DB
    *
    * Although each player places their pieces on the bottom of the board, we need to store
    * both sets on one board. So for simplicity, player 2's pieces will be vertically and horizontally flipped.
    *
    * So, if both players had a piece on the client-side at [9,0], player 2's piece position
    * would be stored as [0,9].
    *
    * When sending back to the client, we can send the positions for player 1 back as-is.
    * But the entire board will need to be flipped for player 2.
    *
    * @param Player  $player the pieces player if storing in the DB, otherwise the current player
    * @param array   $position the pieces position
    *
    * @return array the pieces client position
    */
    public function getPiecePosition($player, $position)
    {
        if ($player->num === 2 && $position[0] !== null && $position[1] !== null) {
            $position[0] = abs($position[0] - (PieceController::BOARD_SIZE - 1));
            $position[1] = abs($position[1] - (PieceController::BOARD_SIZE - 1));
        }

        return $position;
    }
}
