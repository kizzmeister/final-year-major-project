<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Game;
use App\Player;

class GameController extends Controller
{
    const PLAYER_LIMIT = 2;

    /**
    * Apply the authentication middleware to all controller actions
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
    * Create a Game instance, returning the join token
    *
    * @return void
    */
    public function hostGame(Request $request)
    {
        $game = new Game;
        $game->generateToken();
        $game->save();

        // Create as many players as we need
        for ($num = 0; $num < GameController::PLAYER_LIMIT; $num++) {
            $player = new Player;
            $player->num = $num + 1;
            $player->game()->associate($game);
            $player->save();
            $player->setupPieces();
        }

        return $this->joinGame($request, $game->token);
    }

    /**
    * Create a Player instance, adding them to the provided Game
    *
    * @return redirect to game page, or back to index with error
    */
    public function joinGame(Request $request, string $token = "")
    {
        $token = empty($token) ? $request->input("token") : $token;
        $game = Game::where("token", $token)->with("players")->first();
        $existingPlayer = $this->getPlayerFromGame($game);

        if ($game) {
            if (!$existingPlayer) {
                $newPlayer = $game->players()->whereNull("user_id")->orderBy("id")->first();

                if ($newPlayer) {
                    $newPlayer->user()->associate(Auth::user());
                    $newPlayer->save();
                } else {
                    return redirect("/")->with("error", "Unable to join Game - Already at maximum number of players");
                }
            }

            return redirect()->route("playGame", [$game]);
        }

        return redirect("/")->with("error", "Unable to join Game - Invalid Token");
    }

    /**
     * Play the game, showing the game view (if the token is valid)
     *
     * @return Game view, or redirect to index with error
     */
    public function playGame(Request $request, string $token = "")
    {
        if ($token === "") {
            return view("game", [
                "gameToken" => $token,
                "aiType" => $request->input("ai", "random")
            ]);
        }

        $game = Game::where("token", $token)->with("players")->first();
        $player = $this->getPlayerFromGame($game);

        if ($game && $player) {
            $state = $game->state;
            if ($state === "Placement" && !$this->playerHasUnplacedPieces($player)) {
                $state = "Ready";
            }

            $winner = null;
            if ($game->winner) {
                $winner = $game->winner->num;
            }

            return view("game", [
                "gameToken" => $token,
                "gameState" => $state,
                "playerNum" => $player->num,
                "winner" => $winner,
                "playerPieces" => $this->getPlayerPieces($game, $player),
            ]);
        }

        return redirect("/")->with("error", "Unable to join Game - Invalid Token");
    }

    /**
     * Get an array of players, with their associated pieces (and positions) for a game
     *
     * @param Game      $game             the game to lookup
     * @param Player    $currentPlayer    the current player
     * @param bool      $shuffle          enable shuffling of pieces, to further conceal their identities
     *
     * @return array of each players piece positions (with identities for the current player only)
     */
    public function getPlayerPieces(Game $game, Player $currentPlayer, bool $shuffle = true)
    {
        $players = [];

        foreach($game->players()->orderBy("id")->get() as $playerIndex => $player) {
            $players[$playerIndex] = [
                "player" => $player->num,
                "turn" => $game->turn && $game->turn->id === $player->id,
                "pieces" => []
            ];

            foreach($player->pieces()->orderBy("id")->get() as $piece) {
                $position = $this->getPiecePosition($currentPlayer, [$piece->row, $piece->column]);
                $sidebarStates = ["Play", "End"];

                // Reveal the type if owned by the current player, or if it's in the sidebar when we're in play
                if (
                    $player->id === $currentPlayer->id ||
                    (in_array($game->state, $sidebarStates) && $position[0] === null && $position[1] === null)
                ) {
                    $type = $piece->type;
                } else {
                    $type = "Unknown";
                }

                $players[$playerIndex]["pieces"][] = [
                    "token" => $piece->token,
                    "type" => $type,
                    "row" => $position[0],
                    "col" => $position[1]
                ];

                if ($player->id !== $currentPlayer->id && $shuffle) {
                    shuffle($players[$playerIndex]["pieces"]);
                }
            }
        }

        return $players;
    }
}
