<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Piece;
use App\Player;
use App\Game;

use App\Events\PiecePlacement;
use App\Events\Turn;
use App\Events\GameState;
use App\Events\Move;
use App\Events\Combat;

class PieceController extends Controller
{
    /**
    * Board configuration, matching that defined in board.ts
    */
    const BOARD_SIZE = 10;
    const PLACEMENT_RANGE = [
        [[6,0], [9,9]], //Player 1
        [[0,0], [3,9]]  //Player 2
    ];
    const DISABLED_SPACES = [
        [4,2],[4,3],[4,6],[4,7],
        [5,2],[5,3],[5,6],[5,7]
    ];

    /**
    * Apply the authentication middleware to all controller actions
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
    * Store the players initial piece placement in the DB, broadcasting this placement to the other clients
    *
    * @param Illuminate\Http\Request $request the HTTP request object
    *
    * @return JSON response indicating success or error
    */
    public function placePieces(Request $request)
    {
        $gameToken = $request->input("gameToken");
        $pieces = $request->input("pieces");

        $game = Game::where("token", $gameToken)->where("state", "Placement")->with("players")->first();
        $player = $this->getPlayerFromGame($game);

        if ($player && is_array($pieces) && count($pieces) == $player->pieces()->count()) {
            foreach ($pieces as $piece) {
                // Find the piece in the database, and validate it's position
                $storedPiece = $player->pieces()->where("token", $piece['token'])->first();

                if ($storedPiece && is_numeric($piece['row']) && is_numeric($piece['col'])) {
                    // Process the new position and validate that it is within a placement zone
                    $position = $this->getPiecePosition($player, [$piece['row'], $piece['col']]);

                    if ($this->isPlacementSpace($player, $position) && !$this->isDisabledSpace($position)) {
                        $storedPiece->row = $position[0];
                        $storedPiece->column = $position[1];
                        $storedPiece->save();
                        continue;
                    }
                }

                return response()->json(["error" => "Unable to setup piece placement"], 500);
            }

            // Send any necessary events to the game clients
            broadcast(new PiecePlacement($game, $player))->toOthers();
            if (!$this->gameHasUnplacedPieces($game)) {
                $game->turn()->associate($this->getRandomPlayer($game));
                $game->state = "Play";
                $game->save();

                broadcast(new Turn($game));
                broadcast(new GameState($game));
            }

            return response()->json(["success" => "Success"], 200);
        }

        return response()->json(["error" => "Unable to setup piece placement"], 500);
    }

    /**
    * Move a players piece position in the DB, broadcasting this movement to the other clients
    *
    * @param Illuminate\Http\Request $request the HTTP request object
    *
    * @return JSON response indicating success or error
    */
    public function movePiece(Request $request)
    {
        $gameToken = $request->input("gameToken");
        $piece = $request->input("piece");

        $game = Game::where("token", $gameToken)->where("state", "Play")->with("players")->first();
        $player = $this->getPlayerFromGame($game);

        if ($player && $game->turn->num === $player->num) {
            // Find the piece in the database, and validate it's position
            $storedPiece = $player->pieces()->where("token", $piece['token'])->first();
            if ($storedPiece && is_numeric($piece['row']) && is_numeric($piece['col'])) {
                // Process the new position and check that it is a valid move
                $position = $this->getPiecePosition($player, [$piece['row'], $piece['col']]);

                if ($this->isValidMove($game, $storedPiece, $position)) {
                    $existingPiece = $this->getOccupyingPiece($game, $position);

                    if ($existingPiece !== false) {
                        $this->handleCombat($game, $storedPiece, $existingPiece, $position);
                    } else {
                        $storedPiece->row = $position[0];
                        $storedPiece->column = $position[1];
                        $storedPiece->save();

                        // Send the movement event to other game clients
                        broadcast(new Move($game, $storedPiece))->toOthers();
                    }

                    // Update the game turn to be the next player
                    $game->turn()->associate($this->getNextPlayer($game, $player));
                    $game->save();
                    broadcast(new Turn($game));

                    return response()->json(["success" => "Success"], 200);
                }
            }
        }

        return response()->json(["error" => "Unable to move piece"], 500);
    }

    /**
    * Handles the combat stages of piece movement
    *
    * @param Game $game the game instance
    * @param Piece $attackingPiece the instance of the attacking piece
    * @param Piece $defendingPiece the instance of the defending piece
    * @param array $position the winners new position
    *
    * @return void
    */
    public function handleCombat(Game $game, Piece $attackingPiece, Piece $defendingPiece, $position)
    {
        $attackingPiece->row = null;
        $attackingPiece->column = null;
        $defendingPiece->row = null;
        $defendingPiece->column = null;

        // If both types are the same, it's a draw
        if ($attackingPiece->type === $defendingPiece->type) {
            $winner = null;
        } elseif ($attackingPiece->canDefeat($defendingPiece)) {
            $attackingPiece->row = $position[0];
            $attackingPiece->column = $position[1];

            $winner = $attackingPiece;
        } else {
            $defendingPiece->row = $position[0];
            $defendingPiece->column = $position[1];

            $winner = $defendingPiece;
        }

        // Send the combat event to all game clients
        $attackingPiece->save();
        $defendingPiece->save();
        broadcast(new Combat($game, $attackingPiece, $defendingPiece, $winner));

        $attacker = $attackingPiece->player;
        $defender = $defendingPiece->player;

        // If at captured piece was a flag, or one of the players no longer has any movable pieces, end the game
        if ($defendingPiece->type === "Flag") {
            $this->endGame($game, $attacker);
        } elseif (!$this->playerHasMovablePieces($attacker) && !$this->playerHasMovablePieces($defender)) {
            $this->endGame($game, null);
        } elseif (!$this->playerHasMovablePieces($attacker)) {
            $this->endGame($game, $defender);
        } elseif (!$this->playerHasMovablePieces($defender)) {
            $this->endGame($game, $attacker);
        }
    }

    /**
    * Checks if a particular space is within a players allowed placement range
    *
    * @param Player  $player the player
    * @param array   $position the space position
    *
    * @return boolean true if within placement range, otherwise false
    */
    public function isPlacementSpace(Player $player, $position)
    {
        return (
            $position[0] >= PieceController::PLACEMENT_RANGE[$player->num-1][0][0] &&
            $position[1] >= PieceController::PLACEMENT_RANGE[$player->num-1][0][1] &&
            $position[0] <= PieceController::PLACEMENT_RANGE[$player->num-1][1][0] &&
            $position[1] <= PieceController::PLACEMENT_RANGE[$player->num-1][1][1]
        );
    }

    /**
    * Checks if a particular space is disabled
    *
    * @param array $position the space position
    *
    * @return boolean true if disabled, otherwise false
    */
    public function isDisabledSpace($position)
    {
        return in_array($position, PieceController::DISABLED_SPACES);
    }

    /**
    * Checks if a piece can be moved to a certain position
    *
    * @param Game $game the game to check
    * @param Piece $piece the piece to move
    * @param array $target the position to move to
    *
    * @return boolean true if valid, otherwise false
    */
    public function isValidMove(Game $game, Piece $piece, $target)
    {
        // Check for disabled spaces and unmovable pieces
        if ($this->isDisabledSpace($target) || $piece->type === "Flag" || $piece->type === "Bomb") {
            return false;
        }

        // Also check to make sure the target is within the board boundaries
        $maxCoord = PieceController::BOARD_SIZE - 1;
        if ($target[0] < 0 || $target[1] < 0 || $target[0] > $maxCoord || $target[1] > $maxCoord) {
            return false;
        }

        $origin = [$piece->row, $piece->column];

        // If the two rows are the same, check that the column is between origin-1 and origin+1
        if ($origin[0] === $target[0] && ($target[1] >= ($origin[1]-1) && $target[1] <= ($origin[1]+1))) {
            return true;
        }

        // If the two columns are the same, check that the row is between origin-1 and origin+1
        if ($origin[1] === $target[1] && ($target[0] >= ($origin[0]-1) && $target[0] <= ($origin[0]+1))) {
            return true;
        }

        // If the piece is a scout, check the coordinates inbetween the origin and the target
        if (
            $piece->type === "Scout" &&
            (($origin[0] === $target[0] && $origin[1] !== $target[1]) ||
            ($origin[1] === $target[1] && $origin[0] !== $target[0]))
        ) {
            $possibleCoords = $this->calculateCoordsBetween($origin, $target);

            foreach ($possibleCoords as $coords) {
                if ($this->getOccupyingPiece($game, $coords) !== false || $this->isDisabledSpace($coords)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
    * Checks if a particular space is occupied by a piece
    *
    * @param array $position the position to check
    *
    * @return Piece|boolean, the piece in the position, otherwise false
    */
    public function getOccupyingPiece(Game $game, $position)
    {
        foreach ($game->players()->with("pieces")->get() as $player) {
            foreach ($player->pieces as $piece) {
                if ($piece->row === $position[0] && $piece->column === $position[1]) {
                    return $piece;
                }
            }
        }

        return false;
    }

    /**
    * Calculates the possible coordinates between two positions (in one direction only)
    *
    * @param array $origin the origin position
    * @param array $target the target position
    *
    * @return array the possible coordinates
    */
    public function calculateCoordsBetween($origin, $target) {
        $coords = [];

        // Throw an error if both the rows and columns are different
        if ($origin[0] !== $target[0] && $origin[1] !== $target[1]) {
            throw new \Exception("Unable to calculate coordinates");
        }

        if ($origin[0] !== $target[0]) { // If the row is changing
            $increment = ($origin[0] < $target[0]) ? 1 : -1;
            for ($row = ($origin[0] + $increment); $row !== $target[0]; $row += $increment) {
                $coords[] = [$row, $origin[1]];
            }
        } else if ($origin[1] !== $target[1]) { // If the column is changing
            $increment = ($origin[1] < $target[1]) ? 1 : -1;
            for ($col = ($origin[1] + $increment); $col !== $target[1]; $col += $increment) {
                $coords[] = [$origin[0], $col];
            }
        }

        return $coords;
    }
}
