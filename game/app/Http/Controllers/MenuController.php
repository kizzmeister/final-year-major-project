<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Show the game menu
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu');
    }
}
