<?php

namespace App\Events;

use App\Game;
use App\Piece;

class Combat extends GameEvent
{
    public $attackingPiece;
    public $defendingPiece;
    public $winner;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Game $game, Piece $attackingPiece, Piece $defendingPiece, Piece $winner = null)
    {
        parent::__construct($game);

        $this->attackingPiece = $attackingPiece;
        $this->defendingPiece = $defendingPiece;
        $this->winner = $winner;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $winnerToken = null;
        if ($this->winner) {
            $winnerToken = $this->winner->token;
        }

        return [
            "attackingPiece" => array_merge(
                [
                    "player" => $this->attackingPiece->player->num,
                    "type" => $this->attackingPiece->type,
                    "winner" => $this->attackingPiece->token === $winnerToken
                ],
                $this->attackingPiece->toArray()
            ),
            "defendingPiece" => array_merge(
                [
                    "player" => $this->defendingPiece->player->num,
                    "type" => $this->defendingPiece->type,
                    "winner" => $this->defendingPiece->token === $winnerToken
                ],
                $this->defendingPiece->toArray()
            )
        ];
    }
}
