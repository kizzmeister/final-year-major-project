<?php

namespace App\Events;

use App\Game;
use App\Player;

class PiecePlacement extends GameEvent
{
    public $player;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Game $game, Player $player)
    {
        parent::__construct($game);
        
        $this->player = $player;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $pieces = $this->player->pieces->toArray();
        shuffle($pieces);

        return [
            "player" => $this->player->num,
            "pieces" => $pieces
        ];
    }
}
