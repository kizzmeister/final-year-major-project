<?php

namespace App\Events;

use App\Game;

class GameState extends GameEvent
{
    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $data = [
            "gameState" => $this->game->state
        ];

        // If the game has ended, also send the winner
        if ($this->game->state === "End") {
            $data['winner'] = ($this->game->winner) ? $this->game->winner->num : null;
        }

        return $data;
    }
}
