<?php

namespace App\Events;

use App\Game;
use App\Piece;

class Move extends GameEvent
{
    public $piece;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Game $game, Piece $piece)
    {
        parent::__construct($game);
        
        $this->piece = $piece;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            "player" => $this->piece->player->num,
            "piece" => $this->piece->toArray()
        ];
    }
}
