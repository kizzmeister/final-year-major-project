<?php

namespace App\Events;

class Turn extends GameEvent
{
    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            "turn" => $this->game->turn->num
        ];
    }
}
