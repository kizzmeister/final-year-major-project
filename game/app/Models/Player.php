<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Piece;

class Player extends Model
{
    const PIECE_CONFIG = [
        ["Marshall", 1],
        ["General", 1],
        ["Colonel", 2],
        ["Major", 3],
        ["Captain", 4],
        ["Lieutenant", 4],
        ["Sergeant", 4],
        ["Miner", 5],
        ["Scout", 8],
        ["Spy", 1],
        ["Bomb", 6],
        ["Flag", 1]
    ];

    protected $visible = ["num"];

    /**
    * Initialize new players with a defined set of pieces
    *
    * @return void
    */
    public function setupPieces()
    {
        foreach (Player::PIECE_CONFIG as $config) {
            for ($num = 0; $num < $config[1]; $num++) {
                $piece = new Piece;
                $piece->type = $config[0];
                $piece->player()->associate($this);
                $piece->generateToken();
                $piece->save();
            }
        }
    }

    public function game()
    {
        return $this->belongsTo("App\Game");
    }

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function pieces()
    {
        return $this->hasMany("App\Piece");
    }
}
