<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $visible = ["token"];

     /**
     * Randomly generate a token for the game
     *
     * @return void
     */
    public function generateToken()
    {
        do {
            // http://php.net/manual/en/function.random-bytes.php
            $this->token = bin2hex(random_bytes(16));
        } while(Game::where("token", $this->token)->count() !== 0);
    }

     /**
     * Get the value of the model's route key.
     * https://laravel.com/docs/5.4/responses#redirecting-named-routes
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->token;
    }

    public function players()
    {
        return $this->hasMany("App\Player");
    }

    public function turn()
    {
        return $this->belongsTo("App\Player");
    }

    public function winner()
    {
        return $this->belongsTo("App\Player");
    }
}
