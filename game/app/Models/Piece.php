<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    const RANKS = [
        "Bomb",
        "Marshall",
        "General",
        "Colonel",
        "Major",
        "Captain",
        "Lieutenant",
        "Sergeant",
        "Miner",
        "Scout",
        "Spy",
        "Flag"
    ];

    protected $visible = ["token", "row", "column"];

    /**
    * Randomly generate a token for the piece
    *
    * @return void
    */
    public function generateToken()
    {
        // For efficiency reasons I am not checking for collisions here.
        // Given the piece tokens only have to be unique for each player, I don't think this is an issue.
        // http://php.net/manual/en/function.random-bytes.php
        $this->token = bin2hex(random_bytes(16));
    }

    /**
    * Determines whether a piece can defeat another
    *
    * @param Piece $opposingPiece the instance of the opposing piece
    *
    * @return bool true if this piece can defeat the other, otherwise false
    */
    public function canDefeat(Piece $opposingPiece)
    {
        if ($this->type === "Miner" && $opposingPiece->type === "Bomb") {
            return true;
        }

        if ($this->type === "Spy" && $opposingPiece->type === "Marshall") {
            return true;
        }

        $rankNum = array_search($this->type, Piece::RANKS);
        $opposingRankNum = array_search($opposingPiece->type, Piece::RANKS);
        if ($rankNum <= $opposingRankNum) {
            return true;
        }

        return false;
    }

    public function player()
    {
        return $this->belongsTo("App\Player");
    }
}
