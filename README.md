# HTML5 Stratego - Final Year Major Project

Recreation of the Stratego board game in HTML5, PHP and TypeScript.
Includes basic AI for singleplayer and web-socket support for multiplayer.

Screenshot:

![screenshot](screenshot.png)