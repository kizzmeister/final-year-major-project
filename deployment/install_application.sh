#!/bin/bash
mysql -e 'CREATE DATABASE IF NOT EXISTS stratego;'
cd /var/www/html

# Setup the DB and Laravel Environment File
mv .env.production .env
php artisan migrate --force
php artisan key:generate --force

# Setup the Laravel Echo Server Configuration
mv laravel-echo-server.json.production laravel-echo-server.json
