Kieran Dunbar - Technical submission for the MMP project, A Web-Based Version of the Board Game Stratego.
=========================================================================================================

This compressed file contains the technical work for my MMP project, written using TypeScript and PHP (using the Laravel PHP Framework).
This README will summarize the contents of this file, providing details on where you can find different parts of the code.
Instructions will also be provided on how to install and run the game yourself.

For the avoidance of doubt, this file contains a built version of the game, along with most of the required dependencies preinstalled. The only exceptions to this are the dependency managers themselves (Node.js and Composer) and the server applications such as nginx and Laravel Echo Server. Instructions for installing these, and rebuilding the project from source if required, are found below.

====================================
Structure and Contents
====================================

In the parent directory, you will find this README, along with two configuration files. '.travis.yml' and 'appspec.yml' are configuration files for Travis CI and AWS CodeDeploy respectively. They are not required to run the project on a local environment, but are left here for reference. Similarly, the 'deployment' directory only contains deployment scripts used by AWS CodeDeploy.

The main part of the game code is contained within the 'game' directory. Within this, are the various files and directories for both the server and client-side of the game;
* The 'app' directory contains various PHP classes for the server-side, such as controllers, models, service providers and events.
* The 'config' directory contains various PHP classes to handle the configuration for Laravel. The configuration that needs to be changed, can be overridden in a '.env' file, within the 'game' directory.
* The 'database' directory contains the database migrations, which are used to create, incrementally, the backend database for the server-side. This directory also contains the User model factory, used within some automated tests.
* The 'public' directory is what the HTTP server should set as the web root. This contains the built versions of the TypeScript client code (including tests), and various CSS and JavaScript files.
* The 'resources' directory contains various files relating to both the server and client-side of the game. Under 'assets', you can find source files for the TypeScript client (including tests), Less and the external libraries (such as jQuery). While 'lang' and 'views' contain internationalization files and view templates for the Laravel server-side.
* The 'routes' directory contains configuration for the various API endpoints available on the server-side. 'channels.php' also contains the authentication configuration for the game web socket channels.
* The 'tests' directory contains the PHP unit and feature tests for the server-side of the game.

The 'node_modules' and 'vendor' directories contain the dependencies installed by Node.js and Composer. These, and the 'composer.lock' file can be safely deleted if you want to rebuild the game from source (following the instructions below).

Other important files within the 'game' directory include;
* '.env.example' which is the example configuration file that should be edited when installing the game. There are also production and travis configuration files provided for reference, these are only used for deployment.
* 'composer.json' which contains the PHP dependencies for the server-side of the game. These are installable through Composer.
* 'gulpfile.js' which contains the configuration for the gulp build system. This handles the build steps for the client-side of the game, including TypeScript.
* 'laravel-echo-server.json' which contains the configuration for the Laravel Echo Server.
* 'package.json' which contains the dependencies for the client-side of the game. These are installable through Node.js.
* 'phpunit.xml' which contains the configuration for PHPUnit, the testing library for the server-side of the game.
* 'tsconfig.json' which contains the TypeScript build configuration.

====================================
Build and installation instructions
====================================

To build this project from source, you will need to have installed both Node.js and Composer. Links to these are available in the final report.
In order to run the server, you will also need a Linux machine or instance running an HTTP server (such as nginx or Apache), PHP (7.0 or above), MySQL and Redis.
A MySQL database and user will need to have been setup, with credentials to hand.
And the HTTP server should be configured to point a domain (or something like localhost), to the 'game/public' directory. Writable permissions may need to be set on the 'game/storage' and 'game/bootstrap/cache' directories to prevent errors.

The game dependencies and client-side code can then be installed by running the following commands inside the 'game' directory;
* npm install -g gulp
* npm install -g laravel-echo-server
* npm install
* composer install
* gulp

The next step is to update the configuration in your '.env' (renaming the example if required) and 'laravel-echo-server.json' files. Particular attention should be paid to the hostname, web socket server, database and Redis configuration. The server-side of the game can then be installed by running the following commands inside the 'game' directory;
* php artisan migrate
* php artisan key:generate

The unit tests for the client-side will have already been run by the previous 'gulp' command. But the PHP server-side tests can be run using the 'phpunit' command.
Once these steps are complete, the web socket server can then be started along with the Laravel queue worker. These commands should be run inside the 'game' directory either as daemons or in separate terminals;
* laravel-echo-server start
* php artisan queue:work

At this point the game should be accessible and working on the address you have configured. Multiplayer functionality should also work, with moves and other game events being broadcast between game clients. If you need to access the results of the QUnit client-side tests, these can be found at <game URL>/tests.html.
